
sqUtils = require('sqUtils')

mappings = {

   adt = {},
   orm = {},
   siu = {},
   mdm = {}

}

----------------------------------------------------------------------------
function mappings.mapMSH(Message, Appointment, PatientFlow, isA31)
   
   Message.MSH[3][1] = 'Swiftqueue'
   Message.MSH[4][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultSendingFacility')
   Message.MSH[5][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReceivingAppCode')
   Message.MSH[6][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultSendingFacility')
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')
   
   --[[
   From Ian 05.12.23
   
   While this solution (iPM/SQ interfacing) isn’t mature yet, I would like to recommend that Iguana sets 
   both the Accept Acknowledgement (MSH-15) and the Application Acknowledgement (MSH-16) to "AL".  
   Iguana could then use the Accept acknowledgement to confirm that the iIE has received the HL7 message 
   therefore stop the need to resend the HL7 message and then receive a secondary Application Acknowledgement, 
   which would contain the iPM/iIE's error or success status.  For example, if you do not receive an App 
   ACK within 30 seconds, consider the Message as unprocessed in iPM.
  
   Message.MSH[15] = 'NE'
   Message.MSH[16] = 'AL'
   ]]
  -- Message.MSH[15] = 'AL'
  -- Message.MSH[16] = 'AL'
   Message.MSH[15] = 'NE'
   Message.MSH[16] = 'AL'
   
   --not an A31
   if isA31 ~= 1 then
      
      if Appointment.modified_source ~= 'h' and not sqUtils.isEmpty(Appointment.modified_source) then
         Message.MSH[9][1] = 'SIU'
         if Appointment.status == 'active' then      
            Message.MSH[9][2] = 'S13'
         else
            Message.MSH[9][2] = 'S15'
         end
      else
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S12'
      end

      if PatientFlow.check_in_time ~= 'false' then
         Message.MSH[9][1] = 'ADT'
         Message.MSH[9][2] = 'A04'
      end

      if PatientFlow.complete_time ~= 'false' then
         Message.MSH[9][1] = 'ADT'
         Message.MSH[9][2] = 'A03'
      end

      if Appointment.is_attended == 'no' then
         if PatientFlow.current_stage == 'DNA' then
            Message.MSH[9][1] = 'SIU'
            Message.MSH[9][2] = 'S26'
         end
      end

   else
      --process A31's

      Message.MSH[9][1] = 'ADT'
      Message.MSH[9][2] = 'A31'
   
   end
    
   Message.MSH[10] = _G.msgCtrlID

   Message.MSH[12] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'hl7Version')


   --[[ ####Removed temporarily - validate during testing ####
   -- google_cal_id used to store clin_code only
   if Message.MSH[9][2] == 'S12' then
   conn:execute{sql="update reservation set " .. 
   "google_cal_id = " .. conn:quote(Message.MSH[10]:S())..
   " where id = " .. Appointment.id
   , live=false}
end
   ]]--

   return Message

end

----------------------------------------------------------
function mappings.mapEVN(Message, Appointment, PatientFlow)
   
   Message.EVN[2] = os.ts.date('!%Y%m%d%H%M%S')
   
   if Message.MSH[9][2]:S() == 'A31' then
      Message.EVN[1] = 'A31'
      return Message
   end

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.EVN[1] = 'S13'
      else
         Message.EVN[1] = 'S15'
      end
   else
      Message.EVN[1] = 'S12'
   end
   --
   if PatientFlow.check_in_time ~= 'false' then
      Message.EVN[1] = 'A04'
   end
   --
   if PatientFlow.complete_time ~= 'false' then
      Message.EVN[1] = 'A03'
   end
   --
   

   return Message

end

----------------------------------------------------------
function mappings.mapSCH(Message, Appointment, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   local sqApptID = ""
   local extApptID = ""

   if not sqUtils.isEmpty(Appointment.id) then
      sqApptID = Appointment.id
   end
   
   if not sqUtils.isEmpty(Appointment.location_pref) then
      extApptID = Appointment.location_pref
   end

   Message.SCH[1][1] = sqApptID
   Message.SCH[1][2] = 'SWIFTQ'

   --PB 16.11.23: according to Ian G, we shouldn't be sending this in S12
   if Message.MSH[9][2]:S() ~= 'S12' then
      Message.SCH[2][1] = extApptID
   end
   
   Message.SCH[2][2] = 'OAM'

   --second return from function will be session code
   _, Message.SCH[5][1] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   
   trace(Appointment.id) -- reservation id      
   if type(Referral) ~= 'userdata' and Referral.control_id ~= NULL then
      Message.SCH[4][1] = Referral.control_id
      Message.SCH[4][2] = 'REF'          
   end

   local appId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      appId = Appointment.parent_id
   end
   
   local res = sqDB.getAppointmentReason(appId)
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.SCH[7][1] = res[1].ipm_value:S()
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map

      local clinCode = sqDB.getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
      Message.SCH[7][1] = sqUtils.getJsonMappedValue2(sqCfg.appreason_map,clinCode,'default')
   end
   
   --use default appointment duration if not set
   local appDuration = Appointment.duration
   if Appointment.duration == nil then
      appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   end

   --Message.SCH[7][1] = sqDB.THS.getAppointmentReason(Appointment.id)
   Message.SCH[7][2] = 'Follow Up'
   Message.SCH[9] = appDuration
   Message.SCH[10][1] = 'MIN' 
   Message.SCH[11][3] = appDuration
   trace(Message.SCH[11][3]:S())
   
   if Appointment.start == nil then
      net.http.respond{body='{"success": true}', code=200}
      error("Json Payload missing appointment start date")
   end
      
   Message.SCH[11][4] = Appointment.start
   Message.SCH[11][5] = sqUtils.addMinutesToDate(Appointment.start, appDuration)
   
   local res = sqDB.getAppPriorityByAppId(appId)
   trace(res[1].ipm_value:S())
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.SCH[11][6] = res[1].ipm_value:S()
   else
      Message.SCH[11][6] = sqUtils.getJsonMappedValue(sqCfg.priority_map,'default')
   end
   
   
   --Message.SCH[11][6] = sqDBTHS.getPriorityCode(appId, Referral)

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' or Appointment.modified_source == 'PR' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = 'Rescheduled'
      else
         
         --set a default
         Message.SCH[25][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultCancelCode')
         Message.SCH[25][2] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultCancelDesc')
         Message.SCH[25][3] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'cancelCodeDomain')
         
         local action = mappings.getActionReason(Appointment.action_reasons,'cancel')
         if action ~= '' then
            Message.SCH[25][1] = action.code
            Message.SCH[25][2] = action.reason
            Message.SCH[25][3] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'cancelCodeDomain')
         else
            sqUtils.printLogW("No Cancel action reasons received from SQ")
         end
      end
      
   else
      if Message.MSH[9][2]:S() ~= 'S15' then
         Message.SCH[25][1] = 'New'
      end
   end
   
   if Message.MSH[9][2]:S() == 'S26' then
      Message.SCH[25][1] = 'DNA'
      local action = mappings.getActionReason(Appointment.action_reasons,'dna')
      if action ~= '' then
         Message.SCH[25][1] = action.code
         Message.SCH[25][2] = action.reason
         Message.SCH[25][3] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'reasonCodeDomain')
      else
         sqUtils.printLogW("No DNA action reasons received from SQ")
      end
   end
   
   return Message
end

----------------------------------------------------------

function mappings.getActionReason(actions, actionType)
   
   local action = ''
   
   if not sqUtils.isEmpty(actions) then

      for i=1, #actions do
         if actions[i].type == actionType then
            action = actions[i]
            break
         end
      end

   end
   
   return action
   
end

----------------------------------------------------------
function mappings.mapPID(Message, Patient, PatientFlow)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --[[
   PID|1||500244753^^^^PAS^^20231108000000~99999999999^^^AUSHIC^MC^^20231108000000||Xxtest^One^^^MR^^L||
   20000101000000|M||4^Neither Aboriginal nor Torres Strait Islander origin^ETHGR|
   123454 My Street^Cape Richie^Mardie^WA^6714^1101^R||||
   NSP^Not Specified^SPOKL|S^Single^MARRY|7011^Agnosticism^RELIG|||||
   4^Neither Aboriginal nor Torres Strait Islander origin^ETHGR|1101|N||||VWELL^Very well^NATNL||N|N||20231108215343|||||
   ]]
 

   Message.PID[1] = 1 

   local pidIndex = 1
   if not sqUtils.isEmpty(Patient.mrn) then
      Message.PID[3][pidIndex][1] = Patient.mrn
      Message.PID[3][pidIndex][5] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultIdentifierTypeCode')
      pidIndex = pidIndex + 1
   end
   --[[ requested to be removed by Ian Guthrie
   if not sqUtils.isEmpty(Patient.id) then
      Message.PID[3][pidIndex][1] = Patient.id
      Message.PID[3][pidIndex][5] = 'SQID'
   end
   ]]


   Message.PID[5][1][1] = Patient.surname
   Message.PID[5][1][2] = Patient.firstname
   Message.PID[5][1][5] = Patient.title
   Message.PID[7] = string.sub(Patient.Date_of_Birth,7,11) .. string.sub(Patient.Date_of_Birth,4,5) .. 
       string.sub(Patient.Date_of_Birth,1,2) .. "000000"
   Message.PID[8] = Patient.Gender
   
   local ad1 = Patient.Address_1
   Message.PID[11][1][1] = Patient.Address_1
   Message.PID[11][1][2] = Patient.Address_2
   Message.PID[11][1][3] = Patient.Address_3
   Message.PID[11][1][4] = Patient.Address_4
   Message.PID[11][1][5] = Patient.Home_Address
   Message.PID[11][1][6] = 'NSP'
   
   local i = 1
   Message.PID[13][i][1] = Patient.home_phone
   Message.PID[13][i][2] = 'PRN'
   Message.PID[13][i][3] = 'PH'
   i = i +1
   if not sqUtils.isEmpty(Patient.email) then
      Message.PID[13][i][1] = Patient.email
      Message.PID[13][i][2] = 'NET'
      Message.PID[13][i][3] = 'Internet'
      i = i +1
   end
   if not sqUtils.isEmpty(Patient.mobile) then
      Message.PID[13][i][1] = Patient.mobile
      Message.PID[13][i][2] = 'PRN'
      Message.PID[13][i][3] = 'CP'
      i = i +1
   end
   
   --State
   --Need to know if this goes to IPM
   local state = Patient.Address_5
   

   return Message
end  

----------------------------------------------------------

function mappings.getSQFacilityToIPMMApping(facilityId)
     
   local clinic = ''
   local session = ''
   local cons = ''
   local spec = ''
    
   local useCSRLookup = sqUtils.getJsonMappedValue(sqCfg.extra_params,'useCSRLookup')
   
   if useCSRLookup == '1' then
      
      clinic, session, cons, spec = sqDB.THS.getSQFacilityToIPMMApping(facilityId)  

   else

      local map = sqUtils.getJsonMappedValue(sqCfg.extra_params,'SQoutboundClinicMap')
      local str = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(map),tostring(facilityId))
      local tbl = sqUtils.split(str,'|')
      clinic = tbl[2]
      session = tbl[3]
      cons = tbl[4]
      spec = tbl[5]

   end
   
   if clinic == nil then clinic = '' end
   if session == nil then session = '' end
   if cons == nil then cons = '' end
   if spec == nil then spec = '' end
   
   return clinic, session, cons, spec
end

----------------------------------------------------------
function mappings.mapPV1(Message, Appointment, PatientFlow, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   Message.PV1[1] = 1
   Message.PV1[2] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultPatientClass')

   Message.PV1[3][1] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   
   if not sqUtils.isEmpty(Appointment.google_cal_id) then
      Message.PV1[3][1] = Appointment.google_cal_id   
   end
 
   --populate vistit type  / appointment reason
   local clinCode = Message.PV1[3][1]:S()
   --Message.PV1[4] = sqUtils.getJsonMappedValue2(sqCfg.appreason_map,clinCode,Appointment.reason_id)
   Message.PV1[4] = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'appReasonsMap_outbound',Appointment.reason_id)  
 
   Message.PV1[5][1] = ''
   if (type(Referral) ~= 'userdata') and (Referral.covid_19_id ~= '') then
      Message.PV1[5][1] = Referral.covid_19_id
   end
    
   local attendingDoctorMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'attendingDoctorMap')
    
   local docCode, docName = sqDB.getAttendingDoctor(Appointment.id, Appointment.facility_id)
   if docCode ~= '' then
      Message.PV1[7][1] = docCode
   else
      local ret = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(attendingDoctorMap),docName)
      if ret == '' then
         ret = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(attendingDoctorMap),'default')
      end
      Message.PV1[7][1] = ret
   end
  
   res = sqDB.getSpecialtyCode(Appointment.id)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.PV1[10] = res[1]['ipm_value']:S()    
   else
      --[[
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.PV1[10] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)  
      ]]
       _,_,_, Message.PV1[10] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
      
   end
   
   local fID = Appointment.facility_id  
   
   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local appCategoryMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'appCategoryMap')
   
   if not sqUtils.isEmpty(Appointment.custom_fields.account_class) then
      Message.PV1[18] = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(appCategoryMap),Appointment.custom_fields.account_class)
   end

   if (Message.MSH[9][2]:S() == 'A03' or Message.MSH[9][2]:S() == 'A04' or Message.MSH[9][2]:S() == 'S13' or 
                    Message.MSH[9][2]:S() == 'S15' or Message.MSH[9][2]:S() == 'S26' )
      and sqUtils.isEmpty(Appointment.location_pref) then
      --net.http.respond{body='{"success": true}', code=200}
      error("Attempting to send message to IPM with no App ID")
      --sqUtils.printLogW("Attempting to send message to IPM with no App ID")
   end
   
   Message.PV1[19][1] = Appointment.location_pref
   
   if Message.MSH[9][2]:S() == 'S12' then
      --add insurance compnay to PV1[20]
      --[[
      if not sqUtils.isEmpty(Appointment.custom_fields.account_class) then
         Message.PV1[20] = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(appCategoryMap),Appointment.custom_fields.account_class)
      end
      ]]
   end
   
   -- only check and send these values when the app is complete
   if Message.MSH[9][2]:S() == 'S26' then
      
      Message.PV1[36] = '0'
   
   elseif PatientFlow.complete_time ~= 'false' then
      
      Message.PV1[14] = getAttendanceStatusCode(Appointment.custom_fields.attnd_attended_status)
      Message.PV1[36] = getOutcomeStatusCode(Appointment.custom_fields.scocm_outcome)
   
   end
   
   if PatientFlow.complete_time ~= 'false' then     
      Message.PV1[45] = PatientFlow.complete_time
   end
   
   if PatientFlow.check_in_time ~= 'false' then
      Message.PV1[44] = PatientFlow.check_in_time
   end
   
   if PatientFlow.check_in_time == 'false' and PatientFlow.complete_time == 'false' and PatientFlow.current_stage ~= 'DNA' then
      Message.PV1[44] = Appointment.start
   end

   return Message
end

-------------------------------------------------

function mappings.mapPV2(Message, Appointment, PatientFlow, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
 
	Message.PV2[8] = Appointment.start
   Message.PV2[9] = sqUtils.addMinutesToDate(Appointment.start, Appointment.duration)
   
   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local res = sqDB.getAppType(AppId)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
      Message.PV2[12] =  res[1]['ipm_value']:S()
   else
      
      local appTypeMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'appTypeMap')
      
      Message.PV2[12] = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(appTypeMap),'default')
   end
   
   --PB 23.11.23: IR-82, Santhi requested setting only PV2[24] for s26 and Attendance messages
   trace(Message.MSH[9][2]:S())
   if Message.MSH[9][2]:S() == 'A04' or Message.MSH[9][2]:S() == 'A03' or Message.MSH[9][2]:S() == 'S26' then
      Message.PV2[24] = getAttendanceStatusCode(Appointment.custom_fields.attnd_attended_status)
   end
   
  --get the priority code and default if not found
   if not sqUtils.isEmpty(Referral) then
      if not sqUtils.isEmpty(Referral.category_id) then
         Message.PV2[25] = sqDB.THS.getReferralPriorityCode(Referral.facility_id, Referral.category_id)
      else
         local defCatId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultRefCatId')
         Message.PV2[25] = sqDB.THS.getReferralPriorityCode(Referral.facility_id, defCatId)
         sqUtils.printLogW("Missing or invalid refewrral category ID found in export, defaulting..")
      end
   end
      
   Message.PV2[27] = getOutcomeStatusCode(Appointment.custom_fields.scocm_outcome)
     
   return Message
end 

--------------------------------------------------

function mappings.mapAIP(Message, Appointment, PatientFlow)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   -- get the details from the DB
   Message.AIP[1] = 1

   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local docCode, docName = sqDB.getAttendingDoctor(AppId, Appointment.facility_id)
   
   if docCode ~= '' then
      Message.AIP[3][1] = docCode
   else      
      _,_, Message.AIP[3][1] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
      
   end
     
   local res = sqDB.getSpecialtyCode(AppId)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.AIP[4][1] = res[1]['ipm_value']:S()    
   else
      --[[
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.AIP[4][1] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)
      ]]
      --second return from function will be session code
      _,_,_, Message.AIP[4][1] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   end

   return Message
end

--------------------------------------------------

function mappings.mapNK1(Message, NOK)
	sqUtils.printLog("Calling Mappings.mapNK1...")
 
   Message.NK1[1][2][1] = NOK.surname
   Message.NK1[1][2][2] = NOK.firstname
   Message.NK1[1][2][3] = ''
   Message.NK1[1][2][5] = NOK.title
   Message.NK1[1][3][1] = NOK.relationship
   
   Message.NK1[1][4][1] = NOK.Address_1
   Message.NK1[1][4][2] = NOK.Address_1
   Message.NK1[1][4][3] = NOK.Address_1
   Message.NK1[1][4][4] = NOK.Address_1
  
   --Message.NK1[7][1] = conactRoleCode
   Message.NK1[1][15] = NOK.Gender
   if not sqUtils.isEmpty(NOK.Date_of_Birth) then
      Message.NK1[1][16] = string.sub(NOK.Date_of_Birth,7,11) .. string.sub(NOK.Date_of_Birth,4,5) .. 
      string.sub(NOK.Date_of_Birth,1,2) .. "000000" 
   end
   
   Message.NK1[1][5][1][1] = NOK.mobile
   Message.NK1[1][5][1][2] = 'PRN'
   Message.NK1[1][5][1][3] = 'PH'
   
   Message.NK1[1][31][1] = NOK.mobile
   Message.NK1[1][31][2] = 'PRN'
   Message.NK1[1][31][3] = 'CP'
   
   

   return Message
end 

--------------------------------------------------

function mappings.mapAIL(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIL[1] = 1
   Message.AIL[2] = 'A'
   
   local appId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      appId = Appointment.parent_id
   end
   
 
   local clinCode = Appointment.google_cal_id
   
   if sqUtils.isEmpty(Appointment.google_cal_id) then
      clinCode = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   else
      clinCode = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   end
 
   Message.AIL[3][1] = clinCode
   
   Message.AIL[4][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultLocationType')
   
   local res = sqDB.getSpecialtyCode(appId, Appointment.facility_id, Appointment.reason_id)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.AIL[5][1] = res[1]['ipm_value']:S()    
   else
      --[[
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.AIL[5][1] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)  
      ]]
      _,_,_, Message.AIL[5][1] = mappings.getSQFacilityToIPMMApping(Appointment.facility_id)
   end

   Message.AIL[12][1] = 'BOOKED'

   return Message
end 

-------------------------------------------------------------------------

function mappings.buildA31(Message, Value)
   
   sqUtils.printLog("Calling Mappings.buildA31...")
   
   --[[ sample:
   
   MSH|^~\&|IIEIPM|NWA|||20231108215347||ADT^A31|1842|P|2.4|||AL|NE|||
   EVN|A31|20231108215300||PREGI_U|PIMS^Administrative User^^^^^^USERS|
   PID|1||500244753^^^^PAS^^20231108000000~99999999999^^^AUSHIC^MC^^20231108000000||Xxtest^One^^^MR^^L||20000101000000|M||4
   ^Neither Aboriginal nor Torres Strait Islander origin^ETHGR|123454 My Street^Cape Richie^Mardie^WA^6714^1101^R||||
   NSP^Not Specified^SPOKL|S^Single^MARRY|7011^Agnosticism^RELIG|||||4^Neither Aboriginal nor Torres Strait Islander origin^ETHGR|
   1101|N||||VWELL^Very well^NATNL||N|N||20231108215343|||||
   PD1|||MOONAH HEALTH CENTRE^GPPRC^GPP0933^^^^PRIME|
   2075^Wahbi-Izzettin^Sue^^^DR^PRIME^prof_carers^GPP0933^L^^^MAIN~2075^Wahbi-Izzettin^Sue^^^DR^PRIME^prof_carers^GPP0933^L^^^
   LOCAL~N/A^Wahbi-Izzettin^Sue^^^DR^PRIME^prof_carers^NSP^L^^^PROV^GPP0933||||||||
   ROL|1672110^PATPC|UC|PP^GP|2075^Wahbi-Izzettin^Sue^^^DR|19891102000000||||GMPRC^General Practitioner^PRTYP|||
   PV1|1|N
   IN1|1|1493753^^PIMS|HIC|MEDICARE AUSTRALIA||||||||20231108|||NA|Xxtest^One^^^MR^^L||20000101000000|123454 My Street^
   Cape Richie^Mardie^WA^6714^1101^R|||||||||||||||||1234556|||||||M||||||
   
   ]]
   
   local msg = nil
   
   local isA31 = 1
   msg = mappings.mapMSH(Message, Value.appointment, Value.patient_flow, isA31)
   msg = mappings.mapEVN(Message, Value.appointment, Value.patient_flow)
   msg = mappings.mapPID(Message, Value.patient, Value.patient_flow)
 
   if not sqUtils.isEmpty(Value.next_of_kin) then
      msg = mappings.mapNK1(Message, Value.next_of_kin)
   end
	
   return msg
  
   
end

-------------------------------------------------------------------------

function getAttendanceStatusCode(status)
   
   local attendanceStatusCodeMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'attendanceStatusCodeMap')
   
   local res = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(attendanceStatusCodeMap),status)
   if res == '' then
      res = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(attendanceStatusCodeMap),'default')
   end
   
   return res
   
end

-------------------------------------------------------------------------

function getOutcomeStatusCode(status)
   
   local outcomeStatusCodeMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'outcomeStatusCodeMap')
   
   local res = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(outcomeStatusCodeMap),status)
   if res == '' then
      res = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(outcomeStatusCodeMap),'default')
   end
   
   return res
   
end
   


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----    MDM MAPPINGS 
----------------------------------------------------------------------------
----------------------------------------------------------------------------

function mappings.mdm.buildT04(Message, Value)
   
   sqUtils.printLog("Calling Mappings.mdm.buildT04...")
   
   local msg = nil
   
   msg = mappings.mdm.mapMSH(Message, Value)
   msg = mappings.mdm.mapEVN(Message, Value)
   msg = mappings.mdm.mapPID(Message, Value.patient)
   msg = mappings.mdm.mapPV1(Message, Value)
   msg = mappings.mdm.mapTXA(Message, Value)
   msg = mappings.mdm.mapOBX(Message, Value)
 
   return msg
  
end

----------------------------------------------------------------------------

function mappings.mdm.mapMSH(Message, Value)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --MSH|^~\&|CSCLRC|RZZ001|CSCLRC|RZZ001|20161207175100||MDM^T04^MDM_T02|60000102|P|2.4|60000102||AL|AL|GBR|UTF-8|||LZO_R2 _4
   
   Message.MSH[3][1] = 'CSCLRC'
   Message.MSH[4][1] = 'RZZ001'
   Message.MSH[5][1] = 'CSCLRC'
   Message.MSH[6][1] = 'RZZ001'
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')
   Message.MSH[9][1] = 'MDM'
   Message.MSH[9][2] = 'T04'
   Message.MSH[10] = _G.msgCtrlID
   Message.MSH[11][1] = 'P'
   Message.MSH[12] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'hl7Version')
   Message.MSH[13] = _G.msgCtrlID
   Message.MSH[15] = 'AL'
   Message.MSH[16] = 'AL'
   Message.MSH[17] = 'GBR'
   Message.MSH[18][1] = 'UTF-8'
   --Message.MSH[21] = 'LZO_R2 _4'
   
   trace(Message)
   
   return Message

end

----------------------------------------------------------

function mappings.mdm.mapEVN(Message, Value)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --EVN||20161207175100||DOC-U|20161207175100|
   local str = os.ts.date('!%Y%m%d%H%M%S')
   Message.EVN[2] = str
   Message.EVN[4] = 'DOC-U'
   Message.EVN[5][1] = str
   
   trace(Message)

   return Message

end

----------------------------------------------------------

function mappings.mdm.mapPID(Message, Patient)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --PID|1||RZZ001-PAS-100000428^^^RZZ001^FACIL^RZZ001||williams^Sarah^^^||19900101000000|F|||^^Leicester^^^UK^HOME^^|||||||||||||||||||N||""~""~""
 
   Message.PID[1] = 1 

   local pidIndex = 1
   if not sqUtils.isEmpty(Patient.mrn) then
      Message.PID[3][pidIndex][1] = Patient.mrn
      Message.PID[3][pidIndex][5] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultIdentifierTypeCode')
      pidIndex = pidIndex + 1
   end
   
   Message.PID[5][1][1] = Patient.surname
   Message.PID[5][1][2] = Patient.firstname
   Message.PID[5][1][5] = Patient.title
   Message.PID[7] = string.sub(Patient.Date_of_Birth,7,11) .. string.sub(Patient.Date_of_Birth,4,5) .. 
       string.sub(Patient.Date_of_Birth,1,2) .. "000000"
   Message.PID[8] = Patient.Gender
   
   local ad1 = Patient.Address_1
   Message.PID[11][1][1] = Patient.Address_1
   Message.PID[11][1][2] = Patient.Address_2
   Message.PID[11][1][3] = Patient.Address_3
   Message.PID[11][1][4] = Patient.Address_4
   Message.PID[11][1][5] = Patient.Home_Address
   Message.PID[11][1][6] = 'NSP'
   
   local i = 1
   Message.PID[13][i][1] = Patient.home_phone
   Message.PID[13][i][2] = 'PRN'
   Message.PID[13][i][3] = 'PH'
   i = i +1
   if not sqUtils.isEmpty(Patient.email) then
      Message.PID[13][i][1] = Patient.email
      Message.PID[13][i][2] = 'NET'
      Message.PID[13][i][3] = 'Internet'
      i = i +1
   end
   if not sqUtils.isEmpty(Patient.mobile) then
      Message.PID[13][i][1] = Patient.mobile
      Message.PID[13][i][2] = 'PRN'
      Message.PID[13][i][3] = 'CP'
      i = i +1
   end
   
   --State
   --Need to know if this goes to IPM
   local state = Patient.Address_5

   return Message
end 

----------------------------------------------------------

function mappings.mdm.mapPV1(Message, Value)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --   PV1|1|E|asdad^asdasdas^^RZZ001^^^^^SivaSPoint||EPS0000563^^^RZZ001|^|||SP9884123456^prabhuthenappan^senthil^^^DR^^NATGP^^^600048822611|RZZ002-30
   --   |||||||SP9884123456^prabhuthenappan^senthil^^^DR^^NATGP^^^600048822611||Si-0005^^^LRC_ENCOUNTER|||||||||||||||||||||||||20161207174947|

   local dt = os.ts.date('!%Y%m%d%H%M%S')
   
   Message.PV1[1] = 1
   Message.PV1[2] = 'E'
   Message.PV1[3][1] = 'asdad'
   Message.PV1[3][2] = 'asdasdas'
   Message.PV1[3][4][1] = 'RZZ001'
   Message.PV1[3][9] = 'SivaSPoint'
   Message.PV1[5][1] = 'EPS0000563'
   Message.PV1[5][4][1] = 'RZZ001'
   Message.PV1[9][1] = 'SP9884123456'
   Message.PV1[9][2] = 'prabhuthenappan'
   Message.PV1[9][3] = 'senthil'
   Message.PV1[9][6] = 'DR'
   Message.PV1[9][8] = 'NATGP'
   Message.PV1[9][11] = '600048822611'
   Message.PV1[10] = 'RZZ002-30'
   Message.PV1[17][1] = 'SP9884123456'
   Message.PV1[17][2] = 'prabhuthenappan'
   Message.PV1[17][3] = 'senthil'
   Message.PV1[17][6] = 'DR'
   Message.PV1[17][8] = 'NATGP'
   Message.PV1[17][11] = '600048822611'
   Message.PV1[19][1] = 'Si-0005'
   Message.PV1[19][4][1] = 'LRC_ENCOUNTER'
   Message.PV1[44] = dt

   trace(Message)
   
   return Message
end

----------------------------------------------------------

function mappings.mdm.mapTXA(Message, Value)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --TXA|1|CLICORRES||||20151025011000|||123654789^me001^me001^^^Ms^^NATNL|99999^me002^me002^^^MR^^X-GMC|abc^me003^me003^^^^^xxNATDP||||DOCFillerID_101^CSCLRC|DOCName_101|FINALISED||||abc||
   
   local dt = os.ts.date('!%Y%m%d%H%M%S')
   
   Message.TXA[1] = '1'
   Message.TXA[2] = 'CLICORRES'
   Message.TXA[6] = dt
   Message.TXA[9][1][1] = '123654789'
   Message.TXA[9][1][2] = 'me001'
   Message.TXA[9][1][3] = 'me001'
   Message.TXA[9][1][6] = 'Ms'
   Message.TXA[9][1][8] = 'NATNL'
   Message.TXA[10][1][1] = '99999'
   Message.TXA[10][1][2] = 'me002'
   Message.TXA[10][1][3] = 'me002'
   Message.TXA[10][1][6] = 'MR'
   Message.TXA[10][1][8] = 'X-GMC'
   Message.TXA[11][1][1] = 'abc'
   Message.TXA[11][1][2] = 'me003'
   Message.TXA[11][1][3] = 'me003'
   Message.TXA[11][1][6] = ''
   Message.TXA[11][1][8] = 'xxNATDP'
   Message.TXA[15][1] = 'DOCFillerID_101'
   Message.TXA[15][2] = 'CSCLRC'
   Message.TXA[16] = 'DOCName_101'
   Message.TXA[17] = 'FINALISED'
   Message.TXA[21] = 'abc'
   
   trace(Message)

   return Message

end

----------------------------------------------------------

function mappings.mdm.mapOBX(Message, Value)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
   
   --OBX|1|ED|DOCName_101^^^Template_CC||CSCLRC^^PDF^^##BASE64_STRING##||||||F
   
   
   local rnumber = math.random(1, 1000) 
   
   trace(USTR)
   local base64Str = filter.base64.enc(tostring(rnumber)..USTR)
   trace(base64Str)
      
   Message.OBX[1] = '1'
   Message.OBX[2] = 'ED'
   Message.OBX[3][1] = 'DOCName_'..tostring(rnumber)
   Message.OBX[3][4] = 'Template_CC'
   Message.OBX[5][1][1] = 'CSCLRC'
   Message.OBX[5][1][3] = 'PDF'
   Message.OBX[5][1][5] = base64Str
   Message.OBX[11] = 'F'
   
   trace(Message)

   return Message

end




USTR = 'UEsDBBQABgAIAAAAIQDfpNJsWgEAACAFAAATAAgCW0NvbnRlbnRfVHlwZXNdLnhtbCCiBAIooAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC0lMtuwjAQRfeV+g+Rt1Vi6KKqKgKLPpYtUukHGHsCVv2Sx7z+vhMCUVUBkQpsIiUz994zVsaD0dqabAkRtXcl6xc9loGTXmk3K9nX5C1/ZBkm4ZQw3kHJNoBsNLy9GUw2ATAjtcOSzVMKT5yjnIMVWPgAjiqVj1Ykeo0zHoT8FjPg973eA5feJXApT7UHGw5eoBILk7LXNX1uSCIYZNlz01hnlUyEYLQUiep86dSflHyXUJBy24NzHfCOGhg/mFBXjgfsdB90NFEryMYipndhqYuvfFRcebmwpCxO2xzg9FWlJbT62i1ELwGRztyaoq1Yod2e/ygHpo0BvDxF49sdDymR4BoAO+dOhBVMP69G8cu8E6Si3ImYGrg8RmvdCZFoA6F59s/m2NqciqTOcfQBaaPjP8ber2ytzmngADHp039dm0jWZ88H9W2gQB3I5tv7bfgDAAD//wMAUEsDBBQABgAIAAAAIQAekRq37wAAAE4CAAALAAgCX3JlbHMvLnJlbHMgogQCKKAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArJLBasMwDEDvg/2D0b1R2sEYo04vY9DbGNkHCFtJTBPb2GrX/v082NgCXelhR8vS05PQenOcRnXglF3wGpZVDYq9Cdb5XsNb+7x4AJWFvKUxeNZw4gyb5vZm/cojSSnKg4tZFYrPGgaR+IiYzcAT5SpE9uWnC2kiKc/UYySzo55xVdf3mH4zoJkx1dZqSFt7B6o9Rb6GHbrOGX4KZj+xlzMtkI/C3rJdxFTqk7gyjWop9SwabDAvJZyRYqwKGvC80ep6o7+nxYmFLAmhCYkv+3xmXBJa/ueK5hk/Nu8hWbRf4W8bnF1B8wEAAP//AwBQSwMEFAAGAAgAAAAhANZks1H0AAAAMQMAABwACAF3b3JkL19yZWxzL2RvY3VtZW50LnhtbC5yZWxzIKIEASigAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArJLLasMwEEX3hf6DmH0tO31QQuRsSiHb1v0ARR4/qCwJzfThv69ISevQYLrwcq6Yc8+ANtvPwYp3jNR7p6DIchDojK971yp4qR6v7kEQa1dr6x0qGJFgW15ebJ7Qak5L1PWBRKI4UtAxh7WUZDocNGU+oEsvjY+D5jTGVgZtXnWLcpXndzJOGVCeMMWuVhB39TWIagz4H7Zvmt7ggzdvAzo+UyE/cP+MzOk4SlgdW2QFkzBLRJDnRVZLitAfi2Myp1AsqsCjxanAYZ6rv12yntMu/rYfxu+wmHO4WdKh8Y4rvbcTj5/oKCFPPnr5BQAA//8DAFBLAwQUAAYACAAAACEAzZQZ+C0CAABWBgAAEQAAAHdvcmQvZG9jdW1lbnQueG1spJVNj9owEIbvlfofIt8hCbBbFAGrdtmle6i0Ku25Mo6TWNgeyzZQ+us7zgehH1qxyyXOeGaeeceOndndTyWjPbdOgJ6TdJiQiGsGudDlnHz/9jiYksh5qnMqQfM5OXJH7hbv380OWQ5sp7j2ESK0yw6GzUnlvcni2LGKK+qGSjALDgo/ZKBiKArBeHwAm8ejJE3qN2OBceew3j3Ve+pIi1P/0sBwjc4CrKIeTVvGitrtzgyQbqgXGyGFPyI7ue0wMCc7q7MWMTgJCilZI6gdugx7Sd0mZdmuQF0xtlyiBtCuEqZv4600dFYdZP9SE3slu7iDSSfX7cHS0gMOPfAS+XmTpGSj/GVimlywIwFxyrhEwp81OyWKCt0XftPSnC1uevM6wOhvgCmv25yVhZ3paeI62pPenljhZL+C1W7yeWvuOjHriho8gYplT6UGSzcSFeGWRbjqUfisyQJvnA3kxzCa6JDhjZV/nZMkGS9vx+kj6aaWvKA76YMnfZjcT8d1pg0Pv/jMpYToI6uoPdJZHKbCs/ZuALbhOll7aj3SRI6MgNVUoZgfK/hE2ZbE57EPOj9FxjXKBLfjzD/b/4isxZfrX+jCzzodjSZ1hQrfb6aTmhECvtCQ7AFPXzppQqwoK9+bG/AeVG9LXpx5K05zjvfYh1FtFgD+zCx3vjbbcgykw1lnKONNTD2Nt/vKitCeFJo/C89Q5fi267NpsX5tdiXufwiL3wAAAP//AwBQSwMEFAAGAAgAAAAhAKpSJd8jBgAAixoAABUAAAB3b3JkL3RoZW1lL3RoZW1lMS54bWzsWU2LGzcYvhf6H8TcHX/N+GOJN9hjO2mzm4TsJiVHeUaeUawZGUneXRMCJTkWCqVp6aGB3noobQMJ9JL+mm1T2hTyF6rReGzJllnabGApWcNaH8/76tH7So80nstXThICjhDjmKYdp3qp4gCUBjTEadRx7hwOSy0HcAHTEBKaoo4zR9y5svvhB5fhjohRgoC0T/kO7DixENOdcpkHshnyS3SKUtk3piyBQlZZVA4ZPJZ+E1KuVSqNcgJx6oAUJtLtzfEYBwgcZi6d3cL5gMh/qeBZQ0DYQeYaGRYKG06q2Refc58wcARJx5HjhPT4EJ0IBxDIhezoOBX155R3L5eXRkRssdXshupvYbcwCCc1Zcei0dLQdT230V36VwAiNnGD5qAxaCz9KQAMAjnTnIuO9XrtXt9bYDVQXrT47jf79aqB1/zXN/BdL/sYeAXKi+4Gfjj0VzHUQHnRs8SkWfNdA69AebGxgW9Wun23aeAVKCY4nWygK16j7hezXULGlFyzwtueO2zWFvAVqqytrtw+FdvWWgLvUzaUAJVcKHAKxHyKxjCQOB8SPGIY7OEolgtvClPKZXOlVhlW6vJ/9nFVSUUE7iCoWedNAd9oyvgAHjA8FR3nY+nV0SBvXv745uVzcProxemjX04fPz599LPF6hpMI93q9fdf/P30U/DX8+9eP/nKjuc6/vefPvvt1y/tQKEDX3397I8Xz1598/mfPzyxwLsMjnT4IU4QBzfQMbhNEzkxywBoxP6dxWEMsW7RTSMOU5jZWNADERvoG3NIoAXXQ2YE7zIpEzbg1dl9g/BBzGYCW4DX48QA7lNKepRZ53Q9G0uPwiyN7IOzmY67DeGRbWx/Lb+D2VSud2xz6cfIoHmLyJTDCKVIgKyPThCymN3D2IjrPg4Y5XQswD0MehBbQ3KIR8ZqWhldw4nMy9xGUObbiM3+XdCjxOa+j45MpNwVkNhcImKE8SqcCZhYGcOE6Mg9KGIbyYM5C4yAcyEzHSFCwSBEnNtsbrK5Qfe6lBd72vfJPDGRTOCJDbkHKdWRfTrxY5hMrZxxGuvYj/hELlEIblFhJUHNHZLVZR5gujXddzEy0n323r4jldW+QLKeGbNtCUTN/TgnY4iU8/Kanic4PVPc12Tde7eyLoX01bdP7bp7IQW9y7B1R63L+Dbcunj7lIX44mt3H87SW0huFwv0vXS/l+7/vXRv28/nL9grjVaX+OKqrtwkW+/tY0zIgZgTtMeVunM5vXAoG1VFGS0fE6axLC6GM3ARg6oMGBWfYBEfxHAqh6mqESK+cB1xMKVcng+q2eo76yCzZJ+GeWu1WjyZSgMoVu3yfCna5Wkk8tZGc/UItnSvapF6VC4IZLb/hoQ2mEmibiHRLBrPIKFmdi4s2hYWrcz9Vhbqa5EVuf8AzH7U8NyckVxvkKAwy1NuX2T33DO9LZjmtGuW6bUzrueTaYOEttxMEtoyjGGI1pvPOdftVUoNelkoNmk0W+8i15mIrGkDSc0aOJZ7ru5JNwGcdpyxvBnKYjKV/nimm5BEaccJxCLQ/0VZpoyLPuRxDlNd+fwTLBADBCdyretpIOmKW7XWzOZ4Qcm1KxcvcupLTzIaj1EgtrSsqrIvd2LtfUtwVqEzSfogDo/BiMzYbSgD5TWrWQBDzMUymiFm2uJeRXFNrhZb0fjFbLVFIZnGcHGi6GKew1V5SUebh2K6PiuzvpjMKMqS9Nan7tlGWYcmmlsOkOzUtOvHuzvkNVYr3TdY5dK9rnXtQuu2nRJvfyBo1FaDGdQyxhZqq1aT2jleCLThlktz2xlx3qfB+qrNDojiXqlqG68m6Oi+XPl9eV2dEcEVVXQinxH84kflXAlUa6EuJwLMGO44Dype1/Vrnl+qtLxBya27lVLL69ZLXc+rVwdetdLv1R7KoIg4qXr52EP5PEPmizcvqn3j7UtSXLMvBTQpU3UPLitj9falWtv+9gVgGZkHjdqwXW/3GqV2vTssuf1eq9T2G71Sv+E3+8O+77Xaw4cOOFJgt1v33cagVWpUfb/kNioZ/Va71HRrta7b7LYGbvfhItZy5sV3EV7Fa/cfAAAA//8DAFBLAwQUAAYACAAAACEA1DPN0pEDAACTCQAAEQAAAHdvcmQvc2V0dGluZ3MueG1stFbbbts4EH1foP9g6LmKdbHdQqhTJHG1TRFvF5X7AZRI2UR4A0nZcYv++w4pMXLSbuHdok+m5sydZ4Z+8/aBs8meaEOlWEbpRRJNiGgkpmK7jD5vyvh1NDEWCYyYFGQZHYmJ3l6++OPNoTDEWlAzE3AhTMGbZbSzVhXTqWl2hCNzIRURALZSc2ThU2+nHOn7TsWN5ApZWlNG7XGaJckiGtzIZdRpUQwuYk4bLY1srTMpZNvShgw/wUKfE7c3Wcmm40RYH3GqCYMcpDA7qkzwxv+vNwB3wcn+Z0XsOQt6hzQ5o9yD1PjR4pz0nIHSsiHGwAVxFhKkYgw8+87RY+wLiD2U6F2BeZr402nm8//mIHvmwLBzKumhO1prpHueDGXwprjdCqlRzYCVUM4EMoougZZfpOSTQ6GIbuBugNNJEk0dgEmLOmY3qK6sVKCyR5DDq2yAmx3SqLFEVwo10LYbKayWLOhh+Ze0N0BbDV0dLDyJx1PVDwRYCMQhqyckX0sMjD0UnabnN84Z+OhQ20nI54EkDLCmmGxcNyp7ZKSE5Cv6hVwJ/KEzloJHT/VfyOBnCRDhIn+E+9scFSkJsh206TcF8zdRMqrWVGupbwWGe/5twWjbEg0BKLJkDfShWh58n98ThGFv/mLc6SmNYAtjEw6fpLRBNUmyPEsDUx06Ium72c3r/EfIv9vkq0WelkP8ISov3Ab7W4eTo9CE9xY3iNeaosna7bip06j1/TUVAa8JDC05RaquDmAc94DhiLESZiwAPjVeYGrUirT+zNZIb0e/g4b+oRTm+cOjLzfrRP+pZad69KCR6qkRVNLZbLCkwt5RHuSmq6tgJWDNnECdwB/32vdpbM+hsHDFfsTukKeK1yUi/lwNVGK6cjQga6RUz6Z6my4jRrc7mzoCWPjC8BT6j3qbDVjmsazH/AdqXGWgPRxGWRZkJ3p5kOWjbBZks1E2D7L5KFsE2cLJdjDHmlFxD8QORydvJWPyQPD7Ef9O1DfB7JAiq37nAr1kLxiWsJnsC/IA25lgauEfhqKYowe3rLOFMx+0GTrKzj7RdZhTVk89YGRRGKknxp7iz3Jxb0FDgY7Vkdfjir/oE2fUwBpQ8BpYqQP20mPp3D8TdgMsvoeL/UTaa2QIHjAsm1vsHqLe5ms+T9M8ybO4zMtFPEuu8vi6vLqOZ+m71dWszNNVXn4bpjD8m7r8BwAA//8DAFBLAwQUAAYACAAAACEA7Jgi2sUBAADtBAAAEgAAAHdvcmQvZm9udFRhYmxlLnhtbLySbWvbMBDH3w/2HYzeN5adpA+mTsmyBgZjL0r3ARRFtsX0YHRK3Hz7nmTHLQ1lCYPJIOT/3f10+nP3Dy9aJXvhQFpTkmxCSSIMt1tp6pL8fl5f3ZIEPDNbpqwRJTkIIA+Lr1/uu6KyxkOC9QYKzUvSeN8WaQq8EZrBxLbCYLCyTjOPv65ONXN/du0Vt7plXm6kkv6Q5pRekwHjzqHYqpJcfLd8p4XxsT51QiHRGmhkC0dadw6ts27bOssFAL5Zq56nmTQjJpudgLTkzoKt/AQfM3QUUVie0XjS6g0wvwyQjwDNix+1sY5tFJqPnSQII4vB/aQrDNMYWDElN07GQMuMBZFhbM9USWhO13SOe/hmdBp2koZE3jAHIkD6RNrLFdNSHY4qdBKgD7TS8+ao75mToak+BLLGwA42tCSPFFe+XpNeyUoyQ2G5GpU83BVXNijTUaFB4ZHTZ9zFKh45Yw7emfYOnDjxLLWA5JfokiermfnEkZxeoxNz9CM4M73IERe5lzqSL987skLl5nZ2fP+bI3d/d6TnnO/IMBvJT1k3/tMJCXPxvyZkGVrOHz9MSE5vvp34EV//jxMyHGDxCgAA//8DAFBLAwQUAAYACAAAACEAW239kwkBAADxAQAAFAAAAHdvcmQvd2ViU2V0dGluZ3MueG1slNHBSgMxEAbgu+A7LLm32RYVWbotiFS8iKA+QJrOtsFMJsykrvXpHWutSC/1lkkyHzP8k9k7xuoNWAKl1oyGtakgeVqGtGrNy/N8cG0qKS4tXaQErdmCmNn0/GzSNz0snqAU/SmVKkka9K1Zl5Iba8WvAZ0MKUPSx44YXdGSVxYdv27ywBNmV8IixFC2dlzXV2bP8CkKdV3wcEt+g5DKrt8yRBUpyTpk+dH6U7SeeJmZPIjoPhi/PXQhHZjRxRGEwTMJdWWoy+wn2lHaPqp3J4y/wOX/gPEBQN/crxKxW0SNQCepFDNTzYByCRg+YE58w9QLsP26djFS//hwp4X9E9T0EwAA//8DAFBLAwQUAAYACAAAACEA274J8XABAADHAgAAEAAIAWRvY1Byb3BzL2FwcC54bWwgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACcUstOwzAQvCPxD1HurVMQCFVbI9QKceBRqaE9W/YmsXBsyzao/Xs2TRuCuOHTzqx3NLM23O9bk31hiNrZRT6bFnmGVjqlbb3I38vHyV2exSSsEsZZXOQHjPk9v7yAdXAeQ9IYM5KwcZE3Kfk5Y1E22Io4pbalTuVCKxLBUDNXVVriysnPFm1iV0Vxy3Cf0CpUEz8I5r3i/Cv9V1Q52fmL2/LgSY9Dia03IiF/7SbNVLnUAhtYKF0SptQt8hnRA4C1qDF2XF/AzgUV+RWwvoBlI4KQifbHZ8SOIDx4b7QUiRbLX7QMLroqZW9Ht1k3Dmx8BSjBBuVn0OnAC2BjCM/a9jb6gmwFUQfhm5O3AcFGCoNLys4rYSIC+yFg6VovLMmxoSK9j/juS7fq1nAa+U2OMu50ajZeyM7L9TjtqAEbYlGR/cHBQMATPUcwnTzN2hrV+c7fRre/bf8v+exmWtA5LuzMUezhw/BvAAAA//8DAFBLAwQUAAYACAAAACEAgjFPS38BAAAVAwAAEQAIAWRvY1Byb3BzL2NvcmUueG1sIKIEASigAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjJLBTsMwDIbvSLxDlXuXthsIVV0RA+0EEoIhEDeTmC1bk0ZJttK3J223jopJcLPj318c/8muv2QR7NBYUaopiUcRCVCxkgu1nJKXxTy8IoF1oDgUpcIpqdGS6/z8LGM6ZaXBR1NqNE6gDTxJ2ZTpKVk5p1NKLVuhBDvyCuWLn6WR4HxqllQD28ASaRJFl1SiAw4OaAMMdU8keyRnPVJvTdECOKNYoETlLI1HMT1qHRppTza0lR9KKVyt8aT0UOzVX1b0wqqqRtW4lfr5Y/r2cP/cPjUUqtkVQ5JnnKVOuALzjB5DH9ntxxqZ6477xMfMILjS5DdsBaaG4AnWEDzjxmfBzGy9AztQINu+g7ZxYYN1VRpuPXGQeRlHy4zQznvb3Tc48OoCrHvwZn8K5LP676t/tzQUgzvR/J88aRV9mu3N6MZFHvglpt3KD5XX8e3dYk7yJIovw2gSJtEiTtLJJI2i92biQf8RKPcD/J94MSQeAN3Shh85/wYAAP//AwBQSwMEFAAGAAgAAAAhAAtGahAbCwAABHAAAA8AAAB3b3JkL3N0eWxlcy54bWy8nV1z27oRhu870//A0VV7kcjyZ+I5zhnbiWtP4xyfyGmuIRKyUIOEyo/Y7q8vAFIS5CUoLrj1lS1R+wDEuy+I5Yf02+/PqYx+8bwQKjsbTd7vjSKexSoR2cPZ6Mf91bsPo6goWZYwqTJ+Nnrhxej3T3/9y29Pp0X5InkRaUBWnKbx2WhRlsvT8biIFzxlxXu15JneOFd5ykr9Mn8Ypyx/rJbvYpUuWSlmQoryZby/t3c8ajB5H4qaz0XMP6u4SnlW2vhxzqUmqqxYiGWxoj31oT2pPFnmKuZFoXc6lTUvZSJbYyaHAJSKOFeFmpfv9c40PbIoHT7Zs/+lcgM4wgH214A0Pr15yFTOZlKPvu5JpGGjT3r4ExV/5nNWybIwL/O7vHnZvLJ/rlRWFtHTKStiIe51yxqSCs27Ps8KMdJbOCvK80Kw1o0L80/rlrgonbcvRCJGY9Ni8V+98ReTZ6P9/dU7l6YHW+9Jlj2s3uPZux9TtyfOWzPNPRux/N303ASOmx2r/zq7u3z9yja8ZLGw7bB5yXVmTY73DFQKk8j7Rx9XL75XZmxZVaqmEQuo/66xYzDiOuF0+k1rF+itfP5VxY88mZZ6w9nItqXf/HFzlwuV60w/G320beo3pzwV1yJJeOZ8MFuIhP9c8OxHwZPN+39e2Wxt3ohVlen/D04mNgtkkXx5jvnS5L7emjGjyTcTIM2nK7Fp3Ib/ZwWbNEq0xS84MxNANHmNsN1HIfZNROHsbTuzerXv9lOohg7eqqHDt2ro6K0aOn6rhk7eqqEPb9WQxfw/GxJZwp9rI8JmAHUXx+NGNMdjNjTH4yU0x2MVNMfjBDTHk+hojieP0RxPmiI4pYp9Wegk+4En27u5u48RYdzdh4Qw7u4jQBh394Qfxt09v4dxd0/nYdzds3cYd/dkjefWS63oRtssKwe7bK5UmamSRyV/Hk5jmWbZqoiGZw56PCfZSQJMPbM1B+LBtJjZ17szxJo0/HhemkIuUvNoLh6qXBfTQzvOs19c6rI2YkmieYTAnJdV7hmRkJzO+ZznPIs5ZWLTQU0lGGVVOiPIzSV7IGPxLCEevhWRZFJYJ7SunxfGJIIgqVMW52p41xQjmx++imL4WBlIdFFJyYlY32hSzLKG1wYWM7w0sJjhlYHFDC8MHM2ohqihEY1UQyMasIZGNG51flKNW0MjGreGRjRuDW34uN2LUtop3l11TPqfu7uUypzHHtyPqXjImF4ADD/cNOdMozuWs4ecLReROSvdjnX3GdvOhUpeonuKY9qaRLWutylyqfdaZNXwAd2iUZlrzSOy15pHZLA1b7jFbvUy2SzQrmnqmWk1K1tNa0m9TDtlsqoXtMPdxsrhGbYxwJXICzIbtGMJMvibWc4aOSlmvk0vh3dswxpuq9ezEmn3GiRBL6WKH2mm4euXJc91WfY4mHSlpFRPPKEjTstc1bnmWn7fStLL8l/S5YIVwtZKW4j+h/rVFfDoli0H79CdZCKj0e3Lu5QJGdGtIK7vb79G92ppykwzMDTAC1WWKiVjNmcC//aTz/5O08FzXQRnL0R7e050esjCLgXBQaYmqYSIpJeZIhMkx1DL+yd/mSmWJzS0u5zXN52UnIg4ZemyXnQQeEvPi096/iFYDVnev1guzHkhKlPdk8Cc04ZFNfs3j4dPdd9URHJm6I+qtOcf7VLXRtPhhi8TtnDDlwhWTX14MPlLsLNbuOE7u4Wj2tlLyYpCeC+hBvOodnfFo97f4cVfw1NS5fNK0g3gCkg2gisg2RAqWaVZQbnHlke4w5ZHvb+EKWN5BKfkLO8fuUjIxLAwKiUsjEoGC6PSwMJIBRh+h44DG36bjgMbfq9ODSNaAjgwqjwjPfwTXeVxYFR5ZmFUeWZhVHlmYVR5dvA54vO5XgTTHWIcJFXOOUi6A01W8nSpcpa/ECG/SP7ACE6Q1rS7XM3N0wgqq2/iJkCac9SScLFd46hE/slnZF0zLMp+EZwRZVIqRXRubXPAsZHb967tCrNPcgzuwp1kMV8omfDcs0/+WF0vT+vHMl5333aj12nPr+JhUUbTxfpsv4s53tsZuSrYt8J2N9g25ser51nawm55Iqp01VH4MMXxQf9gm9FbwYe7gzcria3Io56RsM3j3ZGbVfJW5EnPSNjmh56R1qdbkV1++Mzyx9ZEOOnKn3WN50m+k64sWge3NtuVSOvIthQ86cqiLatE53FsrhZAdfp5xh/fzzz+eIyL/BSMnfyU3r7yI7oM9p3/EubIjpk0bXvruyfAvG8X0b1mzj8rVZ+337rg1P+hrhu9cMoKHrVyDvpfuNqaZfzj2Hu68SN6zzt+RO8JyI/oNRN5w1FTkp/Se27yI3pPUn4EeraCRwTcbAXjcbMVjA+ZrSAlZLYasArwI3ovB/wItFEhAm3UASsFPwJlVBAeZFRIQRsVItBGhQi0UeECDGdUGI8zKowPMSqkhBgVUtBGhQi0USECbVSIQBsVItBGDVzbe8ODjAopaKNCBNqoEIE2ql0vDjAqjMcZFcaHGBVSQowKKWijQgTaqBCBNipEoI0KEWijQgTKqCA8yKiQgjYqRKCNChFoo9aPGoYbFcbjjArjQ4wKKSFGhRS0USECbVSIQBsVItBGhQi0USECZVQQHmRUSEEbFSLQRoUItFHtxcIBRoXxOKPC+BCjQkqIUSEFbVSIQBsVItBGhQi0USECbVSIQBkVhAcZFVLQRoUItFEhois/m0uUvtvsJ/iznt479vtfumo69d19lNtFHfRHrXrlZ/V/FuFCqceo9cHDA1tv9IOImRTKnqL2XFZ3ufaWCNSFzz8uu5/wcekDv3SpeRbCXjMF8MO+keCcymFXyruRoMg77Mp0NxKsOg+7Zl83EhwGD7smXevL1U0p+nAEgrumGSd44gnvmq2dcDjEXXO0EwhHuGtmdgLhAHfNx07gUWQm59fRRz3H6Xh9fykgdKWjQzjxE7rSEmq1mo6hMfqK5if0Vc9P6Cujn4DS04vBC+tHoRX2o8KkhjbDSh1uVD8BKzUkBEkNMOFSQ1Sw1BAVJjWcGLFSQwJW6vDJ2U8IkhpgwqWGqGCpISpMangow0oNCVipIQEr9cADshcTLjVEBUsNUWFSw8UdVmpIwEoNCVipISFIaoAJlxqigqWGqDCpQZWMlhoSsFJDAlZqSAiSGmDCpYaoYKkhqktqexZlS2qUwk44bhHmBOIOyE4gbnJ2AgOqJSc6sFpyCIHVEtRqpTmuWnJF8xP6qucn9JXRT0Dp6cXghfWj0Ar7UWFS46qlNqnDjeonYKXGVUteqXHVUqfUuGqpU2pcteSXGlcttUmNq5bapA6fnP2EIKlx1VKn1LhqqVNqXLXklxpXLbVJjauW2qTGVUttUg88IHsx4VLjqqVOqXHVkl9qXLXUJjWuWmqTGlcttUmNq5a8UuOqpU6pcdVSp9S4askvNa5aapMaVy21SY2rltqkxlVLXqlx1VKn1LhqqVNqT7U0ftr6ASbDtj9Ipj9cviy5+Q5u54GZpP4O0uYioP3gTbL+oSQTbHoSNT9J1bxtO9xcMKxbtIGwqXih24qbb0/yNNV8C+r6MR77HaivG/Z8VartyGYIVp9uhnRzKbT+3NZlz85+l2bIO/psJekco1o1Xwc/Nmm4q4e6PzNZ/2iX/ucmSzTgqfnBqrqnyTOrUXr7JZfyltWfVkv/RyWfl/XWyZ59aP7V9ln9/W/e+NxOFF7AeLsz9cvmh8M8411/I3xzBdubksYNLcNtb6cYOtKbvq3+Kz79DwAA//8DAFBLAQItABQABgAIAAAAIQDfpNJsWgEAACAFAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAhAB6RGrfvAAAATgIAAAsAAAAAAAAAAAAAAAAAkwMAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAhANZks1H0AAAAMQMAABwAAAAAAAAAAAAAAAAAswYAAHdvcmQvX3JlbHMvZG9jdW1lbnQueG1sLnJlbHNQSwECLQAUAAYACAAAACEAzZQZ+C0CAABWBgAAEQAAAAAAAAAAAAAAAADpCAAAd29yZC9kb2N1bWVudC54bWxQSwECLQAUAAYACAAAACEAqlIl3yMGAACLGgAAFQAAAAAAAAAAAAAAAABFCwAAd29yZC90aGVtZS90aGVtZTEueG1sUEsBAi0AFAAGAAgAAAAhANQzzdKRAwAAkwkAABEAAAAAAAAAAAAAAAAAmxEAAHdvcmQvc2V0dGluZ3MueG1sUEsBAi0AFAAGAAgAAAAhAOyYItrFAQAA7QQAABIAAAAAAAAAAAAAAAAAWxUAAHdvcmQvZm9udFRhYmxlLnhtbFBLAQItABQABgAIAAAAIQBbbf2TCQEAAPEBAAAUAAAAAAAAAAAAAAAAAFAXAAB3b3JkL3dlYlNldHRpbmdzLnhtbFBLAQItABQABgAIAAAAIQDbvgnxcAEAAMcCAAAQAAAAAAAAAAAAAAAAAIsYAABkb2NQcm9wcy9hcHAueG1sUEsBAi0AFAAGAAgAAAAhAIIxT0t/AQAAFQMAABEAAAAAAAAAAAAAAAAAMRsAAGRvY1Byb3BzL2NvcmUueG1sUEsBAi0AFAAGAAgAAAAhAAtGahAbCwAABHAAAA8AAAAAAAAAAAAAAAAA5x0AAHdvcmQvc3R5bGVzLnhtbFBLBQYAAAAACwALAMECAAAvKQAAAAA='





--------------
return mappings
---------------
