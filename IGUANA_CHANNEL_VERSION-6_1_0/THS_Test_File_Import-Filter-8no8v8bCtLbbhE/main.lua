-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Data)

   iguana.stopOnError(false)

   -- Parse the incoming message
   local MsgIn, MsgType = hl7.parse{vmd='dxc_ipm_tas.vmd', data=Data}
	
   local v_proceed = 'no'
   
   if MsgIn.MSH[9][1]:nodeValue() == 'ADT' then
      v_proceed = 'yes'
   elseif MsgIn.MSH[9][1]:nodeValue() == 'SIU' then
      v_proceed = 'yes'   
   elseif MsgIn.MSH[9][1]:nodeValue() == 'MFN' then
      v_proceed = 'yes'
   elseif MsgIn.MSH[9][1]:nodeValue() == 'REF' then
      v_proceed = 'yes'
   else
      v_proceed = 'no'
   end
   --
   if v_proceed == 'yes' then
      -- Prepare the outgoing message
      local MsgOut = hl7.message{vmd='dxc_ipm_tas.vmd', name=MsgType}

      -- Map the outgoing message
      MsgOut:mapTree(MsgIn)
      
      queue.push{data=MsgOut:S()}
      -- queue.push{data=Data}   
   end     
end