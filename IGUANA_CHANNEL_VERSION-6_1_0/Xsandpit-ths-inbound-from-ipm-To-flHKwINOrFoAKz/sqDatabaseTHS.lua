--[[

Author: Peter Branagan
Date: Oct 2023

All database activity needs be executed from this modules, separated from any other scripts


]]


local sqDBTHS = {

   patientData = {
      
      mrn = '',
      localmrn = '',
      medicareNumber = '',
      firstname = '',
      surname = '',
      user_type = '',
      user_status = '',
      dob = '',
      gender = '',   
      home_phone = '',
      mobile = '',
      email = '',
      mobileint = '',
      address1 = '',
      address2 = '',
      address3 = '',
      address4 = '',
      address5 = '',
      title = '',
      parent_mobile = ''
      
   },
   
   nokData = {
      
      nk1_userID = '',
      nk1_parent = '',
      nk1_firstname = '',
      nk1_surname = '',
      nk1_rel_code =  '',
      nk1_rel_desc =  '',
      nk1_rel_id = '',        
      nk1_mobile = '',
      nk1_mobile_format = '',
      nk1_phone = '',
      nk1_address_1 = '',
      nk1_address_2 = '',
      nk1_address_4 = '',
      nk1_address_5 = '',
      nk1_eircode = '',
      nk1_email = ''
      
   }

}

-----------------------------------------------------------------
function sqDBTHS.execute(val)
   
   --[[
         NOTE: **** The audit table being written to this function 
                     will grow, will need a script to purge monthly ****
   ]]
   
   if _G.dbAudit == '1' then 

      --log all inserts and updates to the iguana db audit table

      local channel = iguana.channelName()
      local sql = val.sql:gsub("\\'","'")
      sql = sql:gsub("\n","")
      local msgid = _G.msgCtrlID
      local insert = val.sql:upper():find('INSERT INTO')
      if insert ~= nil then insert = true end
      local update = val.sql:upper():find('UPDATE ')
      if update ~= nil then update = true end
      local delete = val.sql:upper():find('DELETE FROM ')
      if delete ~= nil then delete = true end

      if insert or update or delete then
         
         --[[
         conn:execute{sql="insert into iguana_db_audit (channel_name, msgid, sql_str) values ("..    
            conn:quote(channel)..","..
            conn:quote(msgid)..","..
            conn:quote(sql)..")"
            , live=false}
         ]]
         
         local dt = os.date("%Y-%m-%d %H:%M:%S")
         local line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
         
         if not iguana.isTest() then
            --sqUtils.printLog("Writing SQL statement to DB Audit File..")
            local filePath = "/root/iguana_db_audit.txt"
            local file, err = io.open(filePath, "a")
            if not file then
               iguana.logError("Error opening file: " .. err)
               return
            end

            file:write(line .. "\n") -- Add a newline character to separate lines
            file:close()
         end
         
         
      end
   end
   
   
   --execute the SQL caommand
   
   local res = conn:execute(val)
     
   return res
   
end

----------------------------------------------------------------------------

function sqDBTHS.fillPatientData(PID, mrn)
   
   sqUtils.printLog("Loading patient data...")
   
   sqDBTHS.patientData.mrn = mrn
   sqDBTHS.patientData.firstname = PID[5][2]:nodeValue()
   sqDBTHS.patientData.surname = PID[5][1]:nodeValue()
   sqDBTHS.patientData.user_type = "N"
   sqDBTHS.patientData.dob = sqUtils.parseDOB(PID[7])
   sqDBTHS.patientData.gender = PID[8]:nodeValue()   
   sqDBTHS.patientData.home_phone = ''
   sqDBTHS.patientData.mobile = ''
   sqDBTHS.patientData.email = ''
   sqDBTHS.patientData.title = PID[5][5]:S()
   sqDBTHS.patientData.Home_Address = PID[11][1][5]:S()  
   sqDBTHS.patientData.home_phone = sqDBTHS.getPhoneNumberByType(PID,'PH')
   sqDBTHS.patientData.mobile = sqDBTHS.getPhoneNumberByType(PID,'CP')
   sqDBTHS.patientData.email = sqDBTHS.getPhoneNumberByType(PID,'Internet')
   
   sqDBTHS.patientData.mobileint = ''  
   if sqDBTHS.patientData.mobile:sub(1,2) == '04' then
      sqDBTHS.patientData.mobileint = '61' .. sqDBTHS.patientData.mobile:sub(2)
   else
      sqDBTHS.patientData.mobileint = sqDBTHS.patientData.mobile
   end
   
   sqDBTHS.patientData.address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   sqDBTHS.patientData.address2 = PID[11][1][2]:nodeValue()
   sqDBTHS.patientData.address3 = PID[11][1][3]:nodeValue()
   sqDBTHS.patientData.address4 = PID[11][1][4]:nodeValue()
   sqDBTHS.patientData.address5 = '' 
   
   local mcCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode')
   sqDBTHS.patientData.medicareNumber = getMRNbyAssigningFacility(PID,mcCode)   

end

-----------------------------------------------------------------
function sqDBTHS.fillNOKData(patientID, NK1)

   sqUtils.printLog("Loading NOK data...")
   
   sqDBTHS.nokData.nk1_userID = ''
   sqDBTHS.nokData.nk1_parent = patientID
   sqDBTHS.nokData.nk1_firstname = NK1[1][2][2]:S()
   sqDBTHS.nokData.nk1_surname = NK1[1][2][1]:S()
   
   sqDBTHS.nokData.nk1_rel_code =  NK1[1][3][1]:S()
   sqDBTHS.nokData.nk1_rel_desc =  NK1[1][3][2]:S()
   if sqDBTHS.nokData.nk1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDBTHS.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDBTHS.nokData.nk1_rel_code).." or description = "..conn:quote(sqDBTHS.nokData.nk1_rel_desc), live=true} 
   sqDBTHS.nokData.nk1_rel_id = res[1].id:S()
   
   sqDBTHS.nokData.nk1_phone = sqDBTHS.getNKPhoneNumberByType(NK1,'PH')
   sqDBTHS.nokData.nk1_mobile = sqDBTHS.getNKPhoneNumberByType(NK1,'CP')   
   sqDBTHS.nokData.nk1_email = sqDBTHS.getNKPhoneNumberByType(NK1,'Internet')
  
   if sqDBTHS.nokData.nk1_mobile:sub(1,2) == '04' then
      sqDBTHS.nokData.nk1_mobile_format = '61' .. sqDBTHS.nokData.nk1_mobile:sub(2)
   else
      sqDBTHS.nokData.nk1_mobile_format = sqDBTHS.nokData.nk1_mobile
   end 
   
   sqDBTHS.nokData.nk1_address_1 = NK1[1][4][1]:S()
   sqDBTHS.nokData.nk1_address_2 = NK1[1][4][2]:S()
   sqDBTHS.nokData.nk1_address_3 = NK1[1][4][3]:S()
   sqDBTHS.nokData.nk1_address_4 = NK1[1][4][4]:S()
   sqDBTHS.nokData.nk1_eircode = NK1[1][4][5]:S()

end

-----------------------------------------------------------------
function sqDBTHS.createPatient(PID, mrn, NK1, IN1)
   
   sqUtils.printLog("Creating patient "..mrn.." ...")
   
   sqDBTHS.fillPatientData(PID,mrn)
   
      sqDBTHS.execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created) " ..
         "values (trim(replace("..
         conn:quote(sqDBTHS.patientData.mobileint)..", ' ', '')), "..
         conn:quote(sqDBTHS.patientData.home_phone)..","..
         conn:quote(sqDBTHS.patientData.email)..","..
         conn:quote(sqDBTHS.patientData.mrn)..","..
         conn:quote(sqDBTHS.patientData.firstname)..","..
         conn:quote(sqDBTHS.patientData.surname)..","..
         conn:quote(sqDBTHS.patientData.user_type)..","..
         conn:quote(sqDBTHS.patientData.dob)..","..
         conn:quote(sqDBTHS.patientData.gender)..","..
         conn:quote(sqDBTHS.patientData.address1)..","..
         conn:quote(sqDBTHS.patientData.address2)..","..
         conn:quote(sqDBTHS.patientData.address3)..","..
         conn:quote(sqDBTHS.patientData.address4)..","..
         conn:quote(sqDBTHS.patientData.address5)..","..
         conn:quote(sqDBTHS.patientData.Home_Address)..","..
         conn:quote(sqDBTHS.patientData.title)..","..
         "'active', now()"..
         ")"
         , live=false}
   
   local result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
   local patientID = result[1].id:S()

   --Insert medicare number
   sqDBTHS.insertMedicareNumber(patientID)
   
   --add update insurance details
   sqDBTHS.addUpdateInsurance(IN1,patientID)
   
   sqUtils.printLog("Created patient with id: [" ..patientID.."]")
   --[[
   IR-43: 09/01/2023: Chris Ireland: Process optional NK1 segment
   If an NK1 segment is present:
   Add a user record to record details of the related person
   Add a user_additional_info record to register the NoK contact status
   Assertion: No NoK user record can already exist for that patient and relationship as patient record is being created
   ** Limitation: Only the first NK1 segment is processed. **
   ]]--      

   if NK1 ~= nil then
      if not (NK1:isNull()) then
         
         sqDBTHS.fillNOKData(patientID, NK1)
         
         local sql =       
            "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
            "Address_1, Address_2, Home_Address, Address_4, created) " ..
            "values ( " ..
            conn:quote(sqDBTHS.nokData.nk1_parent)..",".. 
            conn:quote(sqDBTHS.nokData.nk1_rel_id) .. "," ..
            "trim(replace(" .. conn:quote(sqDBTHS.nokData.nk1_mobile_format) .. ", ' ', '')), " ..
            conn:quote(sqDBTHS.nokData.nk1_phone)..","..
            -- conn:quote(email)..","..
            conn:quote(sqDBTHS.nokData.nk1_firstname)..","..
            conn:quote(sqDBTHS.nokData.nk1_surname)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_1)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_2)..","..
            conn:quote(sqDBTHS.nokData.nk1_eircode)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_4)..","..
            "now()"..
            ")"

         sqDBTHS.execute{sql=sql, live=false}
         
         local nk1_user_result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}

         sqUtils.printLog("Added new related person record into users table, with id [" ..nk1_user_result[1].id:S() .. 
            "], for patient id [" ..sqDBTHS.nokData.nk1_parent .. "]")

         -- Update related persons users_additional_info record to be next of kin if contact role is NOK
         if (NK1[7][1]:S() == 'NOK') then
            sqDBTHS.execute{sql=
               "insert into users_additional_info (user_id, act_as_nok)" ..
               "values (" ..conn:quote(nk1_user_result[1].id:S()) .. ", '1')", live=false }

            local nk1_userAddInfo_id = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
            
            sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_userAddInfo_id[1].id:S().."], for userID: [" ..nk1_user_result[1].id:S().."]")
         end
      end  
   end
   
   return result[1].id:S()
   
end

-----------------------------------------------------------------
function sqDBTHS.updatePatient(id, PID, mrn, NK1, IN1)
   
   sqUtils.printLog("Updating patient "..mrn.." ...")
   
   sqDBTHS.fillPatientData(PID, mrn)
     
      local sql ="update users " ..
      "set "..
      "mrn = " .. conn:quote(sqDBTHS.patientData.mrn) .. ", "..
      "firstname = " .. conn:quote(sqDBTHS.patientData.firstname) .. ", "..
      "surname = " .. conn:quote(sqDBTHS.patientData.surname) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDBTHS.patientData.dob) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDBTHS.patientData.mobileint) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDBTHS.patientData.home_phone) .. ", "..
      "email = " .. conn:quote(sqDBTHS.patientData.email) .. ", "..
      "Gender = " .. conn:quote(sqDBTHS.patientData.gender) .. ", "..
      "Address_1 = " .. conn:quote(sqDBTHS.patientData.address1) .. ", "..
      "Address_2 = " .. conn:quote(sqDBTHS.patientData.address2) .. ", "..
      "Address_3 = " .. conn:quote(sqDBTHS.patientData.address3) .. ", "..
      "Address_4 = " .. conn:quote(sqDBTHS.patientData.address4) .. ", "..
      "Address_5 = " .. conn:quote(sqDBTHS.patientData.address5) .. ", "..
      "Home_Address = " .. conn:quote(sqDBTHS.patientData.Home_Address) .. ", "..
      "title = " .. conn:quote(sqDBTHS.patientData.title) .. ", "..
      "modified = now() " ..
      "where id = ".. id
   
      sqDBTHS.execute{sql=sql, live=false}

   --Insert medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDBTHS.insertMedicareNumber(id)
   
   --add update insurance details
   sqUtils.printLog("Updating insurance details for patient: "..id)
   sqDBTHS.addUpdateInsurance(IN1,id)

   --update NOK details
   if NK1 ~= nil then
      
      if not (NK1:isNull()) then
         
         sqDBTHS.fillNOKData(id, NK1)    
         
         local NKUserQueryResultSet = sqDBTHS.execute{sql=
            "select count(id), id from users " ..
            "where parent = " .. id .. 
            " and relationship = " .. conn:quote(sqDBTHS.nokData.nk1_rel_id), live=true}

         -- Update existing or create new related person record in user table
         if NKUserQueryResultSet[1]['count(id)']:S() ~= '0' then

            sqDBTHS.nokData.nk1_userID = NKUserQueryResultSet[1]['id']:S()

            sqUtils.printLog("Updating related person details from NK1 segment into users table for patient id: " .. id)

            local sql ="update users " ..
            "set "..
            "firstname = " .. conn:quote(sqDBTHS.nokData.nk1_firstname) .. ", "..
            "surname = " .. conn:quote(sqDBTHS.nokData.nk1_surname) .. ", "..
            "mobile = trim(replace(" .. conn:quote(sqDBTHS.nokData.nk1_mobile_format) .. ", ' ', '')), "..
            "home_phone = " .. conn:quote(sqDBTHS.nokData.nk1_phone) .. ", "..
            "Address_1 = " .. conn:quote(sqDBTHS.nokData.nk1_address_1) .. ", "..
            "Address_2 = " .. conn:quote(sqDBTHS.nokData.nk1_address_2) .. ", "..
            "Home_Address = " .. conn:quote(sqDBTHS.nokData.nk1_eircode) .. ", "..
            "Address_4 = " .. conn:quote(sqDBTHS.nokData.nk1_address_4) .. ", "..
            "Address_5 = " .. conn:quote(sqDBTHS.nokData.nk1_address_5) .. 
            " where id = ".. sqDBTHS.nokData.nk1_userID

            sqDBTHS.execute{sql=sql, live=false}

         elseif NKUserQueryResultSet[1]['count(id)']:S() == '0' then

            sqUtils.printLog("Adding related person details from NK1 segment into users table for patient: " .. id)

            local sql = 
            "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
            "Address_1, Address_2, Home_Address, Address_4, Address_5, created) " ..
            "values ( " ..
            conn:quote(sqDBTHS.nokData.nk1_parent)..",".. 
            conn:quote(sqDBTHS.nokData.nk1_rel_id) .. "," ..
            "trim(replace(" .. conn:quote(sqDBTHS.nokData.nk1_mobile_format) .. ", ' ', '')), "..
            conn:quote(sqDBTHS.nokData.nk1_phone)..","..
            -- conn:quote(email)..","..
            conn:quote(sqDBTHS.nokData.nk1_firstname)..","..
            conn:quote(sqDBTHS.nokData.nk1_surname)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_1)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_2)..","..
            conn:quote(sqDBTHS.nokData.nk1_eircode)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_4)..","..
            conn:quote(sqDBTHS.nokData.nk1_address_5)..","..
            "now()"..
            ")"

            sqDBTHS.execute{sql=sql, live=false}

            local nk1_user_resultSet = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
            sqDBTHS.nokData.nk1_userID = nk1_user_resultSet[1].id:S()

            sqUtils.printLog("Added new related person record into users table, with id [" ..sqDBTHS.nokData.nk1_userID .. 
               "], for patient ID [" ..sqDBTHS.nokData.nk1_parent .. "]")


         end -- End users record added/updated in users table

         -- If related person is NOK, 
         -- then update existing, or create new, user_additional_info record
         -- and set act_as_nok to '1' (true)

         if (NK1[1][7][1]:S() == 'NOK') then
            
            local NKUserAddInfoResultSet = sqDBTHS.execute{sql=
               "select count(id), id from users_additional_info " ..
               "where user_id = " .. sqDBTHS.nokData.nk1_userID, live=true}

            -- If users_additional_info record exists, update it
            if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
               local sql = "update users_additional_info " ..
               "set act_as_nok = '1' " .. 
               "where id = ".. sqDBTHS.nokData.nk1_userID

               sqDBTHS.execute{sql=sql, live=false}

               sqUtils.printLog("Added NOK category status to users_additional_info for user ID [" .. sqDBTHS.nokData.nk1_userID .. "]")

               -- else if users_additional_info record does not exist, then create it
            elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

               local sql = "insert into users_additional_info (user_id, act_as_nok) " ..
               "values (" ..conn:quote(sqDBTHS.nokData.nk1_userID) .. ", '1')"

               sqDBTHS.execute{sql=sql, live=false}

               local nk1_act_as_nok_result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}

               sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_act_as_nok_result[1].id:S()..
                  "], for userID: [" ..sqDBTHS.nokData.nk1_userID.."]")

            end            
         end -- End NK1[7][1] == NOK
      end
   end -- End NK1 processing
   
   --check if we need to decease / un-decease the patient
   trace(PID[29]:S())
   if PID[29]:S() ~= '' and PID[29]:S() ~= '""' then
      
      sqDBTHS.processDeceasedPatient(id, PID)
      
   end
   
   
end

-----------------------------------------------------------------
function sqDBTHS.processDeceasedPatient(id, PID)
   
   --update the date of death in users_additional_info
   
   if PID == nil or id == '0' then
      return
   end

   local res = sqDBTHS.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      id, live=true}
   
   trace(PID[29]:S())
   local dod = dateparse.parse(PID[29]:D())
   local dodString = string.sub(tostring(dod),1,10)
   
   if res[1].cnt:S() == '0' then
      
      conn:execute{sql="insert into users_additional_info (user_id, deceased_date) " ..
         "values ("..
         id .. "," ..
         conn:quote(dodString)..
         ")"
         , live=false}
   else
      conn:execute{sql="update users_additional_info set " ..
         "deceased_date = "..conn:quote(dodString)..
         " where user_id = "..id
         , live=false}
   end
   
   sqUtils.printLog("DOD details updated!")
   
   --ensure communications are switched off
   
   --switch off emails
   
   local res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,2,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,2,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,2,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,2,0)"
      , live=false}
   end
   
   -- switch off SMS
   
   local res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,1,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,1,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,1,0)"
      , live=false}
   end
   
   res = conn:execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = conn:execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      conn:execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,1,0)"
      , live=false}
   end
  
end

-----------------------------------------------------------------
function sqDBTHS.getPatientIdByMrn(mrn, facilityID)
   
   local PatientId = sqDBTHS.execute{sql="select count(id), id from users where mrn = " .. conn:quote(mrn) ..
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = "..facilityID..")", live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      return '0'
   else
      return PatientId[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDBTHS.mergePatient(id, minorid, facilityID)
   
   sqUtils.printLog("Merging patient...")
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = sqDBTHS.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request")
   end

   local isMinorPatientValid = false
   if (minorid ~= '0') then
      trace(minorid)
      local facilityUserCheck = sqDBTHS.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request")
   end
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   --Referrals First
   local MergeRefs = sqDBTHS.execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = sqDBTHS.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}
      --
      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{HMC HL7 Merge - Minor ID: " .. minorid .. " Major ID: " .. id ..
         "ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"
         sqUtils.printLog("mergePatient().Insert merged user details: " .. sql)
         sqDBTHS.execute{sql=sql, live=false}
      end
      --
      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID:S()) ..
      " and user_id = " .. minorid
      sqUtils.printLog(" mergePatient(). Update ewaiting_referrals: " .. sql)
      sqDBTHS.execute{sql=sql, live=false}
      --
   end
   --
   --Reservations Second
   local MergeRes = sqDBTHS.execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}
   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = sqDBTHS.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}
      --
      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         sqDBTHS.execute{sql=sql, live=false}
      end
      --
      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID:S()) .. 
      " or parent_id = " .. conn:quote(facilityID:S()) .. ")" .. 
      " and user_id = " .. minorid
      sqDBTHS.execute{sql=sql, live=false}
      --
   end
   --
   --Patient Flow Third
   local MergePatFlow = sqDBTHS.execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = sqDBTHS.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}
      --
      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         sqDBTHS.execute{sql=sql, live=false}
      end
      --
      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID:S()) .. 
      " or parent_id = " .. conn:quote(facilityID:S()) .. ")"..
      " and user_id = " .. minorid
      sqDBTHS.execute{sql=sql, live=false}
      --
   end
   --
   -- Remove the minor one from the Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID:S()) .. 
   " and user_id = " .. minorid
   sqDBTHS.execute{sql=sql, live=false}
   --
end   

-----------------------------------------------------------------
function sqDBTHS.createAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)

   sqUtils.printLog("Creating appointment...")

   --extract the res date and time
   --local mrn = SCH[1][1]:S()
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   local DXCId = SCH[2][1]:S()
   --local apptType = PV2[12]:S()
   local apptType = SCH[8][1]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local refID = SCH[4][1]:S()
   local DXCSession = SCH[5][1]:S()

   local appDuration = SCH[9]:S()
   if appDuration  == '' then
      appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   end

   -- 
   --Check if an appointment already exists for this patient
   local ResId = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " 
      .. conn:quote(opdAppointmentString) .. " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then

      --insert the new appointment
      sqDBTHS.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, "..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by,".. 
         "is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
         --    conn:quote(opdAppointmentEndTimeString)..","..
         conn:quote(clinCode)..","..
         reasonID..","..
         conn:quote(refID)..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"
         , live=false}

      local result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}

      --Add A user audit record for the app creation
      sqDBTHS.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),22, 'appointment.created',"..
         result[1].id:S()..","..id..",null)"
         , live=false}      

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'REASON'"..","..
         conn:quote(DXCReason)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'AIPID'"..","..
         conn:quote(DXCAIPID)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'DXCSPEC'"..","..
         conn:quote(DXCSpeciality)..
         ")"
         , live=false}

      sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'SESSION'"..","..
         conn:quote(DXCSession)..
         ")"
         , live=false}

   else
      sqDBTHS.execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..", google_cal_id = " ..conn:quote(clinCode).. 
         ", hl7_id = "..conn:quote(refID)..", source = 'h' where id = "..ResId[1].id, live=false}   

      -- check if the reservation_Ipm records exist - if not create them
      local IPMResId = sqDBTHS.execute{sql="select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
      --
      if IPMResId[1]['count(res_id)']:S() == '0' then

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'REASON'"..","..
            conn:quote(DXCReason)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'AIPID'"..","..
            conn:quote(DXCAIPID)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'DXCSPEC'"..","..
            conn:quote(DXCSpeciality)..
            ")"
            , live=false}

         sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'SESSION'"..","..
            conn:quote(DXCSession)..
            ")"
            , live=false}
         --
      end
      --
   end
end

-----------------------------------------------------------------
function sqDBTHS.updateAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)
   
   sqUtils.printLog("Updating appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   --local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local refID = SCH[4][1]:S()
   local DXCId = SCH[2][1]:S()
   local apptType = SCH[8][1]:S()
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local DXCSession = SCH[5][1]:S()
   
   local appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   
   --local ResId = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}
   local ResId = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " .. conn:quote(opdAppointmentString) ..
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then
      
      --check if an existing appointment exists
      local ResIdUpd = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
         sqDBTHS.execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
           , live=false}
      end
      
      --insert the new appointment
      sqDBTHS.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, reservation_time,"..
           "reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by, is_attended,booking_user_id, source, print,stats, "..
           "quality,capacity_sync, is_urgent) " .. 
            "values ("..
            facilityID..","..
            id..","..
            "'f',0"..","..
            conn:quote(DXCId).."," ..
            conn:quote(opdAppointmentString)..","..
            conn:quote(opdAppointmentTimeString)..","..
            "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
            --conn:quote(opdAppointmentEndTimeString)..","..
            conn:quote(clinCode)..","..
            reasonID..","..
            conn:quote(refID)..","..
            "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
            ")"
            , live=false}  
      
            local result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
      
            --Add A user audit record for the app creation
            sqDBTHS.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.created',"..
                 result[1].id:S()..","..id..",null)"
            , live=false}
      
            sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
             ")"
            , live=false}
      
            sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
             ")"
            , live=false}
      
            sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
             ")"
            , live=false}
      
            sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'REASON'"..","..
               conn:quote(DXCReason)..
               ")"
               , live=false}

               sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'AIPID'"..","..
               conn:quote(DXCAIPID)..
               ")"
               , live=false}

               sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'DXCSPEC'"..","..
               conn:quote(DXCSpeciality)..
               ")"
               , live=false}
         
               sqDBTHS.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'SESSION'"..","..
               conn:quote(DXCSession)..
               ")"
               , live=false}
      --
    else 
      sqDBTHS.execute{sql="update reservation set " .. 
            "reservation_date = " .. conn:quote(opdAppointmentString)..","..
            "reservation_end_time = substring(convert("..conn:quote(opdAppointmentTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
            --"reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
            "google_cal_id = " .. conn:quote(clinCode)..","..
            "hl7_id = " .. conn:quote(refID)..","..
            "modified = now(), modified_source = 'h'"..","..
            "reservation_time = " .. conn:quote(opdAppointmentEndTimeString)..
            " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
            "user_id = " .. id
           , live=false}
      
            --Add A user audit record for the app creation
            sqDBTHS.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.rescheduled',"..
                 conn:quote(ResId[1]['id']:S())..","..id..",null)"
            , live=false}      
      
    end
    --
end

-----------------------------------------------------------------
function sqDBTHS.updatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   
   sqUtils.printLog("Updating patient flow...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   local appReason = 1
   
   local ResId = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0'
      then
      --
      local ResId2 = sqDBTHS.execute{sql="select count(id), id from patient_flow where stage = 'CHECK-IN' and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}
      
      if ResId2[1]['count(id)']:S() == '0' then
         --insert the new patient flow
         sqDBTHS.execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..
            "9919,'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}
       end
    end
    --
end

-----------------------------------------------------------------
function sqDBTHS.cancelAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("Cancelling appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local ResIdUpd = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDBTHS.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where location_pref = " .. conn:quote(DXCId).." and " ..
         "user_id = " .. id
         , live=false}
      
      --Add A user audit record for the app creation
      sqDBTHS.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.cancelled',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDBTHS.dnaAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("DNA appointment...")
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = sqDBTHS.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDBTHS.execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      sqDBTHS.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDBTHS.getPatientReferral(id,facilityID,controlID)
   
   sqUtils.printLog("getting patient referral...")
   
   local referralCount = sqDBTHS.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. id.." and control_id = "..conn:quote(controlID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return referralCount[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDBTHS.createPatientReferral(id,facilityID, clinCode, RF1, PRD)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   --PB 06.12.23 : account class
   local accClass = RF1[5][1]:S() --
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDBTHS.execute
   {sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}

   --if the refarral already exists then skip
   if referralCount[1]['count(id)']:S() ~= '0' then
      sqUtils.printLog("Referral already exists, Skipping creation...")
      --return
   end
   
   --referral does not exist so continue
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   local refExpiry = RF1[8]:S()
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   local refDateReferred = ''
 
   if not isEmpty(RF1[9]:S()) then
      refDateReferred = sqUtils.parseDate(RF1[9])
      trace(refDateReferred)
   end
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   local refPriorityName = RF1[2][2]:S()
   
   local refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
   
   local referralPriority = sqDBTHS.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   
   local referralSpeciality = sqDBTHS.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   
   local refCondition = sqDBTHS.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end
   
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
      
   sql="insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, \
   speciality_id, referral_source_id,"..   "referral_source_name, status, `condition`, control_id, \
   comments, created, created_by) " ..
   "values ("..
   facilityID..","..
   id..","..
   conn:quote(refDateReferred)..","..
   facilityCategory..","..
   facilitySpeciality..","..
   FacilitySourceId..","..
   conn:quote(refSourceName)..","..
   conn:quote(waitingStatus)..","..
   conn:quote(facilityConditionTag)..","..
   conn:quote(refID)..","..
   "'HL7 Referral'"..","..
   "now()"..","..
   "22"..
   ")"

   sqDBTHS.execute{sql=sql, live=false}
   sqUtils.printLog("createPatientReferral: " .. sql)

   local result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
   
   if facilityConditionTag ~= 0 then
      sql = "insert into ewaiting_referral_condition_tags values (" .. result[1].id .."," .. facilityConditionTag .. ")"
      sqUtils.printLog("createPatientReferral:" ..sql)
      sqDBTHS.execute{sql=sql, live=false}
   else
      sqUtils.printLog("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition:S().." not found!")
      iguana.logWarning("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition:S().." not found!")
   end
   
   sqDBTHS.addUpdateReferralExpiryDate(facilityID:S(), result[1].id:S(), refExpiry)
   
   sqUtils.printLog("Creation of referral completed!") 
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end
  
-----------------------------------------------------------------
function sqDBTHS.addUpdateReferralExpiryDate(facId, refId, expDate)

   local res = sqDBTHS.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Expiry Date';", live=true}

   if #res == 0 then
      error("Expiry Date not mapped on ewaiting_custom_fields for facility "..facId)
   end
   
   local customId = res[1]:S()

   
   res = sqDBTHS.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
      conn:quote(refId).." and custom_field_id = "..customId, live=true}
   
   if #res == 0 then
      
      --insert new expiry date
      sqDBTHS.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,custom_field_id) '..
         ' values ('..refId..', """'..expDate..
         '""", '..customId..') ', live=false}
   else
      
      --update the expiry date
      local id = res[1]:S()
      res = sqDBTHS.execute{sql='update ewaiting_referral_custom_field_values '..
         ' SET value = """'..expDate..
         '""" where id = '..id, live=false}
   end   
   
end

-----------------------------------------------------------------
function sqDBTHS.updatePatientReferral(id,facilityID, clinCode, RF1, PRD)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDBTHS.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so update
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
     
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   local refPriorityName = RF1[2][2]:S()
   --
   local referralPriority = sqDBTHS.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --date referred
   local refDateReferred = sqUtils.parseDate(RF1[9])
   
   --referral source name
   local refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
   
   --
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   --
   local referralSpeciality = sqDBTHS.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   --
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   --
   local refCondition = sqDBTHS.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end

   sqDBTHS.execute{sql="update ewaiting_referrals set "..
      "facility_id = "..facilityID..","..
      "user_Id = "..id..","..
      "category_id = "..facilityCategory..","..
      "speciality_id = "..facilitySpeciality..","..
      "referral_source_id = "..FacilitySourceId..","..
      "date_referred = "..conn:quote(refDateReferred)..","..
      "referral_source_name = "..conn:quote(refSourceName)..","..
      "modified = now(), modified_by = 22 "..
      "where id = ".. referralCount[1]['id']:S(), live=false}
   
   sqDBTHS.addUpdateReferralExpiryDate(facilityID:S(), referralCount[1]['id']:S(), refExpiry)
   
   sqUtils.printLog("Updating of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDBTHS.dischargePatientReferral(id, facilityID, refID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local referralCount = sqDBTHS.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by = 2304, "..
   "status = 'discharged'" ..   " where id = ".. referralID
   conn:execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   -- ***** HANDLE DISCHARGE REASON AND MAY NEED TO ADD RIP AS ONE *****
   
   -- ***** MAY NEED TO CANCEL ANY OPEN APPOINTMENTS HERE *******

   sqUtils.printLog("Discharging of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDBTHS.createGPDetails(patientID, PD1, faciliityID)
   
   sqUtils.printLog("Creating GP details...")
   
   --extract the relevant GP details
   local gpFirstname = ''
   local gpOrgID = ''
   local gpID = ''
   local gpPractice = ''
   local gpFullName = ''
   
   gpFirstname = PD1[4][3]:S()
   gpSurname = PD1[4][2]:S()
   gpPractice = PD1[3][1]:S()
   gpID = PD1[4][1]:S()
   gpFullName = gpFirstname.." "..gpSurname
  
   local gpCount = sqDBTHS.execute{sql="select count(id), id from gp_details"..
      " where medical_council_number = " .. conn:quote(gpID) .. 
      " and firstname = " .. conn:quote(gpFirstname) ..
      " and surname = " .. conn:quote(gpSurname), live=true}
   
   if gpCount[1]['count(id)']:S() == '0' then
      
      --create the new GP
      sqDBTHS.execute{sql="insert into gp_details (id,firstname,surname,address_1,medical_council_number)"..
         " values (null, " .. conn:quote(gpFirstname)..","..conn:quote(gpSurname)..","..
         conn:quote(gpPractice)..","..conn:quote(gpID)..")", live=false}

      local newGPID1 = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
      local newGPID = newGPID1[1].id:S()
      
      --now insert it into the facility_gp table
      sqDBTHS.execute{sql="insert into facility_gp (id,facility_id,gp_details,hospital_code,gmc_code,dh_code,m_number)"..
         " values (null,"..conn:quote(faciliityID)..","..conn:quote(newGPID)..",0,0,0,0)", live=false}

      conn:commit{live=true}
      --
      --now update the patient details
      sqDBTHS.execute{sql="update users set gp_details = "..conn:quote(newGPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
   end
   --
end

-----------------------------------------------------------------
function sqDBTHS.getFacilityIDClinic(clinCode, hospitalCode)
   
   local clinicCount = sqDBTHS.execute{sql="select count(id), facility_id from facility_clinic_codes"..
      " where clinic_code = " .. conn:quote(clinCode) .. 
      " and hospital_code = " .. conn:quote(hospitalCode), live=true}
   
   if clinicCount[1]['count(id)']:S() == '0' then
      return '0'
   else
      return clinicCount[1].facility_id:S()
   end
   
end

-----------------------------------------------------------------
function sqDBTHS.createClinicCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq, clinicCode, clinicDesc)
   
   sqUtils.printLog("Creating clinic capacity...")

   -- first check if we have a template clinic in the env for ipM
   local paramCount = sqDBTHS.execute{sql="select count(id), param_value from application_parameter"..
      " where param_name = 'ipm.parent'", live=true}
   
   if paramCount[1]['count(id)']:S() == '0' then
      error('Application parameter for ipm.parent is not setup. Please add before proceeding.')
   else
      -- find a relevant template clinic to create teh details from
      local templateCount = sqDBTHS.execute{sql="select count(id), id from facilities where parent_id = ".. conn:quote(paramCount[1]['param_value']:S())..
      " and duration = " .. conn:quote(sessionDur), live=true}
      
      if templateCount[1]['count(id)']:S() == '0' then
         error('No template found matching the clinic duration')
      else
         local templateFID = templateCount[1]['id']:S()
         --
         local newFID = createNewFacility(templateFID, clinicDesc)
         --
         createClinicCode(newFID, clinicCode)
         --
         createNewAllowableSlots(newFID, templateFID)
         --
         updateCapacity(newFID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
         --
      end
   end
end

-----------------------------------------------------------------
function sqDBTHS.updateCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
   
   sqUtils.printLog("Updating clinic capacity...")
   --
   -- Assumption here is that the entries are already set up in aloable_slots
   -- This is JUST an update to existing capacity
   
   local start_time1 = sessionStart:sub(1,2)
   local start_time2 = sessionStart:sub(3,4)
   local start_time = start_time1..':'..start_time2
   --
   local end_time1 = sessionEnd:sub(1,2)
   local end_time2 = sessionEnd:sub(3,4)
   local end_time = end_time1..':'..end_time2
   
   local ipm_day1 = days:split(',')[1]
   local ipm_day2 = days:split(',')[2]
   local ipm_day3 = days:split(',')[3]
   local ipm_day4 = days:split(',')[4]
   local ipm_day5 = days:split(',')[5]
   local ipm_day6 = days:split(',')[6]
   local ipm_day7 = days:split(',')[7]
   
   --********NEED to cater for change in durations********---
   local checkDur = checkClinicDur(facilityID, sessionDur)
   
   --********NEED to add in audit trail of changes********---
      
   --if the durations match then it's a straighforward update
   if checkDur == '1' then
      --reset Calendar Days
      sqDBTHS.execute{sql="update calendar_days " .. 
         " set monday = 'N', tuesday = 'N', wednesday = 'N'," ..
         " thursday = 'N', friday = 'N', saturday = 'N'," ..
         " sunday = 'N'"..
         " where facility_id = " .. facilityID, live=false}
      
      --reset capacity slots back to 0
      sqDBTHS.execute{sql="update allowable_slots set allowable = 0" ..
         " where facility_id = " .. facilityID
         , live=false}

      conn:commit{live=true} -- need to commit to prevent conflict with update
   else
      --we will need to create new allowable slots
   end
   --
   local day1 = setAllowableDays(facilityID, ipm_day1)
   local day2 = setAllowableDays(facilityID, ipm_day2)
   local day3 = setAllowableDays(facilityID, ipm_day3)
   local day4 = setAllowableDays(facilityID, ipm_day4)
   local day5 = setAllowableDays(facilityID, ipm_day5)
   local day6 = setAllowableDays(facilityID, ipm_day6)
   local day7 = setAllowableDays(facilityID, ipm_day7)
    
   --Update the relevant sessions
   if day1 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day1)
   end
   --
   if day2 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day2)
   end
   --
   if day3 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day3)
   end
   --
   if day4 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day4)
   end
   --
   if day5 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day5)
   end
   --
   if day6 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day6)
   end
   --
   if day7 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day7)
   end
end

-----------------------------------------------------------------
function sqDBTHS.setAllowableDays(facilityID, day)
   
   sqUtils.printLog("Setting allowable days...")
   
   --
   if day == 'MON' then
      sqDBTHS.execute{sql="update calendar_days set monday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      
      return 'Mon'
      --
   elseif day == 'TUE' then
      sqDBTHS.execute{sql="update calendar_days set tuesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Tue'
      --
   elseif day == 'WED' then
      sqDBTHS.execute{sql="update calendar_days set wednesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Wed'
      --
   elseif day == 'THU' then
      sqDBTHS.execute{sql="update calendar_days set thursday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Thu'
      --
   elseif day == 'FRI' then
      sqDBTHS.execute{sql="update calendar_days set friday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Fri'
      --
   elseif day == 'SAT' then
      sqDBTHS.execute{sql="update calendar_days set saturday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sat'
      --
   elseif day == 'SUN' then
      sqDBTHS.execute{sql="update calendar_days set sunday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sun'
      --
   else
      return '0'
   end
end

-----------------------------------------------------------------
function sqDBTHS.updateAllowableSlots(facilityID, start_time, end_time, day)
   
   sqUtils.printLog("Update allowable slots...")
   --
   --readjust end_time so that it is not included in the update
   sqDBTHS.execute{sql="update allowable_slots set allowable = 1" ..
      " where facility_id = " .. facilityID ..
      " and (time >= " .. conn:quote(start_time) .. 
      " and time < " .. conn:quote(end_time) .. ")" ..
      " and date = " .. conn:quote(day), live=false}
   --
end

-----------------------------------------------------------------
function sqDBTHS.checkClinicDur(facilityID, sessionDur)
   --
   local clinicDur = sqDBTHS.execute{sql="select count(id), duration from facilities"..
      " where id = " ..conn:quote(facilityID), live=true}
   
   if clinicDur[1]['duration']:S() == sessionDur then
      return '1'
   else
      return clinicDur[1].duration:S()
   end

end

-----------------------------------------------------------------
function sqDBTHS.createNewFacility(templateFID, clinicDesc)
   
   sqUtils.printLog("Creating new facility...")
        
         --**************
         --- WHERE DO I GET THE PARENT ID from
         --**************
         
         -- now create the clinic in the facilities table
         local createFID = sqDBTHS.execute{sql="insert into facilities (id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,firstname,"..
            "surname,short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,fkey,hl7_endpoint,"..
            "search_ip_restricted) "..
         "select null as id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,".. conn:quote(clinicDesc).." as firstname,"..
            "surname,".. conn:quote(clinicDesc).. " as short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,null as fkey,hl7_endpoint,"..
            "search_ip_restricted from facilities where id = " .. templateFID, live=true}
         
         local newFID1 = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
         local newFID = newFID1:S()
         --
         --create the calendar days entries
         local createFIDDays = sqDBTHS.execute{sql="insert into calendar_days (id,timestamp,facility_id,monday,"..
            "tuesday,wednesday,thursday,friday,saturday,sunday) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,monday,tuesday,wednesday,thursday,friday,saturday,"..
            "sunday from calendar_days where facility_id = ".. templateFID, live=true}
         
         --create the calendar actions entries
         local createFIDActions = sqDBTHS.execute{sql="insert into calendar_actions " ..
            "(id,timestamp,facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule from calendar_actions where facility_id = ".. templateFID, live=true}
         
         --create facility_config
         local createFIDConfig = sqDBTHS.execute{sql="insert into facility_config " ..
            "(id,timestamp,facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen) "..
        "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen from facility_config where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
   return newFID
   
end

-----------------------------------------------------------------
function sqDBTHS.createNewAllowableSlots(newFID, templateFID)
   
   sqUtils.printLog("Creating new allowable slots...")
   
   --create the calendar days entries
   local createAllowableSlots = sqDBTHS.execute{sql="insert into allowable_slots (id,timestamp,facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id) "..
      "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id from allowable_slots where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDBTHS.createClinicCode(newFID, sessionCode)
   
   sqUtils.printLog("Creating clinic code...")
   
   --create the facility_clinic_code record
   local createClinicCodes = sqDBTHS.execute{sql="insert into facility_clinic_codes (id,hospital_group_id,facility_id,clinic_code,"..
      "hospital_code,reason_id,active,created_at,updated_at,waiting_status,condition_id,speciality_id,category_id,"..
      "source_id,opd_video) values (null, 14, ".. conn:quote(newFID)..","..conn:quote(sessionCode)..",'NMH',null,0,now(), null,"..
      "'pre_triage',0,0,0,0,0)", live=true}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDBTHS.createSlotDateRange(facilityID, startDate, endDate)
   
   sqUtils.printLog("Creating slot date range...")
   
   --create a new record for allowable_slots_date_ranges
   local createSlotDateRanges = sqDBTHS.execute{sql="insert into allowable_slots_date_ranges (id,facility_id,"..
      "start_date,end_date,active,created_at,updated_at) values ".. 
      "(null,"..conn:quote(facilityID), ","..conn:quote(startDate)..","..conn:quote(endDate)..","..
      "1, now(), now())", live=true}
   
   conn:commit{live=true}
   
   local result = sqDBTHS.execute{sql="select last_insert_id() as id", live=true}
   --
   return result
   --
end

-----------------------------------------------------------------
function sqDBTHS.updateWaitList(patientID,waitListID,referralID, facilityID)
   
   sqUtils.printLog("Updating waitng list...")
   
   local referralCount = sqDBTHS.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(referralID), live=true}
   trace(waitListID)
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      local createSlotDateRanges = sqDBTHS.execute{sql="update ewaiting_referrals set covid_19_id = "..conn:quote(waitListID)..
      " where id = "..conn:quote(referralCount[1]['id']:S()), live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------
function sqDBTHS.userInFacility(patientId, facilityId)
   
   if facilityId ~= nil and facilityId ~= '' then
   
       local result = sqDBTHS.execute{sql="select id from facility_user where "..
       " user_id = " .. patientId .. " "..
       " and facility_id = " .. facilityId .. ";"
       , live=true}
   
     return result[1].id:nodeValue()
   else
      return nil
   end
end

-----------------------------------------------------------------
function sqDBTHS.addToFacility(patientId, facilityId)
	if sqDBTHS.userInFacility(patientId, facilityId) == ''
   then
	   sqDBTHS.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------

--UTILS:

-----------------------------------------------------------------------------

--return the phone number from the PID based on type
function sqDBTHS.getPhoneNumberByType(PID, pType)
   for i=1, #PID[13] do
      if PID[13][i][3]:S() == pType then
         return PID[13][i][1]:S()
      end
   end
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type
function sqDBTHS.getNKPhoneNumberByType(NK1, pType)
   if NK1:childCount() > 1 then
      for i=1, #NK1[5] do
         if NK1[5][i][3]:S() == pType then
            return NK1[5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type, SMS , L14. L15 etc
function sqDBTHS.getNKPhoneNumberByType(NK1, pType)

   if NK1:childCount() >=1 then
      for i=1, #NK1[1][5] do
         if NK1[1][5][i][3]:S() == pType then
            return NK1[1][5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

-----------------------------------------------------------------------------

function sqDBTHS.insertMedicareNumber(patientID)
   if patientID ~= '0' then
      local res = sqDBTHS.execute{sql="select count(user_id) as cnt, user_id from user_medical_number_reference where user_id = "..patientID, live=true}
      if res[1].cnt:S() == '0' then
         conn:execute{sql="insert into user_medical_number_reference (user_id, reference_type, medical_number) " ..
            "values ("..
            patientID .. "," ..
            conn:quote(sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode'))..","..
            conn:quote(sqDBTHS.patientData.medicareNumber)..
            ")"
            , live=false}
      end
   end
end

-----------------------------------------------------------------------------

function sqDBTHS.addUpdateInsurance(IN1, patientID)

   if IN1 == nil or patientID == '0' then
      return
   end
   
   local insurance_code = IN1[2][1]:S()
   local insurance_policy = IN1[36]:S()

   local res = sqDBTHS.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      patientID, live=true}
   
   if res[1].cnt:S() == '0' then
      conn:execute{sql="insert into users_additional_info (user_id, insurance_code, policy_no) " ..
         "values ("..
         patientID .. "," ..
         conn:quote(insurance_code)..","..
         conn:quote(insurance_policy)..
         ")"
         , live=false}
   else
      conn:execute{sql="update users_additional_info set " ..
         "insurance_code = "..conn:quote(insurance_code)..","..
         "policy_no = "..conn:quote(insurance_policy)..
         " where user_id = "..patientID
         , live=false}
   end
   
   sqUtils.printLog("Insurance details updated!")

end

----------------------------------------------------------------------------
function sqDBTHS.getParentFacilityId(facilityID)

   local parentFID = sqDBTHS.execute{sql="select parent_id from facilities where id = "..facilityID, live=true}

   if parentFID[1]['parent_id']:S() ~= '0' then
      parentFacilityID = parentFID[1]['parent_id']:S()
   else
      parentFacilityID = facilityID
   end

   return parentFacilityID

end



return sqDBTHS