-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
require 'dateparse'
require 'patient-utils'
require 'common-utils'
require 'tas_config'

--PB 20.10.23
map = require('mappings')

local dbfile = ClientConfig['DbFile']

local raw_db_credentials = readFile(dbfile)


dbo = raw_db_credentials:split('\n')

local db_name = dbo[1]
local db_user = dbo[2]
local db_pass = dbo[3]

conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}


function main(Data)

   iguana.stopOnError(false)
   
   local status = iguana.status()
   local res = xml.parse{data=status}
   local msgsQueued = res.IguanaStatus:child("Channel", 9).MessagesQueued:S()
   if tonumber(msgsQueued) > 40 then
      print("stop processing...")
      net.http.respond{body='{"success": true}', code=200}
      return
   end

   -- log the request
   iguana.logDebug(Data)

   if not conn or not conn:check()
      then
      if conn
         then 
         conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}
      end
      conn:close()
      conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}
   end

   conn:begin{}

   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}


   -- Parse the HTTP request
   local Request = net.http.parseRequest{data=Data}

   -- Only accept POST requests
   if Request.method ~= 'POST' then
      net.http.respond{body='{"success": false, "message": "Method not allowed"}', code=405}
      return
   end

   -- Parse the incoming JSON
   local Payload = json.parse{data=net.http.parseRequest{data=Data}.body}

   -- Convert each appointment from JSON into a HL7 message
   for Key,Value in pairs(Payload)
   do
      local Message = hl7.message{vmd='dxc_ipm_tas.vmd', name='SIUS12'}

      local CheckIn = Value.patient_flow.check_in_time

      local proceed = 'Yes'

      --
      if proceed == 'Yes' then
         if Value.appointment.source == 'h' then
            if CheckIn == 'false' then
               if Value.appointment.modified_source == 'b' then
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'c' then
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'o' then      
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'p' then      
                  proceed = 'Yes'
               else
                  proceed = 'No'
               end
            end
         else
            proceed = 'Yes'
         end
         --
      end


      --
      if Value.appointment.modified_source == 'h' then
         if CheckIn == 'false' then
            proceed = 'No'
         end
      end
      --

      local CheckIn = Value.patient_flow.check_in_time
      local CheckInStage = Value.patient_flow.current_stage
--[[
      --it has already been sent at TRIAGE
      if Value.appointment.facility_id == 14291 then
         if CheckInStage == 'TRIAGE' then
            proceed = 'Yes'
         end
         --
         if CheckInStage == 'TRIAGE-CALL-IN' then
            proceed = 'Yes'
         end
         --
         if CheckInStage == 'CHECK-IN' then   
            proceed = 'No'
         end
      end
      --
      --it has already been sent at TRIAGE
      if Value.appointment.facility_id == 9947 then
         if CheckInStage == 'TRIAGE' then
            proceed = 'Yes'
         end
         --
         if CheckInStage == 'TRIAGE-CALL-IN' then
            proceed = 'Yes'
         end
         --
         if CheckInStage == 'CHECK-IN' then   
            proceed = 'No'
         end
      end
      --
 ]]--
      
      trace(proceed)
      if proceed == 'Yes' then

         Message = map.mapMSH(Message, Value.appointment, Value.patient_flow)
         if CheckIn == 'false' then     
            Message = map.mapSCH(Message, Value.appointment, Value.referral)
         else
            Message = map.mapEVN(Message, Value.appointment, Value.patient_flow)
         end
         Message = map.mapPID(Message, Value.patient, Value.patient_flow)
         Message = map.mapPV1(Message, Value.appointment, Value.patient_flow, Value.referral)
         Message = map.mapPV2(Message, Value.appointment, Value.patient_flow, Value.referral)
         if CheckIn == 'false' then
            Message = map.mapAIL(Message, Value.appointment, Value.patient_flow)
            Message = map.mapAIP(Message, Value.appointment, Value.patient_flow)
         end 

         -- Push each appointment to the queue
         queue.push{data=Message:S()}
         conn:commit{live=true}

      end  
   end

   -- Respond to the client
   net.http.respond{body='{"success": true}', code=200}

end





function addMinutesToDate(strDate, mins)

   --[[
   Exmaple usage:
   Accepts a date string in the format "yyyyMMddHHmmss"
   dt = "20230704123000"
   --Add 5 minutes
   local res = addMinutesToDate(dt,5)
   --subtract 5 minutes
   local res = addMinutesToDate(dt,-5)
   --Add a day
   local res = addMinutesToDate(dt,60*24)
   ]]--


   local year = tonumber(strDate:sub(1, 4))
   local month = tonumber(strDate:sub(5, 6))
   local day = tonumber(strDate:sub(7, 8))
   local hour = tonumber(strDate:sub(9, 10))
   local minute = tonumber(strDate:sub(11, 12))
   local second = tonumber(strDate:sub(13, 14))

   local timestamp = os.ts.time({
         year = year,
         month = month,
         day = day,
         hour = hour,
         min = minute,
         sec = second
      })

   local new_timestamp = timestamp + (mins * 60)
   local nt = os.ts.date("*t",new_timestamp)

   return nt.year..string.format("%02d",nt.month)..string.format("%02d",nt.day)..
   string.format("%02d",nt.hour)..string.format("%02d",nt.min)..string.format("%02d",nt.sec)

end

function isEmpty(s)
   return s == nil or s == '' or type(s) == 'userdata'
end

-------------------------------------------------------------------------
--PB 20.10.23
function getClinicConfigMapping(key, ...)
      
   --needs 2 parameters, facility id and reason id
   if select('#', ...) <= 2 then
      local facilityId, reasonId = select(1,...)
      
      local res = conn:execute{sql=
            "select "..key.." from facility_clinic_codes where facility_id = "..facilityId.." and reason_id = "..reasonId,
            live=true}
      return res[1][key]:S()
    
   else
      return ''
   end  
   
end

-------------------------------------------------------------------------
--PB 20.10.23
function getPriorityCode(appId,referral)

   local res = conn:execute{sql=
      "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
      "from reservation_ipm " ..
      "where ipm_name = 'PRIORITY' and res_id = " .. appId
      , live=true}

   if tonumber(res[1].cnt:S()) >= 1 then
      return res[1]['ipm_value']:S()
   else

      -- look for the referral priority
      res = conn:execute{sql="select count(*) as cnt, IFNULL(priority,'') as priority " ..
         "from ewaiting_categories " ..
         "where id = " .. referral.category_id 
         , live=true}

      if tonumber(res[1].cnt:S()) >= 1 then
         return res[1]['priority']:S()
      else

         --the appointment was not created on IPM therefore entry wont exist
         --So use internal map
         return getJsonMappedValue(ClientConfig.priorityMap,'default')
      end

   end
   
end

-------------------------------------------------------------------------
--PB 20.10.23
function getSpecialtyCode(appId, facilityId, reasonId)
   
   local res = conn:execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      local v = getClinicConfigMapping('speciality_id', facilityId, reasonId)
      return getJsonMappedValue(ClientConfig.specialtyMap,v)
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function getAttendingDoctor(appId, facilityId)
   
   local res = conn:execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return getJsonMappedValue(ClientConfig.attendingDoctorMap,facilityId)
   
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function getAppointmentType(appId)
   
   local res = conn:execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'TYPE' " ..
         "and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return getJsonMappedValue(ClientConfig.appTypeMap,'default')
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function getAppointmentReason(appId)
   
   local res = conn:execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return getJsonMappedValue(ClientConfig.appReasonMap,'default')
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function getAppointmentCategory(appId)
   
   local res = conn:execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return getJsonMappedValue(ClientConfig.appCategoryMap,'default')
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function getPatientStatusCode(appId)
   
   return getJsonMappedValue(ClientConfig.patientStatusCodeMap,'default')

end

-------------------------------------------------------------------------
--PB 20.10.23
function getScheduleIdentifier(appId)
   
   return getJsonMappedValue(ClientConfig.scheduleIdentifierMap,'default')

end

-------------------------------------------------------------------------
--PB 20.10.23
function getJsonMappedValue(jsonStr, val )

   local default = ''
   local res = nil
   local tbl = json.parse{data=jsonStr}
   for k, v in pairs(tbl) do
      if k == val then
         return v
      end
      if k == 'default' then
         default = v
      end
   end
   
   if default ~= '' then return default end
   
   return ''
end
