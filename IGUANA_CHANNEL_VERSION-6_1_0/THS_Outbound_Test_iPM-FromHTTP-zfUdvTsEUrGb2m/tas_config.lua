-- Implementation details for Tas
ClientConfig = {

   --mapping SQ specialty code to IPM code
   specialtyMap = [[ 
      { 
        '660' : 'ORTHOP',
        'default' : 'ORTHOP'
      }
   ]],
   
   priorityMap = [[ 
      { 
        'default' : '1'
      }
   ]],
   
   appTypeMap = [[ 
      { 
        'default' : 'OTPAT'
      }
   ]],
   
   patientStatusCodeMap = [[ 
      { 
        'default' : '5'
      }
   ]],
   
   scheduleIdentifierMap = [[ 
      { 
        'default' : 'AM-SQ-ORTHO'
      }
   ]],
   
   appReasonMap = [[ 
      { 
        'default' : 'NSP'
      }
   ]],
   
   appCategoryMap = [[ 
      { 
        'default' : 'PUB'
      }
   ]],
   
   attendingDoctorMap = [[ 
      { 
        '102' : '21167',
        'default' : '21167'
      }
   ]],
   
   defaultPatientClass = 'O',
   defaultLocationType = 'CLINIC'

}

ClientConfig['DbFile'] = '/etc/iNTERFACEWARE/test_au_db_credentials'
ClientConfig['facility'] = {}
ClientConfig['facility']['TAS'] = 12

