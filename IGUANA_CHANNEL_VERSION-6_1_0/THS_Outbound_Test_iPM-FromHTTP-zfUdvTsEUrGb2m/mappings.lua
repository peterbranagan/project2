mappings = {

   adt = {},
   orm = {},
   siu = {}

}

----------------------------------------------------------------------------
function mappings.mapMSH(Message, Appointment, PatientFlow)

   Message.MSH[3][1] = 'Swiftqueue'
   Message.MSH[4][1] = 'NWA'
   Message.MSH[5][1] = 'IIEIPM'
   Message.MSH[6][1] = 'NWA'
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')
   Message.MSH[15] = 'NE'
   Message.MSH[16] = 'AL'

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S13'
      else
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S15'
      end
   elseif Appointment.modified_source == 'c' then
      if Appointment.status == 'active' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S13'
      else
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S15'
      end   
   else
      Message.MSH[9][1] = 'SIU'
      Message.MSH[9][2] = 'S12'
   end
   
   if PatientFlow.check_in_time ~= 'false' then
      Message.MSH[9][1] = 'ADT'
      Message.MSH[9][2] = 'A04'
   end

   if PatientFlow.complete_time ~= 'false' then
      Message.MSH[9][1] = 'ADT'
      Message.MSH[9][2] = 'A03'
   end
   
   if Appointment.is_attended == 'no' then
      if PatientFlow.current_stage == 'DNA' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S26'
      end
   end

   Message.MSH[10] = util.guid(128)

   Message.MSH[12] = '2.4'


   --[[ ####Removed temporarily - validate during testing ####
   -- google_cal_id used to store clin_code only
   if Message.MSH[9][2] == 'S12' then
   conn:execute{sql="update reservation set " .. 
   "google_cal_id = " .. conn:quote(Message.MSH[10]:S())..
   " where id = " .. Appointment.id
   , live=false}
end
   ]]--

   return Message

end

function mappings.mapEVN(Message, Appointment, PatientFlow)

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.EVN[1] = 'S13'
      else
         Message.EVN[1] = 'S15'
      end
   else
      Message.EVN[1] = 'S12'
   end
   --
   if PatientFlow.check_in_time ~= 'false' then
      Message.EVN[1] = 'A04'
   end
   --
   if PatientFlow.complete_time ~= 'false' then
      Message.EVN[1] = 'A03'
   end
   --
   Message.EVN[2] = os.ts.date('!%Y%m%d%H%M%S')

   return Message

end

function mappings.mapSCH(Message, Appointment, Referral)

   local sqApptID = ""
   local extApptID = ""

   if not isEmpty(Appointment.id) then
      sqApptID = Appointment.id
   end
   if not isEmpty(Appointment.location_pref) then
      extApptID = Appointment.location_pref
   end

   Message.SCH[1][1] = sqApptID
   Message.SCH[1][2] = 'SWIFTQ'

   Message.SCH[2][1] = extApptID
   Message.SCH[2][2] = 'OAM'

   Message.SCH[5][1] = getScheduleIdentifier(Appointment.id)

   -- #### End Temp

   --Populate the Referral ID
   --[[
   local findReferral = conn:execute{sql=
      "select count(id), hl7_id from reservation where id = " .. conn:quote(Appointment.id)
      , live=true}

   if findReferral[1]['count(id)']:S() ~= '0' and hl7_id ~= NULL then
      Message.SCH[4][1] = findReferral[1]['hl7_id']:S()
      Message.SCH[4][2] = 'REF'
   end
   ]]--
        
   if type(Referral) ~= 'userdata' and Referral.control_id ~= NULL then
      Message.SCH[4][1] = Referral.control_id
      Message.SCH[4][2] = 'REF'          
   end

   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. Appointment.id
      , live=true}

   --[[
   if ResId[1]['parent_id']:S() ~= '0'
      then
      local AppReason = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      Message.SCH[7][1] = AppReason[1]['ipm_value']:S()

   else
      local AppReason = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. Appointment.id, live=true}
      Message.SCH[7][1] = AppReason[1]['ipm_value']:S()

   end
   ]]
   
   --PB replace above block, overkill
   local appId = Appointment.id
   if ResId[1]['parent_id']:S() ~= '0' then
      appId = ResId[1]['parent_id']:S()
   end
   
   Message.SCH[7][1] = getAppointmentReason(appId)

   -- Message.SCH[7][1] = 'OUTA'
   Message.SCH[7][2] = 'Follow Up'
   Message.SCH[9] = Appointment.duration
   Message.SCH[10][1] = 'MIN'
   Message.SCH[11][3] = Appointment.duration
   Message.SCH[11][4] = Appointment.start
   Message.SCH[11][5] = addMinutesToDate(Appointment.start, Appointment.duration)
   
   --PB 20.10.23 : already done above
   -- get the details from the DB
   --[[
   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}
	]]
   
   --[[
   if ResId[1]['parent_id']:S() ~= '0' then
      local AppPriority = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      Message.SCH[11][6] = AppPriority[1]['ipm_value']:S()
   else
            local AppPriority = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. AppId
         , live=true}
      Message.SCH[11][6] = AppPriority[1]['ipm_value']:S()      
   end
   ]]
   
   --PB 20.10.23
   --Message.SCH[11][6] = getPriorityCode(appId, Referral)
   
   --PB 07.11.23
  --hard code to 1 - will be replced anyway
      Message.SCH[11][6] = '1'
   --
   
   

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = 'Rescheduled'
      else
         Message.SCH[25][1] = 'Cancelled'
      end
   else
      Message.SCH[25][1] = 'New'
   end
   
   if Message.MSH[9][2]:S() == 'S26' then
      Message.SCH[25][1] = 'DNA'
   end
   
   return Message
end

function mappings.mapPID(Message, Patient, PatientFlow)

   Message.PID[1] = 1 

   local pidIndex = 1
   if not isEmpty(Patient.mrn) then
      Message.PID[3][pidIndex][1] = Patient.mrn
      Message.PID[3][pidIndex][5] = 'PAS'
      pidIndex = pidIndex + 1
   end
   if not isEmpty(Patient.id) then
      Message.PID[3][pidIndex][1] = Patient.id
      Message.PID[3][pidIndex][5] = 'SQID'
   end

   Message.PID[5][1] = Patient.surname
   Message.PID[5][2] = Patient.firstname
   Message.PID[7] = Patient.birth_year_field .. Patient.birth_month_field .. Patient.birth_date_field
   Message.PID[8] = Patient.Gender

   return Message
end  

function mappings.mapPV1(Message, Appointment, PatientFlow, Referral)

   Message.PV1[1] = 1
   Message.PV1[2] = ClientConfig.defaultPatientClass
   
   --Message.PV1[3][1] = 'SQ-TEST1'  -- ##### Needs correct value - test only ####
   --PB 20.10.23
   Message.PV1[3][1] = getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
   
   if not isEmpty(Appointment.google_cal_id) then
      Message.PV1[3][1] = Appointment.google_cal_id   
   end
   
   Message.PV1[5][1] = ''
   if (type(Referral) ~= 'userdata') and (Referral.covid_19_id ~= '') then
      Message.PV1[5][1] = Referral.covid_19_id
   end
   
   --Message.PV1[7][1] = '21167' -- ##### Needs correct value - test only ####
   --PB 20.10.23
   res = getAttendingDoctor(Appointment.id, Appointment.facility_id)
   --
   
   --Message.PV1[10] = 'ORTHOP' -- ##### Needs correct value - test only ####
   --PB 20.10.23
   Message.PV1[10] = getSpecialtyCode(Appointment.id, Appointment.facility_id, Appointment.reason_id)
   --
   
   local AppId = Appointment.id
   local fID = Appointment.facility_id       
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}
   
   --[[
   if ResId[1]['parent_id']:S() ~= '0' then
      local AppCategory = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      if not isEmpty(AppCategory[1]['ipm_value']:S()) then
         Message.PV1[18] = AppCategory[1]['ipm_value']:S()
      end
   else
      local AppCategory = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. AppId
         , live=true}
      if not isEmpty(AppCategory[1]['ipm_value']:S()) then
         Message.PV1[18] = AppCategory[1]['ipm_value']:S()
      end      
   end
   ]]
   
   --PB 20.10.23 : replace above block, overkill
   if ResId[1]['parent_id']:S() ~= '0' then
      AppId = ResId[1]['parent_id']:S()
   end
   Message.PV1[18] = getAppointmentCategory(AppId)
   --
   
   Message.PV1[19][1] = Appointment.location_pref
   
   --
   -- only check and send these values when the app is complete
   if Message.MSH[9][2]:S() == 'S26' then
      Message.PV1[36] = '0'
   elseif PatientFlow.complete_time ~= 'false' then
      --
      local AttendId = conn:execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' " ..
         "and field_name = 'ATTND (Attended Status)'", live=true}

      local OutcomeId = conn:execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' and field_name = 'SCOCM (Outcome)'", live=true}   
      --
      if AttendId[1]['id']:S() ~= '0' then
         local AttendCategory = conn:execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. AttendId[1]['id']:S() ..
            " and reservation_id = " .. AppId, live=true}
         Message.PV1[14] = AttendCategory[1]['value_str']:S()
      end
      --
      if OutcomeId[1]['id']:S() ~= '0' then
         local OutcomeCategory = conn:execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. OutcomeId[1]['id']:S() ..
            " and reservation_id = " .. AppId, live=true}
         Message.PV1[36] = OutcomeCategory[1]['value_str']:S()
      end 
   end
   --
   if PatientFlow.complete_time ~= 'false' then     
      Message.PV1[45] = PatientFlow.complete_time
   elseif PatientFlow.check_in_time ~= 'false' then
      Message.PV1[44] = PatientFlow.check_in_time
   else
      Message.PV1[44] = Appointment.start
   end

   return Message
end

--[[
function mappings.mapPV2_OLD(Message, Appointment, PatientFlow)
 
	Message.PV2[8] = Appointment.start
   Message.PV2[9] = addMinutesToDate(Appointment.start, Appointment.duration)
   
   -- get the details from the DB
   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}

   if ResId[1]['parent_id']:S() ~= '0' then
      local AppType = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'TYPE' " ..
         "and res_id = " .. ResId[1]['parent_id']:S(), live=true}
      Message.PV2[12] = AppType[1]['ipm_value']:S()


      local AppPriority = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      Message.PV2[25] = AppPriority[1]['ipm_value']:S()
   else
      local AppType = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'TYPE' and res_id = " .. AppId
         , live=true}
      Message.PV2[12] = AppType[1]['ipm_value']:S()


      local AppPriority = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. AppId
         , live=true}
      Message.PV2[25] = AppPriority[1]['ipm_value']:S()      
   end

   -- #### Temporary hard-coded value to support demo testing
   Message.PV2[24] = '5'
   
   
   
   return Message
end 
--]]

--[[
function mappings.mapAIP_OLD(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIP[1] = 1

   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId
      , live=true}

   Message.AIP[3][1] = '21167' -- ##### Needs correct value - test only ####
   Message.AIP[4][1] = 'ORTHOP' -- ##### Needs correct value - test only ####
  
   if ResId[1]['parent_id']:S() ~= '0' then
      local AIPID = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      if not isEmpty(AIPID[1]['ipm_value']:S()) then
         Message.AIP[3][1] = AIPID[1]['ipm_value']:S()
      end


      local AIPSPEC = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      if not isEmpty(AIPSPEC[1]['ipm_value']:S()) then
         Message.AIP[4][1] = AIPSPEC[1]['ipm_value']:S()
      end
   else
      local AIPID = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. AppId
         , live=true}
      if not isEmpty(AIPID[1]['ipm_value']:S()) then
         Message.AIP[3][1] = AIPID[1]['ipm_value']:S()
      end


      local AIPSPEC = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. AppId
         , live=true}
      if not isEmpty(AIPSPEC[1]['ipm_value']:S()) then
         Message.AIP[4][1] = AIPSPEC[1]['ipm_value']:S()
      end     
   end

   return Message
end
--]]

--[[
function mappings.mapAIL_OLD(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIL[1] = 1
   Message.AIL[2] = 'A'

   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id, IFNULL(google_cal_id,'') as google_cal_id " ..
      "from reservation " ..
      "where id = " .. AppId
      , live=true}

   Message.AIL[3][1] = 'SQ-TEST1' -- ##### Needs correct value - test only ####
   
   if not isEmpty(ResId[1]['google_cal_id']:S()) then
      Message.AIL[3][1] = ResId[1]['google_cal_id']:S()
   end

   Message.AIL[4][1] = 'CLINIC'

   Message.AIL[5][1] = 'ORTHOP' -- ##### Needs correct value - test only ####
   
   if ResId[1]['parent_id']:S() ~= '0' then

      local AIPSPEC = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. ResId[1]['parent_id']:S()
         , live=true}
      if not isEmpty(AIPSPEC[1]['ipm_value']:S()) then
         Message.AIL[5][1] = AIPSPEC[1]['ipm_value']:S()
      end
   else

      local AIPSPEC = conn:execute{sql=
         "select count(*), IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. AppId
         , live=true}
      if not isEmpty(AIPSPEC[1]['ipm_value']:S()) then
         Message.AIL[5][1] = AIPSPEC[1]['ipm_value']:S()
      end      
   end

   Message.AIL[12][1] = 'BOOKED'

   return Message
end 
--]]

--PB 20.10.23
-------------------------------------------------
function mappings.mapPV2(Message, Appointment, PatientFlow, Referral)
 
	Message.PV2[8] = Appointment.start
   Message.PV2[9] = addMinutesToDate(Appointment.start, Appointment.duration)
   
   -- get the details from the DB
   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}

   if ResId[1]['parent_id']:S() ~= '0' then
      AppId = ResId[1]['parent_id']:S()
   end
     
   Message.PV2[12] = getAppointmentType(AppId)
   
   --PB 07.11.23
   --hard code to 1, being replaced anyway
   --Message.PV2[25] = getPriorityCode(AppId, Referral)
   Message.PV2[25] = '1'

   Message.PV2[24] = getPatientStatusCode(AppId)
     
   return Message
end 

--PB 20.10.23
--------------------------------------------------
function mappings.mapAIP(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIP[1] = 1

   local AppId = Appointment.id
   
   local Res = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId
      , live=true}
   
   if Res[1]['parent_id']:S() ~= '0' then
      AppId = Res[1]['parent_id']:S()
   end
   
   Message.AIP[3][1] = getAttendingDoctor(AppId)
   Message.AIP[4][1] = getSpecialtyCode(AppId, Appointment.facility_id, Appointment.reason_id)

   return Message
end

--PB 20.10.23
--------------------------------------------------
function mappings.mapAIL(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIL[1] = 1
   Message.AIL[2] = 'A'
 
   local AppId = Appointment.id
   local ResId = conn:execute{sql=
      "select IFNULL(parent_id,0) as parent_id, IFNULL(google_cal_id,'') as google_cal_id " ..
      "from reservation " ..
      "where id = " .. AppId
      , live=true}
   
   if not isEmpty(ResId[1]['google_cal_id']:S()) then
      Message.AIL[3][1] = ResId[1]['google_cal_id']:S()
   else
      -- retrive from clinic table
      Message.AIL[3][1] = getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
   end

   Message.AIL[4][1] = ClientConfig.defaultLocationType
   
   if ResId[1]['parent_id']:S() ~= '0' then
      AppId = ResId[1]['parent_id']:S()
   end
   
   Message.AIL[5][1] = getSpecialtyCode(AppId, Appointment.facility_id, Appointment.reason_id)

   Message.AIL[12][1] = 'BOOKED'

   return Message
end 

function getAppPriorityByAppId(AppId)
   local AppPriority = conn:execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. AppId, live=true}
   return AppPriority
end

function getJsonMappedValue(jsonStr, val1, val2 )
	--NOTE: If you want to do a reverse find then set the val1 to ''
   --      It will then return the k rather than the value
   local res = nil
   local tbl = json.parse{data=jsonStr}
   for k, v in pairs(tbl) do
      trace(val1,val2,k,v)
      if val1 ~= '' then
         if k == val1 then
            return v
         end
      else
         trace(val2,v)
         if v == val2 or v == tostring(val2) then
            trace(k)
            return k
         end
      end
   end
   return ''
end


--------------
return mappings
---------------
