

function main()

   local Message = 
"MSH|^~\&|Swiftqueue|NWA|IIEIPM|NWA|20230927104802||SIU^S13|62081465160302DFFAFA291257C6A296||2.4|||NE|AL| \
SCH|22760125^SWIFTQ|27524305^OAM||40405^REF|AM-SQ-ORTHO||NSP^Follow Up||30|MIN|^^30^20230928120000^20230928123000^1||||||||||||||Rescheduled| \
PID|1||500244726^^^^PAS~11812511^^^^SQID||Anne^Pat15||20000626|F| \
PV1|1|O|SQ-TEST1||40150||21167|||ORTHOP||||||||PUB|27524305|||||||||||||||||||||||||20230928120000| \
PV2||||||||20230928120000|20230928123000|||OTPAT||||||||||||5|1| \
AIL|1|A|SQ-TEST1|CLINIC|ORTHOP|||||||BOOKED| \
AIP|1||21167|ORTHOP|"
   
   local m = os.date('%M')

   if (m % 30 == 0) then
      trace('30 minute script interval reached')
      -- Call your function here that shall run every 30 minutes
      
      
      -- Push each appointment to the queue
      queue.push{data=Message}
      trace("Message pushed to queue")
   end 

end

