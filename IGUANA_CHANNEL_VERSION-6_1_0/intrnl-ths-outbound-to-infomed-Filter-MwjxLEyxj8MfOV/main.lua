--set the database path
--_G.DbFile = '/etc/iNTERFACEWARE/test_au_db_credentials'
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

function main(Data)

   iguana.stopOnError(false)

   local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile")
   local MsgIn, MsgType = hl7.parse{vmd=vmdFile, data=Data} 

   -- Only push MDM^T04 messages to the queue
   
   trace(MsgType)
   
   if MsgType == 'MDMT04' then

      sqUtils.printLog(Data)

      -- Prepare the outgoing message
      local MsgOut = hl7.message{vmd=vmdFile, name=MsgType}

      -- Map the outgoing message
      MsgOut:mapTree(MsgIn)

      queue.push{data=MsgOut:S()}

   end

end