-- Name: patient-utils
-- Author: Stephen Samra
-- Description: A shared module that contains functions performing patient related operations
--
-- Last Modified : 17/08/23
--
-- 17/08/23 - MP - Initial master copy

require('common-utils')

-- Function to detect whether a patient exists. 
-- The function will return 0 if the patient is not found, else it will return the patient ID
function patientExists(patient)
   
   local firstname = patient[5][2]
   local surname = patient[5][1][1]
   local user_type = "N"
   local dob = patient[7][1]:parseDOB()
   local gender = patient[8]:S()
   local address1 = patient[11][1]
   local mrn = patient[3][1]
   
   local patient_record = conn:execute{sql="select count(id), id from users where"..
      " firstname = "..firstname.." and"..
      " surname = "..surname.." and"..
      " Date_of_Birth = '"..dob.."' and"..
      " Gender = '"..gender.."' and"..
      " Address_1 = "..address1.." and"..
      " mrn = '"..mrn.."'"
      , live=true}
   
   if patient_record[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return patient_record[1].id:S()
   end
   
end

-- Function to create a patient if one does not exist
-- If a patient already exists it will update their record with the new data and return their patient ID
-- If a patient does not already exist the function will create them and return their patient ID
function createPatientIfNotExist(patient)
   
   local mobile = extractMobile(patient[13][2])
   local firstname = patient[5][2]
   local surname = patient[5][1][1]
   local email = patient[13][4]
   local user_type = "N"
   local dob = patient[7][1]:parseDOB()
   local gender = patient[8]:S()
   local address1 = patient[11][1]
   local address2 = patient[11][2]
   local town = patient[11][3]
   local county = patient[11][4]
   local postcode = patient[11][5]
   local mrn = patient[3][1]
   
   local insert_id = '0'
   
   local user_id = patientExists(patient)
   
   if user_id == '0'
      then
      -- Create the patient
      conn:execute{sql="insert into users ("..
         "mobile,"..
         "firstname,"..
         "surname,"..
         "user_type,"..
         "email,"..
         "Date_of_Birth,"..
         "Gender,"..
         "Address_1,"..
         "Address_2,"..
         "Address_4,"..
         "Address_5,"..
         "mrn,"..
         "created "..
         ") values ('"
         ..mobile.."', "
         ..firstname..", "
         ..surname..", '"
         ..user_type.."', '"
         ..email.."', '"
         ..dob.."', '"
         ..gender.."', "
         ..address1..", "
         ..address2..", "
         ..town..", "
         ..county..", '"
         --..postcode..", '"
         ..mrn.."', now())"
         , live=live_db}
      
      insert_id = conn:execute{sql="select last_insert_id() as id", live=true}
      return insert_id[1].id:S()
      
      else
         updateOldPatientData(user_id, patient)
         return user_id
   end
   
end

-- Function which will update patient data that is subject to change
function updateOldPatientData(patient_id, patient)

   local mobile = extractMobile(patient[13][2])
   local email = patient[13][4]
   local address1 = patient[11][1]
   local address2 = patient[11][2]
   local town = patient[11][3]
   local county = patient[11][4]
   local postcode = patient[11][5]
   
   conn:execute{sql="update users set "..
      " mobile = '" .. mobile .. "', "..
      " email = '" .. email .. "', "..
      " address_1 = " .. address1 .. ", "..
      " address_2 = " .. address2 .. ", "..
      --" address_3 = " .. address3 .. ", "..
      " address_4 = " .. town .. ", "..
      " address_5 = " .. county ..
      " where id = " .. patient_id
      , live=live_db}
end

function userInFacility(patientId, facilityId)
   
   if facilityId ~= nil and facilityId ~= '' then
   
       local result = conn:execute{sql="select id from facility_user where "..
       " user_id = " .. patientId .. " "..
       " and facility_id = " .. facilityId .. ";"
       , live=true}
   
     return result[1].id:nodeValue()
   else
      return nil
   end
end

function addToFacility(patientId, facilityId)
	if userInFacility(patientId, facilityId) == ''
   then
	   conn:execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end
