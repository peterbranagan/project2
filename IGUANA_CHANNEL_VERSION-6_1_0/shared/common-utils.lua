-- Name: common-utils
-- Author: Stephen Samra
-- Description: A shared module that contains non specific useful functions

-- Last Modified : 17/08/23
--
-- 17/08/23 - MP - Initial master copy

-- Function to extract the date in YYYY-MM-DD format
function node.parseDate(Data)
   local T = dateparse.parse(Data:nodeValue())
   return os.date('%Y-%m-%d', T)
end

-- Function to extract the time in HH:mm format
function node.parseTime(Data)
   local T = dateparse.parse(Data:nodeValue())
   return os.date('%H:%M', T)
end

-- Function to parse a date into DD/MM/YYYY format
function node.parseDOB(Data)
   if Data:nodeValue() == ''
	   then
      return ''
   else
      local T = dateparse.parse(Data:nodeValue())
      return os.date('%d/%m/%Y', T)
   end
end

-- Function that builds a duration variable given a duration in minutes
function calcDuration(duration)

   local hours = duration/60
   
   if hours >= 1 
      then
      local hour = math.floor(hours)
      local minute = duration % 60
      return "0 "..hour..":"..minute..":0"
   end
   
   if hours < 1
      then
      return "0 0:"..duration..":0"
   end
   
end

-- Function to covert a node value to a string
function node:S()
   return tostring(self)
end

-- Function to extract a mobile number from a string
function extractMobile(number)
   
   local mobile = number:S():gsub("%W", "")
     
   if mobile == nil
      then
      return ''
   end
   
   if mobile == '' then
      return ''
   end
   
   mobile = string.match(mobile, "%d+")
   
   if mobile:sub(0, 2) == '08'
      then
	   local no0 = mobile:sub(2, mobile:len())
      mobile = '353' .. no0
   end
   
   if mobile == nil
      then
      return ''
   else
      return mobile
   end
end

-- Function which takes in a message and returns an identical 
-- message but with some values escaped
function escapeValues(Msg)
   
   -- create a direct copy of the message
   local escapedMsg = hl7.message{vmd='HL7_Definitions.vmd', name=Msg.MSH[9][1] .. Msg.MSH[9][2]}
   
   escapedMsg:mapTree(Msg)
   
   -- escape the patient name
   escapedMsg.PID[5][5] = conn:quote(Msg.PID[5][5]:S())
   escapedMsg.PID[5][2] = conn:quote(Msg.PID[5][2]:S())
   escapedMsg.PID[5][1][1] = conn:quote(Msg.PID[5][1][1]:S())
   
   -- escape the patient address
   escapedMsg.PID[11][1] = conn:quote(Msg.PID[11][1]:S())
   escapedMsg.PID[11][2] = conn:quote(Msg.PID[11][2]:S())
   escapedMsg.PID[11][3] = conn:quote(Msg.PID[11][3]:S())
   escapedMsg.PID[11][4] = conn:quote(Msg.PID[11][4]:S())
   escapedMsg.PID[11][5] = conn:quote(Msg.PID[11][5]:S())
   
   -- escape the doctor names
   escapedMsg.PV1[8][5] = conn:quote(Msg.PV1[8][5]:S())
   escapedMsg.PV1[8][3] = conn:quote(Msg.PV1[8][3]:S())
   escapedMsg.PV1[8][2][1] = conn:quote(Msg.PV1[8][2][1]:S())
   
   escapedMsg.PV1[9][5] = conn:quote(Msg.PV1[9][5]:S())
   escapedMsg.PV1[9][3] = conn:quote(Msg.PV1[9][3]:S())
   escapedMsg.PV1[9][2][1] = conn:quote(Msg.PV1[9][2][1]:S())
   
   return escapedMsg
end

function escapeValuesMercy(Msg)
   
   -- create a direct copy of the message
   local escapedMsg = hl7.message{vmd='mercy.vmd', name=Msg.MSH[9][1] .. Msg.MSH[9][2]}
   
   escapedMsg:mapTree(Msg)
   
   -- escape the patient name
   escapedMsg.PID[5][1][5] = conn:quote(Msg.PID[5][1][5]:S())
   escapedMsg.PID[5][1][2] = conn:quote(Msg.PID[5][1][2]:S())
   escapedMsg.PID[5][1][1][1] = conn:quote(Msg.PID[5][1][1]:S())
   
   -- escape the patient address
   escapedMsg.PID[11][1][1][1] = conn:quote(Msg.PID[11][1][1][1]:S())
   escapedMsg.PID[11][1][2] = conn:quote(Msg.PID[11][1][2]:S())
   escapedMsg.PID[11][1][3] = conn:quote(Msg.PID[11][1][3]:S())
   escapedMsg.PID[11][1][4] = conn:quote(Msg.PID[11][1][4]:S())
   escapedMsg.PID[11][1][5] = conn:quote(Msg.PID[11][1][5]:S())
   
   return escapedMsg
end

-- returns a file's contents given a path
function readFile(file)
   
   local File = io.open(file, 'r')
   local FileContents = File:read('*a')
   File:close()
   
   return FileContents
   
end

function roundTimeToNearestMinute(time, nearest)
	
   local hours = time:sub(0, 2)
   local minutes = time:sub(4, 5)
   local nearestMinute
   
   local hours_int = tonumber(hours)
   local minutes_int = tonumber(minutes)
   
   local upVal = minutes_int
   local downVal = minutes_int
   
   local upCount = 0
   local downCount = 0
   
   while upVal % nearest ~= 0
	do
      upVal = upVal + 1
      upCount = upCount + 1
   end
   
   while downVal % nearest ~= 0
	do
      downVal = downVal - 1
      downCount = downCount + 1
   end
   
   if upCount < downCount then
	    if upVal < 10 then
         upVal = 0 .. upVal
         end
	    trace('rounding up to ' .. upVal)
       nearestMinute = upVal
   else
      if downVal < 10 then
         downVal = 0 .. downVal
         end
       trace('rounding down to ' .. downVal)
       nearestMinute = downVal
   end
   
   for i=1,nearestMinute do
      if i % 60 == 0 then
         hours_int = hours_int + 1
      end
   end
   
   if tonumber(nearestMinute) >= 60 then
      nearestMinute = '00'
   end
   
   if hours_int < 10 then
      hours_int = 0 .. hours_int
   end
   
   return hours_int .. ":" .. nearestMinute
end