--[[

Author: Peter Branagan
Date: Aug 2023

All database activity needs be executed from this modules, separated from any other scripts


]]

local sqDB = {

   patientData = {
      mrn = '',
      localmrn = '',
      firstname = '',
      surname = '',
      user_type = '',
      user_status = '',
      dob = '',
      gender = '',   
      home_phone = '',
      mobile = '',
      email = '',
      home_phone = '',
      mobile = '',
      mobileint = '',
      address1 = '',
      address2 = '',
      address3 = '',
      address4 = '',
      address5 = '',
      title = '',
      parent_mobile = '',
      medicareNumber = '',
      imedixNumber = '',
      middleName = '',
      interpreter_required = '',
      spokenLanguage = '',
      alias_firstname = '',
      alias_surname = '',
      alias_middlename = '',
      aboriginality = ''
   },
   
   nokData = {
      
      nk1_userID = '',
      nk1_parent = '',
      nk1_firstname = '',
      nk1_surname = '',
      nk1_rel_code =  '',
      nk1_rel_desc =  '',
      nk1_rel_id = '',        
      nk1_mobile = '',
      nk1_mobile_format = '',
      nk1_phone = '',
      nk1_address_1 = '',
      nk1_address_2 = '',
      nk1_address_4 = '',
      nk1_address_5 = '',
      nk1_eircode = '',
      nk1_email = '',
      nk1_dob = '',
      nk1_title = '',
      nk1_gender = ''
      
   },
   
   gtData = {
      
      gt1_userID = '',
      gt1_parent = '',
      gt1_firstname = '',
      gt1_surname = '',
      gt1_rel_code =  '',
      gt1_rel_desc =  '',
      gt1_rel_id = '',        
      gt1_mobile = '',
      gt1_mobile_format = '',
      gt1_phone = '',
      gt1_address_1 = '',
      gt1_address_2 = '',
      gt1_address_4 = '',
      gt1_address_5 = '',
      gt1_eircode = '',
      gt1_email = '',
      gt1_dob = '',
      gt1_title = '',
      gt1_gender = ''
      
   },
   
   THS = {}
	--------
}

----------------------------------------------------------------------------

function sqDB.execute(val)
   
   --[[
         NOTE: **** The audit table being written to this function 
                     will grow, will need a script to purge monthly ****
   ]]
   
   local channel = iguana.channelName()
   local sql = val.sql:gsub("\\'","'")
   sql = sql:gsub("\n","")
   local msgid = _G.msgCtrlID
   local insert = val.sql:upper():find('INSERT INTO')
   if insert ~= nil then insert = true end
   local update = val.sql:upper():find('UPDATE ')
   if update ~= nil then update = true end
   local delete = val.sql:upper():find('DELETE FROM ')
   if delete ~= nil then delete = true end
   
   if insert or update or delete then
      --check the sql string for HL7 control characters and replace them
      local decodeSpecialCharacters = sqUtils.getMappedValue(sqCfg.extra_params,"decodeSpecialCharacters")
      if decodeSpecialCharacters == '1' then
         val.sql = sqDB.unEncodeString(val.sql)
      end
   end
   
   trace(val.sql)
   
   --execute the SQL caommand
   
   local res = conn:execute(val)
   
   --log the DB activity if set to do so
   
   if _G.dbAudit == '1' then 

      --log all inserts and updates to the iguana db audit table

      if insert or update or delete then
         
         --[[
         conn:execute{sql="insert into iguana_db_audit (channel_name, msgid, sql_str) values ("..    
            conn:quote(channel)..","..
            conn:quote(msgid)..","..
            conn:quote(sql)..")"
            , live=false}
         ]]
         
         local dt = os.date("%Y-%m-%d %H:%M:%S")
         local line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
         
         if not iguana.isTest() then
            --sqUtils.printLog("Writing SQL statement to DB Audit File..")
            --local filePath = "/root/iguana_db_audit.txt"
            local filePath = os.getenv("DB_AUDIT_FILEPATH")
            local file, err = io.open(filePath, "a")
            if not file then
               iguana.logError("Error opening file: " .. err)
            end
           
            file:write(line .. "\n") -- Add a newline character to separate lines
            
            --write the returned ID from the insert
            if insert then
               local result = conn:execute{sql="select last_insert_id() as id", live=true}
               sql = "ID FROM LAST INSERT = "..result[1].id:S()
               line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
               file:write(line .. "\n")
            end
         
            file:close()
         end
      end
   end
     
   return res
   
end

----------------------------------------------------------------------------

function sqDB.executeBlock(func, blockName)
   
   sqUtils.printLog(blockName.." : Begin DB trans...")
   conn:begin{live=true}

   local Success, Error = pcall(func)

   if Success then

      sqUtils.printLog(blockName.." : Committing DB transactions...")
      conn:commit{live=true}

   else

      sqUtils.printLog(blockName.." : DB activity failed, Rolling back transactions...")
      conn:rollback{live=true}

      error(Error)

   end  

end

----------------------------------------------------------------------------

function sqDB.unEncodeString(str)

   str = str:gsub("\\\\F\\\\", "/")
   str = str:gsub("\\\\T\\\\", "&")
   str = str:gsub("\\\\R\\\\", "~")
   str = str:gsub("\\\\S\\\\", "^")
   str = str:gsub("\\\\E\\\\","\\")

   return str

end

----------------------------------------------------------------------------

function sqDB.getFacilitiesByParent(conn, parent)
   sqUtils.printLog("Calling sqDB.getFacilitiesByParent...")
   
   local res = sqDB.execute{sql=
      "select id from facilities \
      where parent_id = "..conn:quote(parent).." or id ="
      ..conn:quote(parent), live=true}

   if #res == 0 then return nil end
   
   return res
end

----------------------------------------------------------------------------

function sqDB.getPatientIdByMrn(mrn)

   local facilityID  = sqCfg.parent_facility
   
   if mrn == nil or mrn == '' then return '0' end
   if facilityID == nil or facilityID == '' then return '0' end
   
   sqUtils.printLog("Calling sqDB.getPatientIdByMrn...")
   
   local PatientId = sqDB.execute{sql=
      "select count(id), id from users where mrn = " .. conn:quote(mrn) .. 
      " and mrn <> '' and id in (select user_id from facility_user where facility_id in ("..facilityID.."))", 
      live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      return '0'
   else
      return PatientId[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.fillPatientData(PID)
   sqUtils.printLog("Calling sqDB.fillPatientData...")
   
   sqDB.patientData.mrn = PID[3][1][1]:S()
   sqDB.patientData.localmrn = PID[3][2][1]:S()
   sqDB.patientData.firstname = PID[5][2]:nodeValue()
   sqDB.patientData.surname = PID[5][1]:nodeValue()
   sqDB.patientData.user_type = "N"
   sqDB.patientData.user_status = 'active'
   sqDB.patientData.dob = sqUtils.parseDOB(PID[7])
   sqDB.patientData.gender = PID[8]:nodeValue()   
   sqDB.patientData.home_phone = ''
   sqDB.patientData.mobile = ''
   -- jlui2 30/08/2023
   -- sqDB.patientData.email = sqCfg.canc_email_add  
   sqDB.patientData.email = sqUtils.getPhoneNumberByType(PID,'L08')
  
   sqDB.patientData.home_phone = sqUtils.getPhoneNumberByType(PID,'L14')
   sqDB.patientData.mobile = sqUtils.getPhoneNumberByType(PID,'SMS')
   if sqDB.patientData.mobile == nil or sqDB.patientData.mobile == '' then
      sqDB.patientData.mobile = sqUtils.getPhoneNumberByType(PID,'L15')
   end
   
   --internationalize the phone numbers
   sqDB.patientData.mobileint = sqDB.patientData.mobile
   if sqDB.patientData.mobile:sub(1,2) == '08' then
      sqDB.patientData.mobileint = '353' .. sqDB.patientData.mobile:sub(2)
   end
   
   --if the international mobile is still blanck then try use the parent mobile
   if sqDB.patientData.mobileint == '' then
      if sqDB.patientData.parent_mobile ~= '' then
         if sqDB.patientData.parent_mobile:sub(1,2) == '08' then
            sqDB.patientData.mobileint = '353' .. sqDB.patientData.parent_mobile:sub(2)
         end
      end
   end
   
   sqDB.patientData.address1 = PID[11][1][1]:nodeValue() 
   sqDB.patientData.address2 = PID[11][1][2]:nodeValue()
   sqDB.patientData.address3 = PID[11][1][3]:nodeValue()
   sqDB.patientData.address4 = PID[11][1][4]:nodeValue()
   sqDB.patientData.address5 = PID[11][1][5]:nodeValue()
  
   local t = PID[5][5]:nodeValue()
   --get the mapped title from config
   sqDB.patientData.title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.patientData.title == '' then
      sqDB.patientData.title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end
end

----------------------------------------------------------------------------

function sqDB.createPatient(PID)
   sqUtils.printLog("Calling sqDB.createPatient...")
   
   --fill the patient demographic variables, used here and update patient
   sqDB.fillPatientData(PID)
   
   sqUtils.printLog("Creating patient mrn = " .. sqDB.patientData.mrn)
    
   sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, \
      firstname, surname, title, user_type, status, Date_of_Birth, Gender, \
      Address_1, Address_2, Address_3, Address_4, Home_Address, created) " ..
      "values (trim(replace("..
      conn:quote(sqDB.patientData.mobileint)..", ' ', '')), "..
      conn:quote(sqDB.patientData.home_phone)..","..
      conn:quote(sqDB.patientData.email)..","..
      conn:quote(sqDB.patientData.mrn)..","..
      conn:quote(sqDB.patientData.firstname)..","..
      conn:quote(sqDB.patientData.surname)..","..
      conn:quote(sqDB.patientData.title)..","..
      conn:quote(sqDB.patientData.user_type)..","..
      conn:quote(sqDB.patientData.user_status)..","..
      conn:quote(sqDB.patientData.dob)..","..
      conn:quote(sqDB.patientData.gender)..","..
      conn:quote(sqDB.patientData.address1)..","..
      conn:quote(sqDB.patientData.address2)..","..
      conn:quote(sqDB.patientData.address3)..","..
      conn:quote(sqDB.patientData.address4)..","..
      conn:quote(sqDB.patientData.address5)..","..
      "now()"..
      ")"
      , live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.updatePatient(id, PID)
   
   if id == nil or id == '' or id == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updatePatient...")
   
   --fill the patient demographic variables, used here and update patient
   sqDB.fillPatientData(PID)
   
   sqUtils.printLog("Updating patient mrn = " .. sqDB.patientData.mrn)

   local sql ="update users " ..
   "set "..
   "mrn = " .. conn:quote(sqDB.patientData.mrn) .. ", "..
   "firstname = " .. conn:quote(sqDB.patientData.firstname) .. ", "..
   "surname = " .. conn:quote(sqDB.patientData.surname) .. ", "..
   "title = " .. conn:quote(sqDB.patientData.title) .. ", "..
   "Date_of_Birth = " .. conn:quote(sqDB.patientData.dob) .. ", "..
   "mobile = trim(replace(" .. conn:quote(sqDB.patientData.mobileint) .. ", ' ', '')), "..
   "home_phone = " .. conn:quote(sqDB.patientData.home_phone) .. ", "..
   "email = " .. conn:quote(sqDB.patientData.email) .. ", "..
   "Gender = " .. conn:quote(sqDB.patientData.gender) .. ", "..
   "Address_1 = " .. conn:quote(sqDB.patientData.address1) .. ", "..
   "Address_2 = " .. conn:quote(sqDB.patientData.address2) .. ", "..
   "Address_3 = " .. conn:quote(sqDB.patientData.address3) .. ", "..
   "Address_4 = " .. conn:quote(sqDB.patientData.address4) .. ", "..
   "Home_Address = " .. conn:quote(sqDB.patientData.address5) .. ", "..
   "modified = now() " ..
   "where id = ".. id

   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.mergePatient(id, minorid, facilityID)
   sqUtils.printLog("Calling sqDB.mergePatient...")
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   sqUtils.printLog("perform merge variable check")
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request")
   end
   local isMinorPatientValid = false
   if (minorid ~= '0') then
      
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request")
   end

   --Referrals First
   local MergeRefs = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
trace(minorid)
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}

      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: " .. minorid .. " Major ID: " .. id ..
         "ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"

         iguana.logDebug("mergePatient().Insert merged user details: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID) ..
      " and user_id = " .. minorid

      iguana.logDebug(" mergePatient(). Update ewaiting_referrals: " .. sql)

      sqDB.execute{sql=sql, live=false} 
   end

   --Reservations Second
   local MergeRes = sqDB.execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}

   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}

      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         
         iguana.logDebug("sql merged_users: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) ..
      " or parent_id = " .. conn:quote(facilityID) .. ")" ..
      " and user_id = " .. minorid
      
      iguana.logDebug("sql update reservation: " .. sql)

      sqDB.execute{sql=sql, live=false}
   end

   --Patient Flow Third
   local MergePatFlow = sqDB.execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   
   --iguana.logDebug("MergePatFlow: " .. MergePatFlow)

   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}     

      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         
         iguana.logDebug("sql insert merged_users: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) ..
      " or parent_id = " .. conn:quote(facilityID) .. ")"..
      " and user_id = " .. minorid
      
      iguana.logDebug("sql update: " .. sql)

      sqDB.execute{sql=sql, live=false}
   end

   -- Remove the minor one from thw Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) ..
   " and user_id = " .. minorid
   
   iguana.logDebug("sql: " .. sql)

   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.userInFacility(patientId, facilityId)
   local result = sqDB.execute{sql="select id from facility_user where "..
   " user_id = " .. patientId .. " "..
   " and facility_id = " .. facilityId .. ";"
   , live=true}
   if #result == 0 then return '' end
   return result[1].id:nodeValue()
end

----------------------------------------------------------------------------

function sqDB.addToFacility(patientId, facilityId)
   sqUtils.printLog("Calling sqDB.addToFacility...")
   
	if sqDB.userInFacility(patientId, facilityId) == ''
   then
	   sqDB.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------

function sqDB.getPatientReferral(id,facilityID)
   sqUtils.printLog("Calling sqDB.getPatientReferral...")

   local referralCount = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where facility_id = " .. facilityID .. 
      " and status in ('active', 'pre_triage', 'triage') and user_id = " .. id, live=true}

   if referralCount[1]['count(id)']:S() == '0' then
      return nil
   else
      return referralCount[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.updateReferralStatus(referralID, opdAppointment, status)
	
   if referralID == nil or referralID == '' or referralID == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updateReferralStatus...")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "status = '"..status.."' " ..
   "where id = ".. referralID

   sqDB.execute{sql=sql, live=false}
end

----------------------------------------------------------------------------
function sqDB.createPatientReferral(patientId, facilityID, orderId, examCode, examDesc, episodeNumber, 
               accessionNumber, refSourceName, PatientType, status, conditionType, specialty, refSourceId, requestedDate, 
               locationType, requestDetails, contactNum, notes, notesHTML, categoryID)
   
   sqUtils.printLog("Calling sqDB.createPatientReferral...")

   -- jlui2 20/11/2023.  Use categoryId parameter instead of facilityCategory
   --local ref_params = sqCfg.referral_params  
   --local facilityCategory = sqUtils.getMappedValue(ref_params, "facilityCategory")
   
   
   local referralCount = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and user_id = " .. patientId .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. ")",
      live=true}

   if referralCount[1]['count(id)']:S() == '0' then

      --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
      
      sqDB.execute{sql=
         "insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, " ..
         "referral_source_id, referral_source_name, status, `condition`, comments, created, created_by, " ..
         "pas_verified, patient_type) " ..
         "values ("..
         facilityID..","..
         patientId..","..  
         requestedDate..","..  
         categoryID..","..
         specialty..","..
         refSourceId..","..
         conn:quote(refSourceName)..","..
         conn:quote(status)..","..
         conn:quote(conditionType)..","..
         --"'HL7 Referral'"..","..
         conn:quote(notes)..","..
         "now()"..","..
         conn:quote(sqUser).. "," ..
         "0" .."," ..
         conn:quote(PatientType) ..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

      sqDB.execute{sql=
         "insert into ewaiting_referral_condition_tags " ..
         "values (" .. result[1].id .."," .. conditionType .. ")", 
         live=false}
      
      --Insert referral condition notes
      if notesHTML ~= '' then
         
       
         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. result[1].id ..", "..conn:quote(sqUser)..","..conn:quote(notesHTML)..")", 
            live=false}
      end

      sqDB.execute{sql=
         "insert into ewaiting_referral_orders " ..
         "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
         "values (null," .. 
         result[1].id .."," .. 
         conn:quote(orderId) .. "," .. 
         conn:quote(episodeNumber) .. "," .. 
         conn:quote(examCode) .. "," .. 
         conn:quote(examDesc) .. "," ..
         conn:quote(accessionNumber) .. "," .. 
         "now(), '')", 
         live=false}

   else
      
      --check if the order already exists
      local orderCount = sqDB.execute{
         sql="select count(id), order_id from ewaiting_referral_orders where order_id = " .. conn:quote(orderId), live=true}

      if orderCount[1]['count(id)']:S() == '0' then
         sqDB.execute{sql=
            "insert into ewaiting_referral_orders " ..
            "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
            "values (null," .. 
            referralCount[1].id:S() .."," .. 
            conn:quote(orderId) .. "," .. 
            conn:quote(episodeNumber) .. "," .. 
            conn:quote(examCode) .. "," .. 
            conn:quote(examDesc) .. "," ..
            conn:quote(accessionNumber) .. "," .. 
            "now(),'')", live=false}      
      end
      
   end
   
end

----------------------------------------------------------------------------

function sqDB.createAppointment(id, facilityID, SCH, clinCode1, reasonID, PV2, PV1)
   sqUtils.printLog("Calling sqDB.createAppointment...")

   local retResId = '0'
   
   local appointmentDuration = SCH[9]:S()
   if appointmentDuration == '' then
      appointmentDuration = sqUtils.getMappedValue(sqCfg.extra_params,"defaultAppDuration")
   end
   
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
   local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   
   --PB 18.10.23: fix for the replacement of nulls with 0000-00-00
   if opdAppointmentString == nil or opdAppointmentString == '' then 
      iguana.logWarning("Invalid request date in SCH[11][4]")
      opdAppointmentString = '0000-00-00' 
   end
   --
 
   -- 
   --Check if an appointment already exists for this patient
   --PB 03.10.23: Modified quesry to include the facility ID when checking if the
   --             appointment exists. Sometimes a user can have an appointment for the
   --             same date and time but under a different facility
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and reservation_date = " .. conn:quote(opdAppointmentString) .. 
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString) ..
      " and facility_id = "..facilityID, live=true}

   if ResId[1]['count(id)']:S() == '0' then

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, " ..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
         "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appointmentDuration.." minute, 1, 5)"..","..
         conn:quote(clinCode1)..","..
         reasonID..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
      retResId = result[1].id:S() -- return the res id later

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}

   else
      sqDB.execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..
         ", google_cal_id = " ..conn:quote(clinCode1).. 
         ", source = 'h' where id = "..ResId[1].id, live=false}   

      -- check if the reservation_Ipm records exist - if not create them
      local IPMResId = sqDB.execute{sql=
         "select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
      --
      if IPMResId[1]['count(res_id)']:S() == '0' then

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
            ")"
            , live=false}
         --
      end
      --
   end
   
   return retResId
   
end

----------------------------------------------------------------------------

function sqDB.updatePatientFlow(id, facilityID, SCH, clinCode, reasonID, PV1)
   sqUtils.printLog("Calling sqDB.updatePatientFlow...")

   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   
   local DXCId = SCH[1][1]:S()
   local appReason = 1

   local patient_flow_stage = ''

   local timestamp = os.date( "%Y-%m-%d %X" )

   if not sqUtils.isEmpty(SCH[11][4]:S()) then
      timestamp = tostring(dateparse.parse(SCH[11][4]:S()))
   end

   if SCH[25][1]:S() == 'Attended' then
      patient_flow_stage =  'CHECK-IN'  
   elseif SCH[25][1]:S() == 'Did Not Attend' then
      patient_flow_stage = 'DNA'
   elseif SCH[25][1]:S() == 'CheckedOut' then
      patient_flow_stage = 'COMPLETE'

      -- Checkout time should come from the PV1-45.1.1 Discharge Date/Time field
      if not sqUtils.isEmpty(PV1[45]:S()) then
         timestamp = tostring(dateparse.parse(PV1[45]:S()))
      end      
   end

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then
      --
      local ResId2 = sqDB.execute{sql=
         "select count(id), id from patient_flow where res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}

      if ResId2[1]['count(id)']:S() ~= '0' then

         -- Set current_stage to 'N' for all stages, before adding new stage
         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'N' " ..
            "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. 
            " and user_id = " ..id ..
            " and facility_id = " .. facilityID,
            live = false}

         -- It shouldn't matter if previously, the current stage was 'DNA'.  
         -- Above code forces the current stage for that 'DNA' stage to value 'N' 

      end

      --insert the new patient flow
      
      --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      sqDB.execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, \
         res_id, sq_user_id,          stage, nurse, current_stage, bay, source) " .. 
         "values (" ..
         "null, " ..
         conn:quote(timestamp) .. ", " ..
         facilityID ..", " ..
         id..", " ..
         conn:quote(ResId[1]['id']:S()) ..", " ..
         conn:quote(sqUser)..", " ..
         conn:quote(patient_flow_stage) .."," ..
         "NULL, 'Y', NULL, 'hl7'" .. 
         ")"
         , live=false}         

   end
   --
end

----------------------------------------------------------------------------

function sqDB.deleteUpdatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   sqUtils.printLog("Calling sqDB.deleteUpdatePatientFlow...")

   local DXCId = SCH[1][1]:S()

   local patient_flow_stage = ''

   if SCH[25][1]:S() == 'CancelAttended' then
      patient_flow_stage =  'CHECK-IN' -- get the previous stage to operate on
   elseif SCH[25][1]:S() == 'CancelCheckedOut' then
      patient_flow_stage = 'COMPLETE'  -- get the previous stage to operate on
   elseif SCH[25][1]:S() == 'Cancel Did Not Attend' then
      patient_flow_stage = 'DNA'  -- get the previous stage to operate on
   end

   local timestamp = os.date( "%Y-%m-%d %X" )

   if not sqUtils.isEmpty(SCH[11][4]:S()) then
      timestamp = tostring(dateparse.parse(SCH[11][4]:S()))
   end

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then

      local ResId2 = sqDB.execute{sql="select count(id), id, res_id, current_stage from patient_flow where stage = " 
         .. conn:quote(patient_flow_stage) .. " and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}

      if ResId2[1]['count(id)']:S() == '0' and patient_flow_stage == 'COMPLETE' then

         -- Could be a DNA         
         patient_flow_stage = 'DNA'

         ResId2 = sqDB.execute{sql="select count(id), id, res_id, current_stage from patient_flow where stage = " 
            .. conn:quote(patient_flow_stage) .. " and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}                
      end

      if ResId2[1]['current_stage']:S() == 'Y' then            
         local sql = "delete from patient_flow " ..
         "where "..
         "id = " .. ResId2[1]['id']:S()

         sqDB.execute{sql=sql, live=false}          
      else
         local errorTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for res_id " 
         .. ResId2[1]['id']:S() .. " and facilityID " .. facilityID
         error(errorTxt)
      end
   else
      local errorTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for user_id " .. id .. 
      " and facilityID " .. facilityID
      error(errorTxt)
   end

   if SCH[25][1]:S() == 'CancelCheckedOut' then
      -- Set the current_stage of the last/latest entry for that reservation (res_id) to 'Y'
      local ResId3 = sqDB.execute{sql="select count(id), id from patient_flow where res_id = " 
         .. conn:quote(ResId[1]['id']:S() .. " order by timestamp desc"), live=true}

      if ResId3[1]['count(id)']:S() ~= '0' then
         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'Y' " ..
            "where id = " .. conn:quote(ResId3[1]['id']:S()),
            live = false}
      end      
   end
   --
end

----------------------------------------------------------------------------
function sqDB.updateAppointment(id, facilityID, SCH, clinCode1, reasonID, PV2, PV1)
   sqUtils.printLog("Calling sqDB.updateAppointment...")

   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
   local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   
   local reservation_status = ''
   
   -- patient_flow_stage not used ...
   if SCH[25][1]:S() == 'Attended' then
      patient_flow_stage =  'CHECK-IN'
   elseif SCH[25][1]:S() == 'Did Not Attend' then
      patient_flow_stage = 'DNA'
   elseif SCH[25][1]:S() == 'CheckedOut' then
      patient_flow_stage = 'COMPLETE'
   end
   
   --PB 18.10.23: fix for the replacement of nulls with 0000-00-00
   if opdAppointmentString == nil or opdAppointmentString == '' then 
      iguana.logWarning("Invalid request date in SCH[11][4]")
      opdAppointmentString = '0000-00-00' 
   end
   --

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and reservation_date = " .. conn:quote(opdAppointmentString) .. 
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}
   
   if ResId[1]['count(id)']:S() == '0'
      then

      --check if an existing appointment exists
      local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
         " and location_pref = " .. conn:quote(DXCId) .. " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
         then
         sqDB.execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
            , live=false}
      end

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, " ..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
         "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval 30 minute, 1, 5)"..","..
         conn:quote(clinCode1)..","..
         reasonID..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"
         , live=false}  

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}
      --
   else 
      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_end_time = substring(convert("..conn:quote(opdAppointmentTimeString)..", time) + interval 30 minute, 1, 5)"..","..
         "google_cal_id = " .. conn:quote(clinCode1)..","..
         "modified = now(), modified_source = 'h'"..","..
         "reservation_time = " .. conn:quote(opdAppointmentEndTimeString)..
         " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
         "user_id = " .. id
         , live=false}
   end
   --
end

----------------------------------------------------------------------------
function sqDB.cancelAppointment(id, facilityID, SCH, reasonID)
   
   if id == nil or id == '' or id == '0' then return end
   
   sqUtils.printLog("Calling sqDB.cancelAppointment...")
   
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   
   local DXCId = SCH[1][1]:S()

   sqDB.execute{sql="update reservation set " .. 
      "reservation_date = " .. conn:quote(opdAppointmentString)..","..
      "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
      "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
      "status = 'cancelled'" ..","..
      "modified = now(), modified_source = 'h'"..
      " where location_pref = " .. conn:quote(DXCId).." and " ..
      "user_id = " .. id
      , live=false}
end

----------------------------------------------------------------------------
function sqDB.getFacilityIDGivenPatientID_CHO8(patientId)
  local result = sqDB.execute{sql="select facility_id from facility_user where "..
  " user_id = " .. patientId .. ";"
  , live=true}
   
   return result[1].facility_id:nodeValue()
end

function sqDB.getFacilityIDGivenPatientID(patientId)
  local result = sqDB.execute{sql="select facility_id from facility_user where "..
  " user_id = " .. patientId .. ";"
  , live=true}
   
   local str = ''
   for i=1, #result do
      str = str..result[i].facility_id
      if i+1 <= #result then
         str = str..","
      end
   end
   
   return str
end

----------------------------------------------------------------------------
function sqDB.getReservationParentID(AppId)
   local ResId = sqDB.execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}
   return ResId
end

----------------------------------------------------------------------------
function sqDB.getAppCategory(resId)
   local AppCategory = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. resId, live=true}
   return AppCategory
end


----------------------------------------------------------------------------
function sqDB.getAppCategoryNoParentId(AppId)
   local AppCategory = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. AppId, live=true}
   return AppCategory
end

----------------------------------------------------------------------------
function sqDB.getAttendId(fID)
   local AttendId = sqDB.execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' " ..
         "and field_name = 'ATTND (Attended Status)'", live=true}
   return AttendId
end

----------------------------------------------------------------------------
function sqDB.getOutcomeId(fID)
   local OutcomeId = sqDB.execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' " ..
         "and field_name = 'SCOCM (Outcome)'", live=true} 
   return OutcomeId
end

----------------------------------------------------------------------------
function sqDB.getAttendCategory(AttendID, AppId)
   local AttendCategory = sqDB.execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. AttendID ..
            " and reservation_id = " .. AppId, live=true}
   return AttendCategory
end

----------------------------------------------------------------------------
function sqDB.getOutcomeCategory(OutcomeId, AppId)
   local OutcomeCategory = sqDB.execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. OutcomeId ..
            " and reservation_id = " .. AppId, live=true}
   return OutcomeCategory
end

----------------------------------------------------------------------------
function sqDB.getAppType(parentId)
   local AppType = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'TYPE' and res_id = " .. parentId, live=true}
   return AppType
end

----------------------------------------------------------------------------
function sqDB.getAppTypeByAppId(AppId)
   local AppType = sqDB.execute{sql=
         "select IFNULL(ipm_value,'') as ipm_value from reservation_ipm " ..
         "where ipm_name = 'TYPE' and res_id = " .. AppId, live=true}
   return AppType
end

----------------------------------------------------------------------------
function sqDB.getAppPriority(parentId)
   local AppPriority = sqDB.execute{sql=
         "select IFNULL(ipm_value,'') as ipm_value from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. parentId, live=true}  
   return AppPriority
end

----------------------------------------------------------------------------
function sqDB.getAppPriorityByAppId(AppId)
   local AppPriority = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. AppId, live=true}
   return AppPriority
end




----------------------------------------------------------------------------
function sqDB.getReferralIdByAppId(appID)
   local findReferral = sqDB.execute{sql=
            "select count(id), ewaiting_referral_id from ewaiting_referral_orders where res_id = " .. appID, live=true}
   return findReferral
end

----------------------------------------------------------------------------
function sqDB.getLocationPref(appId)
   local lpref = sqDB.execute{sql="select location_pref from reservation where id = " .. conn:quote(appId), live=true}

   return lpref[1]['location_pref']:S()
end

----------------------------------------------------------------------------
function sqDB.getActiveAppts(eRefId)
   local activeAppts = sqDB.execute{sql= 
            "select count(id) " ..
            "from reservation " .. 
            "where id in (select res_id from ewaiting_referral_orders where ewaiting_referral_id = " ..eRefId .. ") " ..
            "and status = 'active'"
            , live=true}
   return activeAppts
end

----------------------------------------------------------------------------
function sqDB.setReferralToDischarged(eRefId)
   
   if eRefId == nil or eRefId == '' or eRefId == '0' then return end
   
   sqUtils.printLog("Calling sqDB.setReferralToDischarged...")
   
   sqDB.execute{sql=
               "update ewaiting_referrals " ..
               "set status = 'discharged' " ..
               "where id = " .. eRefId, live=false} 
end

----------------------------------------------------------------------------
function sqDB.getReferralByOrderId(orderId)
   local findReferral2 = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id, IFNULL(cancellation_date,0) as cancellation_date " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderId), live=true}
end

----------------------------------------------------------------------------
function sqDB.getEwaitingDischargeTimestamp(eRefId)
   local findReferral2a = sqDB.execute{sql=
         "select count(id), timestamp from ewaiting_discharges " ..
         "where referral_id = " .. eRefId, live=true} 
   return findReferral2a
end

----------------------------------------------------------------------------
function sqDB.getEwaitingReservationsCount(eRefId)
   local findReferral2b = sqDB.execute{sql=
            "select count(res_id) from ewaiting_reservations " ..
            "where ewaiting_referral_id = " .. eRefId, live=true}
   return findReferral2b
end

----------------------------------------------------------------------------
function sqDB.updateGoogleCalId(appId, googleCalId)
   
   if appId == nil or appId == '' or appId == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updateGoogleCalId...")
   
   local ResId = sqDB.execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. appId, live=true}

   if ResId[1]['parent_id']:S() ~= '0' then
      sqDB.execute{sql="update reservation set " .. 
         "google_cal_id = " .. conn:quote(googleCalId)..
         " where id = " .. appId
         , live=false}
   end
end

----------------------------------------------------------------------------
function sqDB.getReferralComments(refId)
   local findReferral = sqDB.execute{sql=
      "select count(id), comments from ewaiting_referrals where id = " .. conn:quote(refId), live=true}
   return findReferral
end

----------------------------------------------------------------------------
function sqDB.getReferralProvider(id)
   local findReferralProv = sqDB.execute{sql=
      "select count(id), name from ewaiting_facility_providers " ..
      "where meditech_id = " .. conn:quote(id), live=true}
   return findReferralProv
end

----------------------------------------------------------------------------
function sqDB.getCancelledOrdersNoAppts(orderId)
   local findReferral3 = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id, IFNULL(cancellation_date,0) as cancellation_date " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderId), live=true}
   return findReferral3
end

----------------------------------------------------------------------------
function sqDB.getSQIdsByIPMAppId(appId)
   
   --pulls the Ids for reservation, ewaiting_referrals and ewaiting_referral_orders
   --from SQ based on the IPM app Id stored in location_pref
   --return reservation ID, Order ID, Referral ID
   --If order and referral IDs are null then they dont exist for the appointment
   
   if appId == nil or Id == '' then
      return nil,nil,nil
   end
   
   local res = sqDB.execute{sql=
      "select IFNULL(res.id,'') as 'ResID', IFNULL(ero.id,'') as 'OrderID', IFNULL(ero.ewaiting_referral_id,'') as 'RefID' \
      from reservation res left outer join ewaiting_referral_orders ero on ero.res_id = res.id \
      where location_pref = ".. conn:quote(appId)
      , live=true}

   return res[1].ResID:S(), res[1].OrderID:S(), res[1].RefID:S()
end

----------------------------------------------------------------------------
function sqDB.getReferringDoctor(appId)
   
   local refDoc = nil
   
   if appId == nil or Id == '' then
      return '','',''
   end
   
   local resId, orderId, refId = sqDB.getSQIdsByIPMAppId(appId)
   if refId ~= '' then
      local res = sqDB.execute{sql=
         "select referral_source_name from ewaiting_referrals \
         where id = ".. conn:quote(refId)
         , live=true}
      refDoc = res[1].referral_source_name:S()
   end
   
   return refDoc
end

----------------------------------------------------------------------------
function sqDB.updateAccessionNumber(appId)
   
   if appId == nil or appId == '' or appId == '0' then return end
   
   local resId, orderId, refId = sqDB.getSQIdsByIPMAppId(appId)
   
   if orderId ~= '' then
      local res = sqDB.execute{sql=
         "update ewaiting_referral_orders set accession_number = 'C"..appId.."'"..
         " where id = ".. orderId , live=false}
   end
   
   return
end

----------------------------------------------------------------------------
function sqDB.updateReservationModifiedSource(id, SCH)
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
  
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then
      
      local sql ="update reservation set modified_source = " .. conn:quote("h") .. " where id = " ..
      conn:quote(ResId[1]['id']:S())

      sqDB.execute{sql=sql, live=false}

   else
      local errorTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for user_id " .. id .. 
      " and facilityID " .. facilityID
      error(errorTxt)
   end   
end

----------------------------------------------------------------------------

function sqDB.getExamDetailsOnIPMID(AppId)
   
   -- Specific to Galway where there will only ever be one order per referral
   local examCode = ''
   local examDesc = ''
   
   -- SELECT id from demo.reservation where location_pref = <<AppId>>
   -- SELECT ewaiting_referral_id from demo.ewaiting_reservations where res_id = <<id>>  ( "Reservation to Referral" lookup table)
   -- SELECT exam_code, exam_name from demo.ewaiting_referral_orders where ewaiting_referral_id = <<ewaiting_referral_id
   
   local res = sqDB.execute{sql=
      "select exam_code, exam_name from reservation res left outer join ewaiting_reservations ewr on (res.id = ewr.res_id ) "..
      "left outer join ewaiting_referral_orders ero on (ero.ewaiting_referral_id = ewr.ewaiting_referral_id) "..
      "where location_pref = "..conn:quote(AppId)
      , live=true}
   
   if #res > 0 then
      examCode = res[1].exam_code:S()
      examDesc = res[1].exam_name:S()
   end
   
   return examCode, examDesc

end

----------------------------------------------------------------------------
function sqDB.getAppointmentReason(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. appId
         , live=true}
   
   return res
end

----------------------------------------------------------------------------
function sqDB.getClinicConfigMapping(key, ...)
      
   --needs 2 parameters, facility id and reason id
   if select('#', ...) <= 2 then
      local facilityId, reasonId = select(1,...)
      
      local res = sqDB.execute{sql=
            "select "..key.." from facility_clinic_codes where facility_id = "..facilityId.." and reason_id = "..reasonId,
            live=true}
      return res[1][key]:S()
    
   else
      return ''
   end  
   
end
----------------------------------------------------------------------------
function sqDB.getSpecialtyCode(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. appId
         , live=true}
   
   return res
end

----------------------------------------------------------------------------
function sqDB.getAttendingDoctor(appId, facilityId)
   
   local docCode = ''
   local docName = ''
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         docCode = res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      local parentFacilityId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'parentFacilityId')
      
      res = sqDB.execute{sql=
         "select firstname from facilities where parent_id = "..
         parentFacilityId.." and facility_type = 'sp'"
         , live=true}
      
      docName = res[1].firstname:S()
   
   end
   
   return docCode,docName
end

----------------------------------------------------------------------------
function sqDB.getParentFacilityId(facilityID)

   local parentFacilityID = ''
   local parentFID = sqDB.execute{sql="select parent_id from facilities where id = "..facilityID, live=true}

   if parentFID[1]['parent_id']:S() ~= '0' then
      parentFacilityID = parentFID[1]['parent_id']:S()
   else
      parentFacilityID = facilityID
   end

   return parentFacilityID

end


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

--     THS IMPLEMENTATION  -----


-----------------------------------------------------------------
----------------------------------------------------------------------------

function sqDB.THS.fillPatientData(PID, mrn)
   
   sqUtils.printLog("Loading patient data...")
   
   local patFirstname = ''
   local patSurname = ''
   local patMiddlename = ''
   local patTitle = ''
   
   --if we are using an identifier for patient names then pull the name based on that
   --example identifer L = Legal Name, if no identifer then pull first name
   local nameIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'patientNameIdentifier')   
   patFirstname, patSurname, patMiddlename, patTitle = sqDB.THS.getPatientName(PID,nameIdentifier)
   
   local alias_patFirstname = ''
   local alias_patSurname = ''
   local alias_patMiddlename = ''
   local slias_patTitle = ''
   
   --pull oput the alias name
   local aliasNameIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'aliasNameIdentifier')   
   alias_patFirstname, alias_patSurname, alias_patMiddlename, alias_patTitle = sqDB.THS.getPatientName(PID,aliasNameIdentifier)
      
   sqDB.patientData.mrn = mrn
   sqDB.patientData.firstname = patFirstname
   sqDB.patientData.surname = patSurname
   sqDB.patientData.middleName = patMiddlename
   sqDB.patientData.user_type = "N"
   sqDB.patientData.dob = sqUtils.parseDOB(PID[7])
   sqDB.patientData.gender = PID[8]:nodeValue()   
   sqDB.patientData.home_phone = ''
   sqDB.patientData.mobile = ''
   sqDB.patientData.email = ''
   sqDB.patientData.title = patTitle
   sqDB.patientData.spokenLanguage = PID[15][2]:S()
   sqDB.patientData.aboriginality = PID[10][1]:S()
   
   sqDB.patientData.alias_firstname = alias_patFirstname
   sqDB.patientData.alias_middlename = alias_patMiddlename
   sqDB.patientData.alias_surname = alias_patSurname
   
   --get the mapped title from config
   sqDB.patientData.title = sqUtils.getMappedValue(sqCfg.title_map, patTitle)
   if sqDB.patientData.title == '' then
      sqDB.patientData.title = patTitle:sub(1,1):upper()..patTitle:sub(2, 2+patTitle:len()-2):lower()
   end
   
   sqDB.patientData.Home_Address = PID[11][1][5]:S()  
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')
   
   sqDB.patientData.home_phone = sqDB.THS.getPhoneNumberByType(PID,homePhoneIdentifier)
   sqDB.patientData.mobile = sqDB.THS.getPhoneNumberByType(PID,mobileIdentifier)
   sqDB.patientData.email = sqDB.THS.getPhoneNumberByType(PID,emailIdentifier)
   
   sqDB.patientData.mobileint = ''  
   if sqDB.patientData.mobile:sub(1,2) == mobileCheckDigits then
      sqDB.patientData.mobileint = mobileCheckDigits .. sqDB.patientData.mobile:sub(2)
   else
      sqDB.patientData.mobileint = sqDB.patientData.mobile
   end
   
   sqDB.patientData.address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   sqDB.patientData.address2 = PID[11][1][2]:nodeValue()
   sqDB.patientData.address3 = '' --PID[11][1][3]:nodeValue()
   sqDB.patientData.address4 = PID[11][1][3]:nodeValue()
   sqDB.patientData.address5 = PID[11][1][4]:nodeValue() 
   
   local mcCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode')
   sqDB.patientData.medicareNumber = sqDB.THS.getMedicareNumbers(PID,mcCode)
   
   local imCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'iMedixCode')
   sqDB.patientData.imedixNumber = getMRNbyAssigningFacility(PID,imCode) 

end

-----------------------------------------------------------------
function sqDB.THS.fillNOKData(patientID, NK1)

   sqUtils.printLog("Loading NOK data...")
   
   sqDB.nokData.nk1_userID = ''
   sqDB.nokData.nk1_parent = patientID
   sqDB.nokData.nk1_firstname = NK1[1][2][2]:S()
   sqDB.nokData.nk1_surname = NK1[1][2][1]:S()
   
   sqDB.nokData.nk1_rel_code =  NK1[1][3][1]:S()
   sqDB.nokData.nk1_rel_desc =  NK1[1][3][2]:S()
   if sqDB.nokData.nk1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.nokData.nk1_rel_code).." or description = "..conn:quote(sqDB.nokData.nk1_rel_desc), live=true} 
   sqDB.nokData.nk1_rel_id = res[1].id:S()
   
   sqDB.nokData.nk1_phone = sqDB.THS.getNKPhoneNumberByType(NK1,'PH')
   sqDB.nokData.nk1_mobile = sqDB.THS.getNKPhoneNumberByType(NK1,'CP')   
   sqDB.nokData.nk1_email = sqDB.THS.getNKPhoneNumberByType(NK1,'Internet')
  
   if sqDB.nokData.nk1_mobile:sub(1,2) == '04' then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.nokData.nk1_mobile_format = mobileCountryCode .. sqDB.nokData.nk1_mobile:sub(2)
   else
      sqDB.nokData.nk1_mobile_format = sqDB.nokData.nk1_mobile
   end 
   
   sqDB.nokData.nk1_address_1 = NK1[1][4][1]:S()
   sqDB.nokData.nk1_address_2 = NK1[1][4][2]:S()
   sqDB.nokData.nk1_address_3 = NK1[1][4][3]:S()
   sqDB.nokData.nk1_address_4 = NK1[1][4][4]:S()
   sqDB.nokData.nk1_eircode = NK1[1][4][5]:S()
   
   sqDB.nokData.nk1_dob = sqUtils.parseDOB(NK1[1][16])
   local t = NK1[1][2][5]:nodeValue()
   --get the mapped title from config
   sqDB.nokData.nk1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.nokData.nk1_title == '' then
      sqDB.nokData.nk1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.nokData.nk1_gender = NK1[1][15]:S()

end

----------------------------------------------------------------------------
function sqDB.THS.fillNKContactData(patientID, nokContacts)
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')
   
   function getPhoneNumberByType(NK1, pType)
      if NK1:childCount() >=1 then
         for i=1, NK1:childCount() do
            if NK1[i][3]:S() == pType then
               return NK1[i][1]:S()
            end
         end
      end  
      return ''   
   end
   
   sqDB.nokData.nk1_userID = ''
   sqDB.nokData.nk1_parent = patientID
   
   sqDB.nokData.nk1_firstname = nokContacts[2][2]:S()
   sqDB.nokData.nk1_surname = nokContacts[2][1]:S()
   
   sqDB.nokData.nk1_rel_code = nokContacts[3][1]:S()
   sqDB.nokData.nk1_rel_desc = nokContacts[3][2]:S()
   
   if sqDB.nokData.nk1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.nokData.nk1_rel_code).." or description = "..conn:quote(sqDB.nokData.nk1_rel_desc), live=true} 
   sqDB.nokData.nk1_rel_id = res[1].id:S()
      
   sqDB.nokData.nk1_phone = getPhoneNumberByType(nokContacts[5],homePhoneIdentifier)   
   sqDB.nokData.nk1_mobile = getPhoneNumberByType(nokContacts[5],mobileIdentifier)
   sqDB.nokData.nk1_email = getPhoneNumberByType(nokContacts[5],emailIdentifier)
  
   if sqDB.nokData.nk1_mobile:sub(1,2) == mobileCheckDigits then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.nokData.nk1_mobile_format = mobileCountryCode .. sqDB.nokData.nk1_mobile:sub(2)
   else
      sqDB.nokData.nk1_mobile_format = sqDB.nokData.nk1_mobile
   end 
   
   sqDB.nokData.nk1_address_1 = nokContacts[4][1]:S()
   sqDB.nokData.nk1_address_2 = nokContacts[4][2]:S()
   sqDB.nokData.nk1_address_3 = nokContacts[4][3]:S()
   sqDB.nokData.nk1_address_4 = nokContacts[4][4]:S()
   sqDB.nokData.nk1_eircode = nokContacts[4][5]:S()
   
   sqDB.nokData.nk1_dob = sqUtils.parseDOB(nokContacts[16])
   local t = nokContacts[2][5]:nodeValue()
   --get the mapped title from config
   sqDB.nokData.nk1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.nokData.nk1_title == '' then
      sqDB.nokData.nk1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.nokData.nk1_gender = nokContacts[15]:S()
   
end

----------------------------------------------------------------------------
function sqDB.THS.fillGTContactData(patientID, gtContacts)
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')
   
   function getPhoneNumberByType(GT1, pType)
      if GT1:childCount() >=1 then
         for i=1, GT1:childCount() do
            if GT1[i][3]:S() == pType then
               return GT1[i][1]:S()
            end
         end
      end  
      return ''   
   end
   
   sqDB.gtData.gt1_userID = ''
   sqDB.gtData.gt1_parent = patientID
   
   sqDB.gtData.gt1_firstname = gtContacts[3][1][2]:S()
   sqDB.gtData.gt1_surname = gtContacts[3][1][1]:S()
   
   sqDB.gtData.gt1_rel_code = gtContacts[11][1]:S()
   sqDB.gtData.gt1_rel_desc = gtContacts[11][2]:S()
   
   if sqDB.gtData.gt1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process GT1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.gtData.gt1_rel_code).." or description = "..conn:quote(sqDB.gtData.gt1_rel_desc), live=true} 
   sqDB.gtData.gt1_rel_id = res[1].id:S()
     
   sqDB.gtData.gt1_phone = getPhoneNumberByType(gtContacts[6],homePhoneIdentifier)   
   sqDB.gtData.gt1_mobile = getPhoneNumberByType(gtContacts[6],mobileIdentifier)
   sqDB.gtData.gt1_email = getPhoneNumberByType(gtContacts[6],emailIdentifier)
  
   if sqDB.gtData.gt1_mobile:sub(1,2) == mobileCheckDigits then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.gtData.gt1_mobile_format = mobileCountryCode .. sqDB.gtData.gt1_mobile:sub(2)
   else
      sqDB.gtData.gt1_mobile_format = sqDB.gtData.gt1_mobile
   end 
   
   sqDB.gtData.gt1_address_1 = gtContacts[5][1][1]:S()
   sqDB.gtData.gt1_address_2 = gtContacts[5][1][2]:S()
   sqDB.gtData.gt1_address_3 = gtContacts[5][1][3]:S()
   sqDB.gtData.gt1_address_4 = gtContacts[5][1][4]:S()
   sqDB.gtData.gt1_eircode = gtContacts[5][1][5]:S()
   
   sqDB.gtData.gt1_dob = sqUtils.parseDOB(gtContacts[8])
   
   local t = gtContacts[3][1][5]:nodeValue()
   --get the mapped title from config
   sqDB.gtData.gt1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.gtData.gt1_title == '' then
      sqDB.gtData.gt1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.gtData.gt1_gender = gtContacts[9]:S()
   
end

----------------------------------------------------------------------------
function sqDB.THS.getReferralPriorityCode(facilityID, categoryID)
   local refPriority = sqDB.execute{sql=
      "select priority from ewaiting_categories where facility_id = " ..
      facilityID .. " and id = ".. categoryID, live=true}
   return refPriority[1].priority
end

----------------------------------------------------------------------------
function sqDB.THS.getReferralByControlID(controlID , userID, facilityID)
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select id, facility_id from ewaiting_referrals where control_id = " ..
      conn:quote(controlID) .. " and user_id = "..userID.." and facility_id = "..facilityID

   if suppressFacilityCheck == '1' then
      sqlSTR = "select id, facility_id from ewaiting_referrals where control_id = " ..
      conn:quote(controlID) .. " and user_id = "..userID
   end
   
   local res = sqDB.execute{sql=sqlSTR, live=true}
   
   return res[1].id:S(), res[1].facility_id:S()
end

-----------------------------------------------------------------
function sqDB.THS.createPatient(PID, mrn, NK1, IN1, GT1)
   
   sqUtils.printLog("Creating patient "..mrn.." ...")
   
   sqDB.THS.fillPatientData(PID,mrn)
   
   --[[ temp remove translator_required
   
      sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created, requires_translator) " ..
         "values (trim(replace("..
   ]]
   sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created) " ..
         "values (trim(replace("..
         conn:quote(sqDB.patientData.mobileint)..", ' ', '')), "..
         conn:quote(sqDB.patientData.home_phone)..","..
         conn:quote(sqDB.patientData.email)..","..
         conn:quote(sqDB.patientData.mrn)..","..
         conn:quote(sqDB.patientData.firstname)..","..
         conn:quote(sqDB.patientData.surname)..","..
         conn:quote(sqDB.patientData.user_type)..","..
         conn:quote(sqDB.patientData.dob)..","..
         conn:quote(sqDB.patientData.gender)..","..
         conn:quote(sqDB.patientData.address1)..","..
         conn:quote(sqDB.patientData.address2)..","..
         conn:quote(sqDB.patientData.address3)..","..
         conn:quote(sqDB.patientData.address4)..","..
         conn:quote(sqDB.patientData.address5)..","..
         conn:quote(sqDB.patientData.Home_Address)..","..
         conn:quote(sqDB.patientData.title)..","..
         --temp remove translator
         --conn:quote(sqDB.patientData.interpreter_required)..","..
         "'active', now()"..
         ")"
         , live=false}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   local patientID = result[1].id:S()

   --Insert medicare number
   sqDB.THS.insertMedicareNumber(patientID)
   
   --Insert infomedix number
   sqDB.THS.insertIMedixNumber(patientID)
   
   --add update insurance details
   --PB 14.05.24 : moved to general processing fro all messages
   --sqDB.THS.addUpdateInsurance(IN1,patientID)
   
   --add additional patient info
   sqDB.THS.addUpdateUserAdditionalInfo(patientID, sqDB.patientData.middleName, sqDB.patientData.spokenLanguage, 
      sqDB.patientData.alias_firstname, sqDB.patientData.alias_surname, sqDB.patientData.alias_middlename,
      sqDB.patientData.aboriginality
   )
   
   sqUtils.printLog("Created patient with id: [" ..patientID.."]") 

   if NK1 ~= nil then  
      if not (NK1:isNull()) then
         sqDB.THS.addUpdateNK(patientID, NK1)
      end  
   end
   
   if GT1 ~= nil then  
      if not (GT1:isNull()) then
        sqDB.THS.addUpdateGT(patientID, GT1)
      end  
   end
   
   return result[1].id:S()
   
end

-----------------------------------------------------------------
function sqDB.THS.updatePatient(id, PID, mrn, NK1, IN1, GT1, msgType)
   
   sqUtils.printLog("Updating patient "..mrn.." ...")
   trace(id)
   sqDB.THS.fillPatientData(PID, mrn)
     
      local sql ="update users " ..
      "set "..
      "mrn = " .. conn:quote(sqDB.patientData.mrn) .. ", "..
      "firstname = " .. conn:quote(sqDB.patientData.firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.patientData.surname) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.patientData.dob) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.patientData.mobileint) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.patientData.home_phone) .. ", "..
      "email = " .. conn:quote(sqDB.patientData.email) .. ", "..
      "Gender = " .. conn:quote(sqDB.patientData.gender) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.patientData.address1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.patientData.address2) .. ", "..
      "Address_3 = " .. conn:quote(sqDB.patientData.address3) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.patientData.address4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.patientData.address5) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.patientData.Home_Address) .. ", "..
      --temp remove translator
      --"requires_translator = " .. conn:quote(sqDB.patientData.interpreter_required) .. ", "..
      "title = " .. conn:quote(sqDB.patientData.title) .. ", "..
      "modified = now() " ..
      "where id = ".. id

      sqDB.execute{sql=sql, live=false}

   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertMedicareNumber(id)
   
   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertIMedixNumber(id)
   
   --add update insurance details
   --sqUtils.printLog("Updating insurance details for patient: "..id)
   --PB 14.05.24 - moved to general processing for all messages
   --sqDB.THS.addUpdateInsurance(IN1,id)
   
   --add additional patient info
   sqDB.THS.addUpdateUserAdditionalInfo(id, sqDB.patientData.middleName, sqDB.patientData.spokenLanguage,
      sqDB.patientData.alias_firstname, sqDB.patientData.alias_surname, sqDB.patientData.alias_middlename,
      sqDB.patientData.aboriginality
   )
   
   if msgType == 'A31' then   

      --clear all NOKs for this poatient
      --if NK1 missing then this is a removal of NOK
      --if NK1 exists then the logic to come will add/update as required
      sqDB.THS.removePatientNOKflags(id)
      
      --clear the act as emergency flags
      sqDB.THS.removePatientEmergflags(id)

      --update NOK details
      if NK1 ~= nil then
         if not (NK1:isNull()) then
            sqDB.THS.addUpdateNK(id, NK1)
         end
      end
      
      --update guadians GT1 details
      if GT1 ~= nil then  
         if not (GT1:isNull()) then
            sqDB.THS.addUpdateGT(id, GT1)
         end  
      end
      
      

   end -- end if A31
   
   --check if we need to decease / un-decease the patient
   trace(PID[29]:S())
   if PID[29]:S() ~= '' and PID[29]:S() ~= '""' then
      
      sqDB.THS.processDeceasedPatient(id, PID)
      
   end
   
   
end

-----------------------------------------------------------------

function sqDB.THS.addUpdateNK(id, NK1)
   
   --get NOK contacts
   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')
   local nokContacts = sqDB.THS.getNOKContacts(NK1,nokIdentifier)
   trace(nokContacts)

   --get emergency contacts
   local emergencyContactIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emergencyContactIdentifier')
   local emergencyContacts = sqDB.THS.getNOKContacts(NK1,emergencyContactIdentifier)
   trace(emergencyContacts)

   --get other contacts
   local otherCarerIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'otherCarerIdentifier')
   local otherContacts = sqDB.THS.getNOKContacts(NK1,otherCarerIdentifier)
   trace(otherContacts)
   
   --get guardian contacts
   --get other contacts
   local guardianContactIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'guardianContactIdentifier')
   local guardianContacts = sqDB.THS.getNOKContacts(NK1,guardianContactIdentifier)
   trace(guardianContacts)
   
   --get carers
   local carerIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'carerIdentifier')
   local carerContacts = sqDB.THS.getNOKContacts(NK1,carerIdentifier, 1)
   trace(carerContacts)

   --create nok contact(s)
   if #nokContacts > 0 then
      
      trace(#nokContacts)
      for i=1, #nokContacts do
         if nokContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, nokContacts[i])
         end
      end               
   end

   --create emergency contact(s)
   if #emergencyContacts > 0 then
      --for now add all emergency contacts based on no end date
      trace(#emergencyContacts)
      for i=1, #emergencyContacts do
         if emergencyContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, emergencyContacts[i])
         end
      end  
   end

   --create other contact(s)
   if #otherContacts > 0 then
      --for now add all other contact based on no end date
      trace(#otherContacts)
      for i=1, #otherContacts do
         if otherContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, otherContacts[i])
         end
      end  
   end
   
   --create guardian contact(s)
   if #guardianContacts > 0 then
      --for now add all other contact based on no end date
      trace(#guardianContacts)
      for i=1, #guardianContacts do
         if guardianContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, guardianContacts[i])
         end
      end  
   end
   
   --create carer contact(s)
   if #carerContacts > 0 then
      --for now add all other contact based on no end date
      trace(#carerContacts)
      for i=1, #carerContacts do
         if carerContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, carerContacts[i])
         end
      end  
   end
   

end

-----------------------------------------------------------------

function sqDB.THS.addUpdateGT(id, GT1)
 
   --get GT contacts
   local gtContacts = {}
   
   for i=1, #GT1 do
      --only process entries with no end date
      if GT1[i][14]:S() == '' then
         sqDB.THS.updateGTContact(id, GT1[i])
      end
   end


end

-----------------------------------------------------------------

function sqDB.THS.removePatientNOKflags(id)

   local sqlStr = "select uai.id as id from users_additional_info uai "..
                  "join users u on u.id = uai.user_id where u.parent = "..conn:quote(id)..
                  " and uai.act_as_nok = 1 "
   local res = sqDB.execute{sql=sqlStr, live=true}

   if #res > 0 then
      
      local inStr = '('
      for i=1, #res do
         inStr = inStr..res[i].id:S()
         if i+1 <= #res then
            inStr = inStr..','
         end
      end
      inStr = inStr..')'

      sqlStr = "update users_additional_info set act_as_nok = 0 "..
      "where id in "..inStr

      local res = sqDB.execute{sql=sqlStr, live=false}

   end

end

-----------------------------------------------------------------

function sqDB.THS.removePatientEmergflags(id)

   local sqlStr = "select uai.id as id from users_additional_info uai "..
                  "join users u on u.id = uai.user_id where u.parent = "..conn:quote(id)..
                  " and uai.emergency_contact = 1 "
   local res = sqDB.execute{sql=sqlStr, live=true}

   if #res > 0 then
      
      local inStr = '('
      for i=1, #res do
         inStr = inStr..res[i].id:S()
         if i+1 <= #res then
            inStr = inStr..','
         end
      end
      inStr = inStr..')'

      sqlStr = "update users_additional_info set emergency_contact = 0 "..
      "where id in "..inStr

      local res = sqDB.execute{sql=sqlStr, live=false}

   end

end

-----------------------------------------------------------------
function sqDB.THS.processDeceasedPatient_OLD(id, PID)
   
   --update the date of death in users_additional_info
   
   if PID == nil or id == '0' then
      return
   end

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      id, live=true}
   
   trace(PID[29]:S())
   local dod = dateparse.parse(PID[29]:D())
   local dodString = string.sub(tostring(dod),1,10)
   
   if res[1].cnt:S() == '0' then
      
      sqDB.execute{sql="insert into users_additional_info (user_id, deceased_date) " ..
         "values ("..
         id .. "," ..
         conn:quote(dodString)..
         ")"
         , live=false}
   else
      sqDB.execute{sql="update users_additional_info set " ..
         "deceased_date = "..conn:quote(dodString)..
         " where user_id = "..id
         , live=false}
   end
   
   sqDB.execute{sql="update users set " ..
         "status = 'deceased'"..
         " where id = "..id
         , live=false}
   
   sqUtils.printLog("DOD details updated!")
   
   --ensure communications are switched off
   
   --switch off emails
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,2,0)"
      , live=false}
   end
   
   -- switch off SMS
   
   local res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,1,0)"
      , live=false}
   end
  
end


-----------------------------------------------------------------
function sqDB.THS.processDeceasedPatient(userID, PID)
   
   --update the date of death in users_additional_info
   
   if PID == nil or userID == '0' then
      return
   end

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      userID, live=true}
   
   trace(PID[29]:S())
   local dod = dateparse.parse(PID[29]:D())
   local dodString = string.sub(tostring(dod),1,10)
   
   if res[1].cnt:S() == '0' then
      
      sqDB.execute{sql="insert into users_additional_info (user_id, deceased_date) " ..
         "values ("..
         userID .. "," ..
         conn:quote(dodString)..
         ")"
         , live=false}
   else
      
      sqDB.execute{sql="update users_additional_info set " ..
         "deceased_date = "..conn:quote(dodString)..
         " where user_id = "..userID
         , live=false}
   
   end
   
   sqDB.execute{sql="update users set " ..
         "status = 'deceased'"..
         " where id = "..userID
         , live=false}
   
   sqUtils.printLog("DOD details updated!")
   
   --ensure communications are switched off
   
   sqUtils.printLog("DOD: Updating communication preferences...")
   
   local res = 0
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_channels where `key` = 'email'", live=true}
   local channelID_emails = res[1].id:S()
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_channels where `key` = 'sms'", live=true}
   local channelID_sms = res[1].id:S()
   
   res = sqDB.execute{sql="select * from user_notification_events", live=true}
   local eventCount = #res
   
   trace(eventCount)
   
   --loop through all event typoes but only disable emails & sms - FOR NOW
   
   for i=1, eventCount do
      
      -- this is open to additional keys
      if res[i].key:S() == 'appointment.reminders' or res[i].key:S() == 'post_appointment.communication' then

         local eventID = res[i].id:S()
         
         --disable the emails
         
         local res2 = 0

         res2 = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
            userID.." and event_id = "..eventID.." and channel_id = "..channelID_emails, live=true}

         if res2[1].cnt:S() ~= '0' then

            sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
               res2[1].id:S(), live=false}

         else

            sqDB.execute{sql=
               "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
               "values (null,"..
               userID..","..
               eventID..","..
               channelID_emails..",0)"
               , live=false}

         end

         -- disable the SMS
         
         res2 = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
            userID.." and event_id = "..eventID.." and channel_id = "..channelID_sms, live=true}

         if res2[1].cnt:S() ~= '0' then

            sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
               res2[1].id:S(), live=false}

         else

            sqDB.execute{sql=
               "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
               "values (null,"..
               userID..","..
               eventID..","..
               channelID_sms..",0)"
               , live=false}

         end
         
      end

   end
   
   sqUtils.printLog("DOD: Communication preferences updated!")
  
end


-----------------------------------------------------------------
function sqDB.THS.getPatientIdByMrn(mrn, facilityID)
   
   local PatientId = sqDB.execute{sql="select count(id), id from users where mrn = " .. conn:quote(mrn) ..
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = "..facilityID..")", live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      return '0'
   else
      return PatientId[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.mergePatient(id, minorid, facilityID)
   
   sqUtils.printLog("Merging patient...")
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request")
   end

   local isMinorPatientValid = false
   if (minorid ~= '0') then
      trace(minorid)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request")
   end
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   --Referrals First
   local MergeRefs = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}
      --
      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{HMC HL7 Merge - Minor ID: " .. minorid .. " Major ID: " .. id ..
         "ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"
         sqUtils.printLog("mergePatient().Insert merged user details: " .. sql)
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID:S()) ..
      " and user_id = " .. minorid
      sqUtils.printLog(" mergePatient(). Update ewaiting_referrals: " .. sql)
      sqDB.execute{sql=sql, live=false}
      --
   end
   --
   --Reservations Second
   local MergeRes = sqDB.execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}
   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}
      --
      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID:S()) .. 
      " or parent_id = " .. conn:quote(facilityID:S()) .. ")" .. 
      " and user_id = " .. minorid
      sqDB.execute{sql=sql, live=false}
      --
   end
   --
   --Patient Flow Third
   local MergePatFlow = sqDB.execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}
      --
      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID:S()) .. 
      " or parent_id = " .. conn:quote(facilityID:S()) .. ")"..
      " and user_id = " .. minorid
      sqDB.execute{sql=sql, live=false}
      --
   end
   --
   -- Remove the minor one from the Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID:S()) .. 
   " and user_id = " .. minorid
   sqDB.execute{sql=sql, live=false}
   --
end   

-----------------------------------------------------------------
function sqDB.THS.createAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)

   sqUtils.printLog("Creating appointment...")
   
   if sqUtils.isEmpty(reasonID) then
      reasonID = 0
   end

   --extract the res date and time
   --local mrn = SCH[1][1]:S()
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   local DXCId = SCH[2][1]:S()
   --local apptType = PV2[12]:S()
   local apptType = SCH[8][1]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local refID = SCH[4][1]:S()
   local DXCSession = SCH[5][1]:S()

   local appDuration = SCH[9]:S()
   if appDuration  == '' then
      appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   end

   -- 
   --Check if an appointment already exists for this patient
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " 
      .. conn:quote(opdAppointmentString) .. " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, "..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by,".. 
         "is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
         --    conn:quote(opdAppointmentEndTimeString)..","..
         conn:quote(clinCode)..","..
         reasonID..","..
         conn:quote(refID)..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),"..sqUser..", 'appointment.created',"..
         result[1].id:S()..","..id..",null)"
         , live=false}      

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'REASON'"..","..
         conn:quote(DXCReason)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'AIPID'"..","..
         conn:quote(DXCAIPID)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'DXCSPEC'"..","..
         conn:quote(DXCSpeciality)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'SESSION'"..","..
         conn:quote(DXCSession)..
         ")"
         , live=false}

   else
      sqDB.execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..", google_cal_id = " ..conn:quote(clinCode).. 
         ", hl7_id = "..conn:quote(refID)..", source = 'h' where id = "..ResId[1].id, live=false}   

      -- check if the reservation_Ipm records exist - if not create them
      local IPMResId = sqDB.execute{sql="select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
      --
      if IPMResId[1]['count(res_id)']:S() == '0' then

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'REASON'"..","..
            conn:quote(DXCReason)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'AIPID'"..","..
            conn:quote(DXCAIPID)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'DXCSPEC'"..","..
            conn:quote(DXCSpeciality)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'SESSION'"..","..
            conn:quote(DXCSession)..
            ")"
            , live=false}
         --
      end
      --
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)
   
   sqUtils.printLog("Updating appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   --local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local refID = SCH[4][1]:S()
   local DXCId = SCH[2][1]:S()
   local apptType = SCH[8][1]:S()
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local DXCSession = SCH[5][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   
   --local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " .. conn:quote(opdAppointmentString) ..
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then
      
      --check if an existing appointment exists
      local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
         sqDB.execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
           , live=false}
      end
      
      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, reservation_time,"..
           "reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by, is_attended,booking_user_id, source, print,stats, "..
           "quality,capacity_sync, is_urgent) " .. 
            "values ("..
            facilityID..","..
            id..","..
            "'f',0"..","..
            conn:quote(DXCId).."," ..
            conn:quote(opdAppointmentString)..","..
            conn:quote(opdAppointmentTimeString)..","..
            "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
            --conn:quote(opdAppointmentEndTimeString)..","..
            conn:quote(clinCode)..","..
            reasonID..","..
            conn:quote(refID)..","..
            "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
            ")"
            , live=false}  
      
            local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
            --Add A user audit record for the app creation
            sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.created',"..
                 result[1].id:S()..","..id..",null)"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'REASON'"..","..
               conn:quote(DXCReason)..
               ")"
               , live=false}

               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'AIPID'"..","..
               conn:quote(DXCAIPID)..
               ")"
               , live=false}

               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'DXCSPEC'"..","..
               conn:quote(DXCSpeciality)..
               ")"
               , live=false}
         
               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'SESSION'"..","..
               conn:quote(DXCSession)..
               ")"
               , live=false}
      --
   else 

      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_end_time = substring(convert("..conn:quote(opdAppointmentTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
         --"reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "google_cal_id = " .. conn:quote(clinCode)..","..
         "hl7_id = " .. conn:quote(refID)..","..
         "modified = now(), modified_source = 'h'"..","..
         "reservation_time = " .. conn:quote(opdAppointmentEndTimeString)..
         " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
         "user_id = " .. id
         , live=false}


      --update the ewaiting_reservations table
      
      local resID = ResId[1].id:S()
      local res = sqDB.execute{sql="select count(id), id from ewaiting_referrals where control_id = "..conn:quote(refID), live=true}
      
      if sqUtils.isEmpty(refID) then
         sqUtils.printLogW("updateAppointment: Referral ID not found for control id: "..refID)
      end
      
      refID = res[1].id:S()
      
      res = sqDB.execute{sql="select count(ewaiting_referral_id), ewaiting_referral_id from ewaiting_reservations where res_id = "..conn:quote(resID), live=true}
      local origRefID = res[1].ewaiting_referral_id:S()
      
      --update the referral id for the appointment  
      
      if not sqUtils.isEmpty(refID) then

         sqDB.execute{sql="update ewaiting_reservations set " .. 
            "ewaiting_referral_id = " .. conn:quote(refID)..
            " where res_id = " .. conn:quote(resID)
            , live=false}


         --PB 22.05.24 - IR-303 : if we are change referral id and referral status originally in follow_up then move to assigned
         if origRefID ~= refID then

            sqDB.execute{sql="update ewaiting_referrals set " .. 
               "status = 'assigned' "..
               " where status = 'follow_up' and id = " .. conn:quote(refID)
               , live=false}

         end 
         
      end

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),"..sqUser..", 'appointment.rescheduled',"..
         conn:quote(ResId[1]['id']:S())..","..id..",null)"
         , live=false}       

   end
    --
end

-----------------------------------------------------------------
function sqDB.THS.updatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   
   sqUtils.printLog("Updating patient flow...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   local appReason = 1
   
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0'
      then
      --
      local ResId2 = sqDB.execute{sql="select count(id), id from patient_flow where stage = 'CHECK-IN' and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}
      
   
      if ResId2[1]['count(id)']:S() == '0' then
         
         --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
         
         --insert the new patient flow
         sqDB.execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..
            conn:quote(sqUser)..",'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}
       end
    end
    --
end

-----------------------------------------------------------------
function sqDB.THS.cancelAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("Cancelling appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where location_pref = " .. conn:quote(DXCId).." and " ..
         "user_id = " .. id
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.cancelled',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDB.THS.dnaAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("DNA appointment...")
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDB.THS.getPatientReferral(id,facilityID,controlID)
   
   sqUtils.printLog("getting patient referral...")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. id.." and control_id = "..conn:quote(controlID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return referralCount[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.createPatientReferral(id,facilityID, clinCode, RF1, PRD, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   --PB 06.12.23 : account class
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --local accClass = RF1[5][2]:S() 
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDB.execute
   {sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}

   --if the refarral already exists then skip
   if referralCount[1]['count(id)']:S() ~= '0' then
      sqUtils.printLog("Referral already exists, Skipping creation...")
      return
   end
   
   --referral does not exist so continue
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   local refExpiry = RF1[8]:S()
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   local refDateReferred = ''
 
   if not isEmpty(RF1[9]:S()) then
      refDateReferred = sqUtils.parseDate(RF1[9])
      trace(refDateReferred)
   end
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   
   --PB 07.05.2024 : IR-285
   local refPriorityName = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'refPriorityMap_inbound',RF1[2][2]:S())  
   if sqUtils.isEmpty(refPriorityName) then
      sqUtils.printLogW("Priority mapping not found for: "..RF1[2][2]:S().." in mapping: refPriorityMap_inbound")
      refPriorityName = RF1[2][2]:S()
   end
   
   local refSourceName = ''
   if PRD ~= '' then

      if PRD[1][1]:S() == 'PIMS' then
         refSourceName = PRD[4][9]:S()
      else
         refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
      end
   
   end
   
   local referralPriority = sqDB.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   refSpeciality = sqUtils.checkControlCharacters(refSpeciality,'1')
   
   local referralSpeciality = sqDB.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   
   --replace control characters
   refCondition = sqUtils.checkControlCharacters(refCondition,'2')
   trace(refCondition)
   
   local refConditionRes = sqDB.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refConditionRes[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refConditionRes[1]['id']:S()
   end
  
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
   
   --if secondary referral then place in followup
   local secondReferralStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralStatus")
   if RF1[1][1]:S() == secondReferralStatus then
      waitingStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralTab")
   end
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
      
   --speciality_id, referral_source_id,"..   "referral_source_name, status, `condition`, control_id, \
   sql="insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, \
   speciality_id, referral_source_id,"..   "referral_source_name, status, control_id, \
   comments, created, created_by) " ..
   "values ("..
   facilityID..","..
   id..","..
   conn:quote(refDateReferred)..","..
   facilityCategory..","..
   facilitySpeciality..","..
   FacilitySourceId..","..
   conn:quote(refSourceName)..","..
   conn:quote(waitingStatus)..","..
   --conn:quote(facilityConditionTag)..","..
   conn:quote(refID)..","..
   "'HL7 Referral'"..","..
   "now()"..","..
   sqUser..
   ")"

   sqDB.execute{sql=sql, live=false}
   sqUtils.printLog("createPatientReferral: " .. sql)

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   if facilityConditionTag ~= 0 then
      sql = "insert into ewaiting_referral_condition_tags values (" .. result[1].id .."," .. facilityConditionTag .. ")"
      sqUtils.printLog("createPatientReferral:" ..sql)
      sqDB.execute{sql=sql, live=false}
   else
      trace(refCondition)
      sqUtils.printLog("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition.." not found!")
      iguana.logWarning("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition.." not found!")
   end
   
   --PB 02.01.23 : SQAU-154 - Adding to ewaiting_referrals_additional_info
   --Need to create an entry in ewaiting_referrals_additional_info and set the consultant_id = the parent facility id
   
   
   -- pass consultant id here
   
   local refId = result[1].id:S()
   local res = sqDB.execute{sql=
      "select count(id) as cnt from ewaiting_referrals_additional_info where consultant_id = " .. conn:quote(consID) ..
      " and referral_id = " .. conn:quote(refId) , live=true}
	--if the consultant entry for this referral does not already exist then add it
   if res[1].cnt:S() == '0' then   
      sqDB.execute{sql=
         "insert into ewaiting_referrals_additional_info (id, referral_id, created_at, updated_at, consultant_id) " ..
         "values (null," .. refId ..",now(), now(), " .. conn:quote(consID) ..")", live=false}      
   end
   
   -- 

   sqDB.THS.addUpdateReferralExpiryDate(facilityID, result[1].id:S(), refExpiry)
   
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --sqDB.THS.addUpdateReferralAccountClass(facilityID, result[1].id:S(), accClass)
   
   
   --if secondary referral then set a follow up reason
   local secondReferralStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralStatus")
   if RF1[1][1]:S() == secondReferralStatus then

      local followupReasonID = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralReason")

      local res = sqDB.execute{sql=
         "select count(id) as cnt from ewaiting_referral_follow_up_reasons where ewaiting_referral_id = " ..
         conn:quote(refId) , live=true}

      --if the consultant entry for this referral does not already exist then add it
      if res[1].cnt:S() == '0' then   
         sqDB.execute{sql=
            "insert into ewaiting_referral_follow_up_reasons (id, ewaiting_referral_id, follow_up_reason_id, created_at) " ..
            "values (null," .. refId .."," .. conn:quote(followupReasonID) .. ", now() )", live=false}  
      else
         sqDB.execute{sql=
            "update ewaiting_referral_follow_up_reasons set follow_up_reason_id = "..conn:quote(followupReasonID) ..
            " where ewaiting_referral_id = "..conn:quote(refId), live=false}
      end
   
   end
   
   sqUtils.printLog("Creation of referral completed!") 
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
   return result[1].id
   
end
  
-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralExpiryDate(facId, refId, expDate)

   local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Expiry Date';", live=true}

   if #res == 0 then
      error("Expiry Date not mapped on ewaiting_custom_fields for facility "..facId)
   end
   
   local customId = res[1]:S()

   res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
      conn:quote(refId).." and custom_field_id = "..customId, live=true}
   
   if #res == 0 then
      
      --insert new expiry date
      sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
         ' values ('..refId..', """'..expDate..
         '""", "'..expDate..'",'..customId..') ', live=false}
   else
      
      --update the expiry date
      local id = res[1]:S()
      res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
         ' SET value = """'..expDate..
         '""" , value_str = "'..expDate..'" where id = '..id, live=false}
   end   
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralAccountClass(facId, refId, accClass)

   local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Account Class';", live=true}

   if #res == 0 then
      error("Account Class not mapped on ewaiting_custom_fields for facility "..facId)
   end
   
   local customId = res[1]:S()
 
   res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
      conn:quote(refId).." and custom_field_id = "..customId, live=true}
   
   if #res == 0 then
      
      --insert new account class
      sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
         ' values ('..refId..', """'..accClass..
         '""", '..conn:quote(accClass)..','..customId..') ', live=false}
   else
      
      --update the account class
      local id = res[1]:S()
      res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
         ' SET value = """'..accClass..
         '""" , value_str = '..conn:quote(accClass)..' where id = '..id, live=false}
   end   
   --
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralConditionType(facId, refId, cpcDesc)

   local conditionType = ''
   
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_condition_tags where name = " 
      .. conn:quote(cpcDesc), live=true}
   
   if res[1]['cnt']:S() ~= '0' then
      conditionType = res[1]['id']:S()
   end

   if sqUtils.isEmpty(conditionType) then
       sqUtils.printLogW("Condition type not mapped on ewaiting_condition_tags table")
      return
   end
   
   sql = "select referral_id from ewaiting_referral_condition_tags where referral_id = " .. conn:quote(refId)
   res = sqDB.execute{sql=sql, live=true}
   
   if #res == 0 then
      
      --insert condition type
      sql = "insert into ewaiting_referral_condition_tags (referral_id, condition_tag_id) values (" .. 
              res[1].referral_id .."," .. conditionType .. ")"
      sqDB.execute{sql=sql, live=false}
   
   else
      
      --update the condition type
      sql = "update ewaiting_referral_condition_tags set condition_tag_id = "..conditionType..
            " where referral_id = "..conn:quote(refId)
      sqDB.execute{sql=sql, live=false}
     
   end   
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateWLComments(patientID, facilityID, controlID, comments)

   sqUtils.printLog("Updating waitng list comments...")
   
   local wlComment = "Waiting List Comment:\n\n"..comments
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")

   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
 
   if referralCount[1]['count(id)']:S() ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      local res = sqDB.execute{sql="select id from ewaiting_referrals_condition_notes where ewaiting_referral_id = "..
         referralCount[1].id:S().." and condition_note like 'Waiting List Comment:%'" , live=true}

      if #res == 0 then

         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. referralCount[1].id ..", "..conn:quote(sqUser)..","..conn:quote(wlComment)..")", 
            live=false}

      else

         sqDB.execute{sql=
            "update ewaiting_referrals_condition_notes set "..
            " condition_note = "..conn:quote(wlComment)..","..
            " updated_at = now() "..
            " where id = "..res[1].id:S(), 
            live=false}

      end

   end

end
   
-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralComments(patientID, facilityID, controlID, comments)

   sqUtils.printLog("Updating referral comments...")
   
   local refComment = "Referral Comment:\n\n"..comments
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")

   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}

   if referralCount[1]['count(id)']:S() ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      local res = sqDB.execute{sql="select id from ewaiting_referrals_condition_notes where ewaiting_referral_id = "..
         referralCount[1].id:S().." and condition_note like 'Referral Comment:%'", live=true}

      if #res == 0 then
         
         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. referralCount[1].id ..", "..conn:quote(sqUser)..","..conn:quote(refComment)..")", 
            live=false}

      else

         sqDB.execute{sql=
            "update ewaiting_referrals_condition_notes set "..
            " condition_note = "..conn:quote(refComment)..","..
            " updated_at = now() "..
            " where id = "..res[1].id:S(), 
            live=false}

      end

   end

end

-----------------------------------------------------------------
function sqDB.THS.updatePatientReferral(id,facilityID, clinCode, RF1, PRD, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so update
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   
   --PB 06.12.23 : account class / appCategory
   local accClass = RF1[5][2]:S()
   
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
     
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   
   --PB 16.05.2024 : IR-293
   local refPriorityName = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'refPriorityMap_inbound',RF1[2][2]:S())  
   if sqUtils.isEmpty(refPriorityName) then
      sqUtils.printLogW("Priority mapping not found for: "..RF1[2][2]:S().." in mapping: refPriorityMap_inbound")
      refPriorityName = RF1[2][2]:S()
   end 
   
   --
   local referralPriority = sqDB.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --date referred
   local refDateReferred = sqUtils.parseDate(RF1[9])
   
   --referral source name
   local refSourceName = ''
   if PRD ~= '' then
      refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
   end
   
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   refSpeciality = sqUtils.checkControlCharacters(refSpeciality,'1')
 
   --
   local referralSpeciality = sqDB.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   --
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   refCondition = sqUtils.checkControlCharacters(refCondition,'2')
   
   --
   local refCondition = sqDB.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end
   
   --PB 12.12.23: Simon suggested this is not required
   --facilityConditionTag = ''
   --
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

   sqDB.execute{sql="update ewaiting_referrals set "..
      "facility_id = "..facilityID..","..
      "user_Id = "..id..","..
      "category_id = "..facilityCategory..","..
      "speciality_id = "..facilitySpeciality..","..
      "referral_source_id = "..FacilitySourceId..","..
      "date_referred = "..conn:quote(refDateReferred)..","..
      "referral_source_name = "..conn:quote(refSourceName)..","..
      "modified = now(), modified_by = "..sqUser..
      " where id = ".. referralCount[1]['id']:S(), live=false}
   
   --update the consultant id
   local res = sqDB.execute{sql=
      "update ewaiting_referrals_additional_info set consultant_id = " .. conn:quote(consID) ..
      " where referral_id = " .. referralCount[1]['id']:S() , live=false}
	
   
   sqDB.THS.addUpdateReferralExpiryDate(facilityID, referralCount[1]['id']:S(), refExpiry)
   
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --sqDB.THS.addUpdateReferralAccountClass(facilityID, referralCount[1]['id']:S(), accClass)
   
   sqUtils.printLog("Updating of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDB.THS.dischargePatientReferral(id, facilityID, refID, disOutcome)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..   " where id = ".. referralID
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   --get the discharge reason
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_discharge_reasons where "..
      " code = " .. conn:quote(disOutcome).." and facility_id = "..facilityID, live=true}

   local disReasonID = 0
   if res[1].cnt:S() ~= '0' then
      disReasonID = res[1].id:S()
   end
   
   local sqlStr = "insert into ewaiting_discharges (timestamp, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values (".. "now()"..",".. referralID..",".. disReasonID..",".. "''"..",".. sqUser.. ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
   
   -- ***** MAY NEED TO CANCEL ANY OPEN APPOINTMENTS HERE *******

   sqUtils.printLog("Discharging of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDB.THS.createGPDetails(patientID, PD1, faciliityID)
   
   sqUtils.printLog("Creating GP details...")
   
   --extract the relevant GP details
   local gpFirstname = ''
   local gpOrgID = ''
   local gpID = ''
   local gpPractice = ''
   local gpFullName = ''
   
   gpFirstname = PD1[4][3]:S()
   gpSurname = PD1[4][2]:S()
   gpPractice = PD1[3][1]:S()
   gpID = PD1[4][1]:S()
   gpFullName = gpFirstname.." "..gpSurname
  
   local gpCount = sqDB.execute{sql="select count(id), id from gp_details"..
      " where medical_council_number = " .. conn:quote(gpID) .. 
      " and firstname = " .. conn:quote(gpFirstname) ..
      " and surname = " .. conn:quote(gpSurname), live=true}
   
   if gpCount[1]['count(id)']:S() == '0' then
      
      --create the new GP
      sqDB.execute{sql="insert into gp_details (id,firstname,surname,address_1,medical_council_number)"..
         " values (null, " .. conn:quote(gpFirstname)..","..conn:quote(gpSurname)..","..
         conn:quote(gpPractice)..","..conn:quote(gpID)..")", live=false}

      local newGPID1 = sqDB.execute{sql="select last_insert_id() as id", live=true}
      local newGPID = newGPID1[1].id:S()
      
      --now insert it into the facility_gp table
      sqDB.execute{sql="insert into facility_gp (id,facility_id,gp_details,hospital_code,gmc_code,dh_code,m_number)"..
         " values (null,"..conn:quote(faciliityID)..","..conn:quote(newGPID)..",0,0,0,0)", live=false}

      conn:commit{live=true}
      --
      --now update the patient details
      sqDB.execute{sql="update users set gp_details = "..conn:quote(newGPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
   else
      
      --PB 23.05.24 - IR-307 update patient with GP ID
      local GPID = gpCount[1].id:S()
      sqDB.execute{sql="update users set gp_details = "..conn:quote(GPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
      
   end
   --
end

-----------------------------------------------------------------
function sqDB.THS.getFacilityIDClinic(clinCode, hospitalCode)
   
   local clinicCount = sqDB.execute{sql="select count(id), facility_id from facility_clinic_codes"..
      " where clinic_code = " .. conn:quote(clinCode) .. 
      " and hospital_code = " .. conn:quote(hospitalCode), live=true}
   
   if clinicCount[1]['count(id)']:S() == '0' then
      return '0'
   else
      return clinicCount[1].facility_id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.createClinicCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq, clinicCode, clinicDesc)
   
   sqUtils.printLog("Creating clinic capacity...")

   -- first check if we have a template clinic in the env for ipM
   local paramCount = sqDB.execute{sql="select count(id), param_value from application_parameter"..
      " where param_name = 'ipm.parent'", live=true}
   
   if paramCount[1]['count(id)']:S() == '0' then
      error('Application parameter for ipm.parent is not setup. Please add before proceeding.')
   else
      -- find a relevant template clinic to create teh details from
      local templateCount = sqDB.execute{sql="select count(id), id from facilities where parent_id = ".. conn:quote(paramCount[1]['param_value']:S())..
      " and duration = " .. conn:quote(sessionDur), live=true}
      
      if templateCount[1]['count(id)']:S() == '0' then
         error('No template found matching the clinic duration')
      else
         local templateFID = templateCount[1]['id']:S()
         --
         local newFID = createNewFacility(templateFID, clinicDesc)
         --
         createClinicCode(newFID, clinicCode)
         --
         createNewAllowableSlots(newFID, templateFID)
         --
         updateCapacity(newFID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
         --
      end
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
   
   sqUtils.printLog("Updating clinic capacity...")
   --
   -- Assumption here is that the entries are already set up in aloable_slots
   -- This is JUST an update to existing capacity
   
   local start_time1 = sessionStart:sub(1,2)
   local start_time2 = sessionStart:sub(3,4)
   local start_time = start_time1..':'..start_time2
   --
   local end_time1 = sessionEnd:sub(1,2)
   local end_time2 = sessionEnd:sub(3,4)
   local end_time = end_time1..':'..end_time2
   
   local ipm_day1 = days:split(',')[1]
   local ipm_day2 = days:split(',')[2]
   local ipm_day3 = days:split(',')[3]
   local ipm_day4 = days:split(',')[4]
   local ipm_day5 = days:split(',')[5]
   local ipm_day6 = days:split(',')[6]
   local ipm_day7 = days:split(',')[7]
   
   --********NEED to cater for change in durations********---
   local checkDur = checkClinicDur(facilityID, sessionDur)
   
   --********NEED to add in audit trail of changes********---
      
   --if the durations match then it's a straighforward update
   if checkDur == '1' then
      --reset Calendar Days
      sqDB.execute{sql="update calendar_days " .. 
         " set monday = 'N', tuesday = 'N', wednesday = 'N'," ..
         " thursday = 'N', friday = 'N', saturday = 'N'," ..
         " sunday = 'N'"..
         " where facility_id = " .. facilityID, live=false}
      
      --reset capacity slots back to 0
      sqDB.execute{sql="update allowable_slots set allowable = 0" ..
         " where facility_id = " .. facilityID
         , live=false}

      conn:commit{live=true} -- need to commit to prevent conflict with update
   else
      --we will need to create new allowable slots
   end
   --
   local day1 = setAllowableDays(facilityID, ipm_day1)
   local day2 = setAllowableDays(facilityID, ipm_day2)
   local day3 = setAllowableDays(facilityID, ipm_day3)
   local day4 = setAllowableDays(facilityID, ipm_day4)
   local day5 = setAllowableDays(facilityID, ipm_day5)
   local day6 = setAllowableDays(facilityID, ipm_day6)
   local day7 = setAllowableDays(facilityID, ipm_day7)
    
   --Update the relevant sessions
   if day1 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day1)
   end
   --
   if day2 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day2)
   end
   --
   if day3 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day3)
   end
   --
   if day4 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day4)
   end
   --
   if day5 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day5)
   end
   --
   if day6 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day6)
   end
   --
   if day7 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day7)
   end
end

-----------------------------------------------------------------
function sqDB.THS.setAllowableDays(facilityID, day)
   
   sqUtils.printLog("Setting allowable days...")
   
   --
   if day == 'MON' then
      sqDB.execute{sql="update calendar_days set monday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      
      return 'Mon'
      --
   elseif day == 'TUE' then
      sqDB.execute{sql="update calendar_days set tuesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Tue'
      --
   elseif day == 'WED' then
      sqDB.execute{sql="update calendar_days set wednesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Wed'
      --
   elseif day == 'THU' then
      sqDB.execute{sql="update calendar_days set thursday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Thu'
      --
   elseif day == 'FRI' then
      sqDB.execute{sql="update calendar_days set friday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Fri'
      --
   elseif day == 'SAT' then
      sqDB.execute{sql="update calendar_days set saturday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sat'
      --
   elseif day == 'SUN' then
      sqDB.execute{sql="update calendar_days set sunday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sun'
      --
   else
      return '0'
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateAllowableSlots(facilityID, start_time, end_time, day)
   
   sqUtils.printLog("Update allowable slots...")
   --
   --readjust end_time so that it is not included in the update
   sqDB.execute{sql="update allowable_slots set allowable = 1" ..
      " where facility_id = " .. facilityID ..
      " and (time >= " .. conn:quote(start_time) .. 
      " and time < " .. conn:quote(end_time) .. ")" ..
      " and date = " .. conn:quote(day), live=false}
   --
end

-----------------------------------------------------------------
function sqDB.THS.checkClinicDur(facilityID, sessionDur)
   --
   local clinicDur = sqDB.execute{sql="select count(id), duration from facilities"..
      " where id = " ..conn:quote(facilityID), live=true}
   
   if clinicDur[1]['duration']:S() == sessionDur then
      return '1'
   else
      return clinicDur[1].duration:S()
   end

end

-----------------------------------------------------------------
function sqDB.THS.createNewFacility(templateFID, clinicDesc)
   
   sqUtils.printLog("Creating new facility...")
        
         --**************
         --- WHERE DO I GET THE PARENT ID from
         --**************
         
         -- now create the clinic in the facilities table
         local createFID = sqDB.execute{sql="insert into facilities (id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,firstname,"..
            "surname,short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,fkey,hl7_endpoint,"..
            "search_ip_restricted) "..
         "select null as id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,".. conn:quote(clinicDesc).." as firstname,"..
            "surname,".. conn:quote(clinicDesc).. " as short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,null as fkey,hl7_endpoint,"..
            "search_ip_restricted from facilities where id = " .. templateFID, live=true}
         
         local newFID1 = sqDB.execute{sql="select last_insert_id() as id", live=true}
         local newFID = newFID1:S()
         --
         --create the calendar days entries
         local createFIDDays = sqDB.execute{sql="insert into calendar_days (id,timestamp,facility_id,monday,"..
            "tuesday,wednesday,thursday,friday,saturday,sunday) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,monday,tuesday,wednesday,thursday,friday,saturday,"..
            "sunday from calendar_days where facility_id = ".. templateFID, live=true}
         
         --create the calendar actions entries
         local createFIDActions = sqDB.execute{sql="insert into calendar_actions " ..
            "(id,timestamp,facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule from calendar_actions where facility_id = ".. templateFID, live=true}
         
         --create facility_config
         local createFIDConfig = sqDB.execute{sql="insert into facility_config " ..
            "(id,timestamp,facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen) "..
        "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen from facility_config where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
   return newFID
   
end

-----------------------------------------------------------------
function sqDB.THS.createNewAllowableSlots(newFID, templateFID)
   
   sqUtils.printLog("Creating new allowable slots...")
   
   --create the calendar days entries
   local createAllowableSlots = sqDB.execute{sql="insert into allowable_slots (id,timestamp,facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id) "..
      "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id from allowable_slots where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDB.THS.createClinicCode(newFID, sessionCode)
   
   sqUtils.printLog("Creating clinic code...")
   
   --create the facility_clinic_code record
   local createClinicCodes = sqDB.execute{sql="insert into facility_clinic_codes (id,hospital_group_id,facility_id,clinic_code,"..
      "hospital_code,reason_id,active,created_at,updated_at,waiting_status,condition_id,speciality_id,category_id,"..
      "source_id,opd_video) values (null, 14, ".. conn:quote(newFID)..","..conn:quote(sessionCode)..",'NMH',null,0,now(), null,"..
      "'pre_triage',0,0,0,0,0)", live=false}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDB.THS.createSlotDateRange(facilityID, startDate, endDate)
   
   sqUtils.printLog("Creating slot date range...")
   
   --create a new record for allowable_slots_date_ranges
   local createSlotDateRanges = sqDB.execute{sql="insert into allowable_slots_date_ranges (id,facility_id,"..
      "start_date,end_date,active,created_at,updated_at) values ".. 
      "(null,"..conn:quote(facilityID), ","..conn:quote(startDate)..","..conn:quote(endDate)..","..
      "1, now(), now())", live=false}
   
   conn:commit{live=true}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   --
   return result
   --
end

-----------------------------------------------------------------

function sqDB.THS.updateWaitList(patientID, waitListID, controlID, facilityID)
   
   sqUtils.printLog("Updating waitng list...")
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      local res = sqDB.execute{sql="update ewaiting_referrals set covid_19_id = "..conn:quote(waitListID)..
      " where id = "..conn:quote(referralCount[1]['id']:S()), live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------

function sqDB.THS.removeWaitList(patientID, waitListID, facilityID)
   
   sqUtils.printLog("Removing waitng list...")
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
               " user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
   end
   
   --using the waiting list id check if the WL has referrals
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   --if found then set the status to 'removed', which hides it on the SQ front end
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      sqlSTR = "update ewaiting_referrals set status = 'removed' "..
               "where facility_id = " .. facilityID .. " and user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
      
      if suppressFacilityCheck == '1' then
         sqlSTR = "update ewaiting_referrals set status = 'removed' "..
               "where user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
      end
      
      local res = sqDB.execute{sql=sqlSTR, live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------

function sqDB.THS.unRemoveWaitList(patientID, referralID, facilityID)
   
   sqUtils.printLog("Un-Removing waitng list...")
      
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
   end
   
   --using the waiting list id check if the WL has been previously deleted
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   --if found then set the status to 'assigned', which makes it visable again on the fron end
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      sqlSTR = "update ewaiting_referrals set status = 'triage' "..
      "where facility_id = " .. facilityID .. " and user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
      
      if suppressFacilityCheck == '1' then
         sqlSTR = "update ewaiting_referrals set status = 'triage' "..
         "where user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
      end
      
      local res = sqDB.execute{sql=sqlSTR, live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------
function sqDB.THS.userInFacility(patientId, facilityId)
   
   if facilityId ~= nil and facilityId ~= '' then
   
       local result = sqDB.execute{sql="select id from facility_user where "..
       " user_id = " .. patientId .. " "..
       " and facility_id = " .. facilityId .. ";"
       , live=true}
   
     return result[1].id:nodeValue()
   else
      return nil
   end
end

-----------------------------------------------------------------
function sqDB.THS.addToFacility(patientId, facilityId)
	if sqDB.THS.userInFacility(patientId, facilityId) == ''
   then
	   sqDB.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------

--UTILS:

-----------------------------------------------------------------------------

--return the phone number from the PID based on type
function sqDB.THS.getPhoneNumberByType(PID, pType)
   for i=1, #PID[13] do
      if PID[13][i][3]:S() == pType then
         return PID[13][i][1]:S()
      end
   end
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type
function sqDB.THS.getNKPhoneNumberByType(NK1, pType)
   if NK1:childCount() > 1 then
      for i=1, #NK1[5] do
         if NK1[5][i][3]:S() == pType then
            return NK1[5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type, SMS , L14. L15 etc
function sqDB.THS.getNKPhoneNumberByType(NK1, pType)

   if NK1:childCount() >=1 then
      for i=1, #NK1[1][5] do
         if NK1[1][5][i][3]:S() == pType then
            return NK1[1][5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

--------------------------------------------------------------

function sqDB.THS.getPatientName(PID, identifier)
   
   local firstname = ''
   local surname = ''
   local middlename = ''
   local title = ''
   
   for i=1, #PID[5] do
      
      if PID[5][i][7]:S() == identifier then
         
         firstname = PID[5][i][2]:S()
         surname = PID[5][i][1]:S()
         middlename = PID[5][i][3]:S()
         title = PID[5][i][5]:S()
         
         break
      
      end
   
   end
   
   return firstname, surname, middlename, title

end

--------------------------------------------------------------

function sqDB.THS.insertMedicareNumber(patientID)
   
   --[[
      
      NOTE: From Rosie
      
      If a medicare number has no end date then it is the current number
      If all numbers are end dated then we store the number with the latest end date
      
      ]]
   
   trace(patientID, sqDB.patientData.medicareNumber)
   
   if sqUtils.isEmpty(patientID) or sqUtils.isEmpty(sqDB.patientData.medicareNumber) then return end
   
   sqUtils.printLog("Updating medicare numbers for patient ID: "..patientID)
   
   --remove all current medicare numbers for the patient ID
   
   local sqlStr = "delete from user_medical_number_reference where reference_type = 'MC' and user_id = "..patientID
   print(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
      
   --insert the latest medicare numbers
   
   local mcCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode')
   
   local mcNumbers = sqUtils.split(sqDB.patientData.medicareNumber,'|')
   
   local latestEndDate = '0'
   local mcNumber = ''
   local date_from = ''
   local date_to = ''     
   
    
   for i=1, #mcNumbers do
      
      local flds = sqUtils.split(mcNumbers[i],'^')
      trace(flds[1],flds[2],flds[3])
      
      if flds[1] ~= '' then
         
         if flds[3] == '' then

            --no end date to this is the currrent medicare number
            mcNumber = flds[1]
            date_from = conn:quote(sqUtils.parseDateNum(flds[2]))
            if flds[2] == '' then date_from = 'NULL' end
            date_to = conn:quote(sqUtils.parseDateNum(flds[3]))
            if flds[3] == '' then date_to = 'NULL' end

            break

         else
            trace(flds[3],latestEndDate)
            if flds[3] > latestEndDate then

               mcNumber = flds[1]
               date_from = conn:quote(sqUtils.parseDateNum(flds[2]))
               if flds[2] == '' then date_from = 'NULL' end
               date_to = conn:quote(sqUtils.parseDateNum(flds[3]))
               if flds[3] == '' then date_to = 'NULL' end
               
               latestEndDate = flds[3]

            end

         end
         
      end
      
   end
   
   trace(mcNumber,date_from,date_to)
   
   if mcNumber ~= '' then

      local sqlStr="insert into user_medical_number_reference (user_id, reference_type, medical_number, from_date, to_date) " ..
      "values ("..
      patientID .. "," ..
      conn:quote(mcCode)..","..
      conn:quote(mcNumber)..","..date_from..","..
      date_to..
      ")"

      trace(sqlStr)
      print(sqlStr)
      sqDB.execute{sql=sqlStr , live=false}
      
   end
   
end

-----------------------------------------------------------------------------

function sqDB.THS.insertIMedixNumber(patientID)
   
   trace(patientID, sqDB.patientData.imedixNumber)
   
   if sqUtils.isEmpty(patientID) or sqUtils.isEmpty(sqDB.patientData.imedixNumber) then return end
   
   sqUtils.printLog("Updating infomedix number for patient ID: "..patientID)
   
   --remove all current infomedix numbers for the patient ID
   
   local sqlStr = "delete from user_medical_number_reference where reference_type ='PT' and user_id = "..patientID
   print(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
      
   --insert the latest infomedix number
   
   local imCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'iMedixCode')

   local sqlStr="insert into user_medical_number_reference (user_id, reference_type, medical_number) " ..
   "values ("..
   patientID .. "," ..
   conn:quote(imCode)..","..
   conn:quote(sqDB.patientData.imedixNumber)..
   ")"

   trace(sqlStr)
   print(sqlStr)
   sqDB.execute{sql=sqlStr , live=false}
 
end

-----------------------------------------------------------------------------

function sqDB.THS.addUpdateInsurance(IN1, patientID)

   if IN1 == nil or patientID == '0' then
      return
   end
   
   local insurance_code = IN1[2][1]:S()
   local insurance_policy = IN1[36]:S()
   local insurance_company = IN1[4]:S()   -------------- AWAITING PLACEHOLDER ON DB *********

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      patientID, live=true}
   
   if res[1].cnt:S() == '0' then
      sqDB.execute{sql="insert into users_additional_info (user_id, insurance_code, insurance_company, policy_no) " ..
         "values ("..
         patientID .. "," ..
         conn:quote(insurance_code)..","..
         conn:quote(insurance_company)..","..
         conn:quote(insurance_policy)..
         ")"
         , live=false}
   else
      sqDB.execute{sql="update users_additional_info set " ..
         "insurance_code = "..conn:quote(insurance_code)..","..
         "insurance_company = "..conn:quote(insurance_company)..","..
         "policy_no = "..conn:quote(insurance_policy)..
         " where user_id = "..patientID
         , live=false}
   end
   
   sqUtils.printLog("Insurance details updated!")

end

-----------------------------------------------------------------------------

function sqDB.THS.addUpdateInsurance_NEW(patientID, insJson, insText)
   
   --- TODO : get the ID of the insurance company , 99 for testing
   
   local res = sqDB.execute{sql="select count(id) as cnt, param_value from application_parameter ap where param_name = 'patient.insurance_custom_field_id';"
      , live=true}
   
   if res[1].cnt:S() == '0' then
      error("No mapping found for Insurance custom ID")
   end
   
   local customID = res[1].param_value:S()
 
   res = sqDB.execute{sql="select id from user_custom_fields_values where user_id = "..
      conn:quote(patientID).." and facility_user_custom_fields_id = "..customID , live=true}
   
   if #res == 0 then
      
      --insert new insurance
      sqDB.execute{sql='insert into user_custom_fields_values (user_id, uuid, facility_user_custom_fields_id, value, value_str) '..
         ' values ('..conn:quote(patientID)..', uuid(), '..conn:quote(customID)..', '..
         conn:quote(insJson)..', '..conn:quote(insText)..')', live=false}
   else
      
      --update the insurance
      local id = res[1]:S()
      res = sqDB.execute{sql='update user_custom_fields_values '..
         ' SET value = '..conn:quote(insJson)..
         ' , value_str = '..conn:quote(insText)..' where id = '..id, live=false}
   end   
   --
   
end


----------------------------------------------------------------------------

function sqDB.THS.addUpdateUserAdditionalInfo(patientID, middleName, language, aliasFirstname, aliasSurname, aliasMiddlename, indig_status )

   --validate that indig_status exists on DB
   local indig_res = sqDB.execute{sql="select id from indigenous_status where code = "..conn:quote(indig_status) , live=true}
   if #indig_res == 0 then
      sqUtils.printLogW("Indig Status code not found on SQ DB: "..indig_status)
      indig_status=''
   else
      indig_status = indig_res[1].id:S()
   end
   
   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      patientID, live=true}
  
   local strSQL = ''
   
   if res[1].cnt:S() == '0' then

      strSQL = "insert into users_additional_info (user_id, middlename, translator_language, aka_firstname, aka_surname, aka_middle_name"

      if indig_status ~= '' then
         strSQL = strSQL..", indigenous_status_id"
      end
      
      strSQL = strSQL..") values ("..
      patientID .. "," ..
      conn:quote(middleName)..","..
      conn:quote(language)..", "..
      conn:quote(aliasFirstname)..", "..
      conn:quote(aliasSurname)..", "..
      conn:quote(aliasMiddlename)

      if indig_status ~= '' then
         strSQL = strSQL..", "..conn:quote(indig_status)
      end

      strSQL = strSQL..")"

      sqDB.execute{sql=strSQL, live=false}

   else
      
      strSQL = "update users_additional_info set " ..
         "middlename = "..conn:quote(middleName)..", "..
         "translator_language = "..conn:quote(language)..", "..
         "aka_firstname = "..conn:quote(aliasFirstname)..", "..
         "aka_surname = "..conn:quote(aliasSurname)..", "..
         "aka_middle_name = "..conn:quote(aliasMiddlename)
      
      if indig_status ~= '' then
         strSQL = strSQL..", indigenous_status_id = "..conn:quote(indig_status)
      end
      
      strSQL = strSQL.." where user_id = "..patientID
      
      sqDB.execute{sql=strSQL, live=false}
   end

end

----------------------------------------------------------------------------

function sqDB.THS.getParentFacilityId(facilityID)

   local parentFacilityID = ''
   
   local parentFID = sqDB.execute{sql="select parent_id from facilities where id = "..facilityID, live=true}

   if parentFID[1]['parent_id']:S() ~= '0' then
      parentFacilityID = parentFID[1]['parent_id']:S()
   else
      parentFacilityID = facilityID
   end

   return parentFacilityID

end

----------------------------------------------------------------------------

function sqDB.THS.getCSRparentFacilityId(MsgIn)

   --get the default staging facility if no h.org or refer_to found
   local stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultParentFacility_'..MsgIn.MSH[4][1]:S(),tmp)
   if sqUtils.isEmpty(stagingFacility) then
      sqUtils.printLogW('No default staging facility defined for: '..MsgIn.MSH[4][1]:S())
      stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'parentFacilityCatchAll')
   end

   local parentFacilityID = ''

   --What if there are multiple RT's - at the moment just picking the first
   local RT = ''
   for i = 1, #MsgIn.Group2 do
      if (MsgIn.Group2[i].PRD[1][1]:S() == 'RT') then
         RT = MsgIn.Group2[i].PRD
         break
      end
   end
   if RT == '' then
      error("No RT segment found in the referral message!")
   end
   sqUtils.printLog("Referral RT segment: "..RT:S())

   local parentFacilityID = ''
   local consID = ''
   local consName = ''
   
   --determine if we are using the config mapping or CSR lookup
   local useCSRLookup = sqUtils.getJsonMappedValue(sqCfg.extra_params,'useCSRLookup')
  
   --get the parentfacility ID based on 4.1 first
   local clinic = RT[4][1]:S()
   
   if useCSRLookup ~= '1' then

      local tbl7 = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'multiFacilityMap',clinic)
      if not sqUtils.isEmpty(tbl7) then
         local tmp = sqUtils.split(tbl7)
         parentFacilityID = tmp[1]
         consName = tmp[2]
      end
   
   else
      
      parentFacilityID, cons = sqDB.THS.getParentFacilityFromCSR(clinic, MsgIn.MSH[4]:S())
      
   end

   --if lookup fails on 4.1 then try 4.4
   
   if sqUtils.isEmpty(parentFacilityID) then

      clinic = RT[4][4]:S()
      
      if useCSRLookup ~= '1' then
         
         tbl7 = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'multiFacilityMap',clinic)
         if not sqUtils.isEmpty(tbl7) then
            local tmp = sqUtils.split(tbl7,'^')
            parentFacilityID = tmp[1]
            consName = tmp[2]
         end   

      else
         
         parentFacilityID, consID = sqDB.THS.getParentFacilityFromCSR(clinic, MsgIn.MSH[4]:S())

      end

   end

   --if still no parent facilty found then default to a staging area
   if sqUtils.isEmpty(parentFacilityID) then

      parentFacilityID = stagingFacility

   end

   sqUtils.printLog("Multi facility parent facility ID returned: "..parentFacilityID)
   
   --if not using CSR lookup then we need to go back to the DB to find the cons id
   if useCSRLookup ~= '1' then

      if consName == '' then
         consID = '0' -- if cons id not picked up then default to zero
      else

         --lookup the cons ID
         local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_consultants where name = "..
            conn:quote(consName), live=true}

         if res[1].cnt:S() ~= '0' then
            consID = res[1].id:S()
         else
            consID = '0'
         end

      end

   end
    
      
   return parentFacilityID, consID

end

----------------------------------------------------------------------------

function sqDB.THS.checkReferralStaging(MsgIn, patientID, parentFacilityID)
   
   sqUtils.printLog("checkReferralStaging: checking if movement from bucket to clinic facility..")

   --get the default staging facility if no h.org or refer_to found
   local stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultParentFacility_'..MsgIn.MSH[4][1]:S(),tmp)
   if sqUtils.isEmpty(stagingFacility) then
      error('No default staging facility defined for: '..MsgIn.MSH[4][1]:S())
   end
   
   if parentFacilityID == stagingFacility then
      return
   end
   
   --if we got this far then the i13 indicates a movement of a referral out out 
   --the staging area/bucket into a 'proper' facility
   
   local orderID = MsgIn.RF1[6][1]:S()
   
   --find the order for the same patient in the staging facility
   local sRefID = sqDB.THS.getReferralByControlID(orderID , patientID, stagingFacility)
   trace(sRefID)
   
   --referral found in the staging area so set the facility to the new one
   if not sqUtils.isEmpty(sRefID) then
      
      sqUtils.printLog("checkReferralStaging: moving the referral out of staging area..")
      
      local res = sqDB.execute{sql=
      "update ewaiting_referrals set facility_id = "..parentFacilityID..
      " where id = "..sRefID, live=false}
   
   end

end

----------------------------------------------------------------------------

--set all referrals for a patient / facility to be discharged with a reason of deceased
function sqDB.THS.setDeceasedReferral(patientId, refId, facId)

   sqUtils.printLog("setting referral "..refId.." for patient "..patientId.." to discharged..")

   sqDB.execute{sql=
      "update ewaiting_referrals " ..
      "set status = 'discharged' " ..
      "where id = " .. refId .. " and facility_id = "..facId, live=false}   
   
   --Update the ewaiting_discharges table and add a discharge reason id

   local discReasonId = sqUtils.getMappedValue(sqCfg.extra_params,"RIPReasonCode")

   local sqlStr = "insert into ewaiting_discharges (timestamp, res_id, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values ("..
   "now()"..",null"..
   ","..
   refId..","..
   discReasonId..","..
   "''"..","..
   "0"..
   ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}

 
end

----------------------------------------------------------------------------

function sqDB.THS.addUpdatePatientAlerts(patientID,parentFacilityID,MsgIn)
   
   sqUtils.printLog("add/updating patient alert(s) for patient ID: "..patientID)
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --remove existing alerts that where hl7 generated for the patient as 
   --full alert history will come across in message
   local res = sqDB.execute{sql="delete from user_alerts where "..
            "user_id = "..conn:quote(patientID).." and sq_user_id = "..conn:quote(sqUser) , live=false}
   
   for i = 1, #MsgIn.ZPA do  

      local alertStart = MsgIn.ZPA[i][4]:S()
      local alertEnd = MsgIn.ZPA[i][5]:S()
      local alertCode = MsgIn.ZPA[i][3][1]:S()
      local alertDesc = MsgIn.ZPA[i][3][2]:S()
      local alertDomain = MsgIn.ZPA[i][2][1]:S()
      local alertComment = MsgIn.ZPA[i][6]:S()

      --insert the alert
      local sqlStr = "insert into user_alerts (user_id, alert, code, domain, start_date, end_date,sq_user_id, comments, created_At) " ..
      "values ("..conn:quote(patientID)..", ".. 
      conn:quote(alertDesc)..", "..
      conn:quote(alertCode)..", "..
      conn:quote(alertDomain)..", "
      
      if alertStart == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertStart)..", "
      end

      if alertEnd == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertEnd)..", "
      end

      sqlStr = sqlStr..
      conn:quote(sqUser)..", "..
      conn:quote(alertComment)..", "..
      "now()"..
      ")"
      trace(sqlStr)
      sqDB.execute{sql=sqlStr, live=false}

   end
   
end

--------------------------------------------------------------

function sqDB.THS.getMedicareNumbers(PID, idType)
   
   local medicareNumbers = ''
   for i=1, #PID[3] do
      if PID[3][i][5]:S() == idType then
         medicareNumbers = medicareNumbers .. PID[3][i][1]:S().."^".. PID[3][i][7]:S().."^"..PID[3][i][8]:S().."|"
      end
   end
   return medicareNumbers
   
end

--------------------------------------------------------------

function sqDB.THS.getNOKContacts(NK1, nokIdentifier, flag)
   
   --if flag = 1 then search on relationship instead of NK type
   
   local nokContacts = {}
   
   for i=1, #NK1 do
      if NK1[i][7][1]:S() == nokIdentifier or (flag == 1 and NK1[i][3][1]:S() == nokIdentifier) then
         trace(NK1[i])
         table.insert(nokContacts,NK1[i])
      end
   end
  
   return nokContacts
   
end

--------------------------------------------------------------

function sqDB.THS.createNKContact(patientID, nokContacts)
 
   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillNKContactData(patientID, nokContacts)

   --insert the NOK contact as a user
   local sql =       
   "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
   "Address_1, Address_2, Home_Address, Address_4, Date_of_Birth, title, gender, created) " ..
   "values ( " ..
   conn:quote(sqDB.nokData.nk1_parent)..",".. 
   conn:quote(sqDB.nokData.nk1_rel_id) .. "," ..
   "trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), " ..
   conn:quote(sqDB.nokData.nk1_phone)..","..
   -- conn:quote(email)..","..
   conn:quote(sqDB.nokData.nk1_firstname)..","..
   conn:quote(sqDB.nokData.nk1_surname)..","..
   conn:quote(sqDB.nokData.nk1_address_1)..","..
   conn:quote(sqDB.nokData.nk1_address_2)..","..
   conn:quote(sqDB.nokData.nk1_eircode)..","..
   conn:quote(sqDB.nokData.nk1_address_4)..","..
   conn:quote(sqDB.nokData.nk1_dob)..","..
   conn:quote(sqDB.nokData.nk1_title)..","..
   conn:quote(sqDB.nokData.nk1_gender)..","..
   "now()"..
   ")"

   sqDB.execute{sql=sql, live=false}

   local nk1_user_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   sqUtils.printLog("Added new related person record into users table, with id [" ..nk1_user_result[1].id:S() .. 
      "], for patient id [" ..sqDB.nokData.nk1_parent .. "]")

   -- Update related persons users_additional_info record to be next of kin if contact role is NOK

   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')   
   if nokContacts[7][1]:S() == nokIdentifier then
      sqDB.execute{sql=
         "insert into users_additional_info (user_id, act_as_nok)" ..
         "values (" ..conn:quote(nk1_user_result[1].id:S()) .. ", '1')", live=false }

      local nk1_userAddInfo_id = sqDB.execute{sql="select last_insert_id() as id", live=true}

      sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_userAddInfo_id[1].id:S().."], for userID: [" ..nk1_user_result[1].id:S().."]")
   end

end

--------------------------------------------------------------

function sqDB.THS.updateNKContact(id, nokContacts)

   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillNKContactData(id, nokContacts)
   
   local NKUserQueryResultSet = sqDB.execute{sql=
   "select count(id), id from users " ..
   "where parent = " .. id .. 
   " and firstname = "..conn:quote(sqDB.nokData.nk1_firstname).." and surname = "..
      conn:quote(sqDB.nokData.nk1_surname).." and date_of_birth = "..conn:quote(sqDB.nokData.nk1_dob) , live=true}
   
   -- Update existing or create new related person record in user table
   if NKUserQueryResultSet[1]['count(id)']:S() ~= '0' then

      sqDB.nokData.nk1_userID = NKUserQueryResultSet[1]['id']:S()

      sqUtils.printLog("Updating related person details from NK1 segment into users table for patient id: " .. id)

      local sql ="update users " ..
      "set "..
      "firstname = " .. conn:quote(sqDB.nokData.nk1_firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.nokData.nk1_surname) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.nokData.nk1_phone) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.nokData.nk1_address_1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.nokData.nk1_address_2) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.nokData.nk1_eircode) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.nokData.nk1_address_4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.nokData.nk1_address_5) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.nokData.nk1_dob) .. ", "..
      "title = " .. conn:quote(sqDB.nokData.nk1_title) .. ", "..
      "relationship = " .. conn:quote(sqDB.nokData.nk1_rel_id) .. ", "..
      "gender = " .. conn:quote(sqDB.nokData.nk1_gender) ..
      " where id = ".. sqDB.nokData.nk1_userID

      sqDB.execute{sql=sql, live=false}

   elseif NKUserQueryResultSet[1]['count(id)']:S() == '0' then

      sqUtils.printLog("Adding related person details from NK1 segment into users table for patient: " .. id)

      local sql = 
      "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
      "Address_1, Address_2, Home_Address, Address_4, Address_5, title, Date_of_Birth, gender, created) " ..
      "values ( " ..
      conn:quote(sqDB.nokData.nk1_parent)..",".. 
      conn:quote(sqDB.nokData.nk1_rel_id) .. "," ..
      "trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), "..
      conn:quote(sqDB.nokData.nk1_phone)..","..
      -- conn:quote(email)..","..
      conn:quote(sqDB.nokData.nk1_firstname)..","..
      conn:quote(sqDB.nokData.nk1_surname)..","..
      conn:quote(sqDB.nokData.nk1_address_1)..","..
      conn:quote(sqDB.nokData.nk1_address_2)..","..
      conn:quote(sqDB.nokData.nk1_eircode)..","..
      conn:quote(sqDB.nokData.nk1_address_4)..","..
      conn:quote(sqDB.nokData.nk1_address_5)..","..
      conn:quote(sqDB.nokData.nk1_title)..","..
      conn:quote(sqDB.nokData.nk1_dob)..","..
      conn:quote(sqDB.nokData.nk1_gender)..","..
      "now()"..
      ")"

      sqDB.execute{sql=sql, live=false}

      local nk1_user_resultSet = sqDB.execute{sql="select last_insert_id() as id", live=true}
      sqDB.nokData.nk1_userID = nk1_user_resultSet[1].id:S()

      sqUtils.printLog("Added new related person record into users table, with id [" ..sqDB.nokData.nk1_userID .. 
         "], for patient ID [" ..sqDB.nokData.nk1_parent .. "]")

   end -- End users record added/updated in users table

   -- If related person is NOK, 
   -- then update existing, or create new, user_additional_info record
   -- and set act_as_nok to '1' (true)

   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')  
   if nokContacts[7][1]:S() == nokIdentifier then

      local NKUserAddInfoResultSet = sqDB.execute{sql=
         "select count(id), id from users_additional_info " ..
         "where user_id = " .. sqDB.nokData.nk1_userID, live=true}

      -- If users_additional_info record exists, update it
      if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
         local sql = "update users_additional_info " ..
         "set act_as_nok = '1' " .. 
         "where id = ".. NKUserAddInfoResultSet[1]['id']:S()

         sqDB.execute{sql=sql, live=false}

         sqUtils.printLog("Added NOK category status to users_additional_info for user ID [" .. sqDB.nokData.nk1_userID .. "]")

         -- else if users_additional_info record does not exist, then create it
      elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

         local sql = "insert into users_additional_info (user_id, act_as_nok) " ..
         "values (" ..conn:quote(sqDB.nokData.nk1_userID) .. ", '1')"

         sqDB.execute{sql=sql, live=false}

         local nk1_act_as_nok_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

         --[[sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_act_as_nok_result[1].id:S()..
            "], for userID: [" ..sqDB.nokData.nk1_userID.."]")
         ]]

      end            
   end -- End NK1[7][1] == NOK
   
   
   -- If related person is emergency contact, 
   -- then update existing, or create new, user_additional_info record
   -- and set emergency_contact to '1' (true)

   local emergIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emergencyContactIdentifier')  
   if nokContacts[7][1]:S() == emergIdentifier then

      local NKUserAddInfoResultSet = sqDB.execute{sql=
         "select count(id), id from users_additional_info " ..
         "where user_id = " .. sqDB.nokData.nk1_userID, live=true}

      -- If users_additional_info record exists, update it
      if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
         local sql = "update users_additional_info " ..
         "set emergency_contact = '1' " .. 
         "where id = ".. NKUserAddInfoResultSet[1]['id']:S()

         sqDB.execute{sql=sql, live=false}

         sqUtils.printLog("Added emergency category status to users_additional_info for user ID [" .. sqDB.nokData.nk1_userID .. "]")

         -- else if users_additional_info record does not exist, then create it
      elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

         local sql = "insert into users_additional_info (user_id, emergency_contact) " ..
         "values (" ..conn:quote(sqDB.nokData.nk1_userID) .. ", '1')"

         sqDB.execute{sql=sql, live=false}

         local nk1_act_as_emergency_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

         sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_act_as_emergency_result[1].id:S()..
            "], for userID: [" ..sqDB.nokData.nk1_userID.."]")
         

      end            
   end -- End NK1[7][1] == EMERG

   
end

--------------------------------------------------------------

function sqDB.THS.updateGTContact(id, gtContact)

   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillGTContactData(id, gtContact)
   
   local GTUserQueryResultSet = sqDB.execute{sql=
   "select count(id), id from users " ..
   "where parent = " .. id .. 
   " and firstname = "..conn:quote(sqDB.gtData.gt1_firstname).." and surname = "..
      conn:quote(sqDB.gtData.gt1_surname).." and date_of_birth = "..conn:quote(sqDB.gtData.gt1_dob) , live=true}
   
   -- Update existing or create new related person record in user table
   if GTUserQueryResultSet[1]['count(id)']:S() ~= '0' then

      sqDB.gtData.gt1_userID = GTUserQueryResultSet[1]['id']:S()

      sqUtils.printLog("Updating related person details from GT1 segment into users table for patient id: " .. id)

      local sql ="update users " ..
      "set "..
      "firstname = " .. conn:quote(sqDB.gtData.gt1_firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.gtData.gt1_surname) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.gtData.gt1_mobile_format) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.gtData.gt1_phone) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.gtData.gt1_address_1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.gtData.gt1_address_2) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.gtData.gt1_eircode) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.gtData.gt1_address_4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.gtData.gt1_address_5) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.gtData.gt1_dob) .. ", "..
      "title = " .. conn:quote(sqDB.gtData.gt1_title) .. ", "..
      "relationship = " .. conn:quote(sqDB.gtData.gt1_rel_id) .. ", "..
      "gender = " .. conn:quote(sqDB.gtData.gt1_gender) ..
      " where id = ".. sqDB.gtData.gt1_userID

      sqDB.execute{sql=sql, live=false}

   elseif GTUserQueryResultSet[1]['count(id)']:S() == '0' then

      sqUtils.printLog("Adding related person details from GT1 segment into users table for patient: " .. id)

      local sql = 
      "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
      "Address_1, Address_2, Home_Address, Address_4, Address_5, title, Date_of_Birth, gender, created) " ..
      "values ( " ..
      conn:quote(sqDB.gtData.gt1_parent)..",".. 
      conn:quote(sqDB.gtData.gt1_rel_id) .. "," ..
      "trim(replace(" .. conn:quote(sqDB.gtData.gt1_mobile_format) .. ", ' ', '')), "..
      conn:quote(sqDB.gtData.gt1_phone)..","..
      -- conn:quote(email)..","..
      conn:quote(sqDB.gtData.gt1_firstname)..","..
      conn:quote(sqDB.gtData.gt1_surname)..","..
      conn:quote(sqDB.gtData.gt1_address_1)..","..
      conn:quote(sqDB.gtData.gt1_address_2)..","..
      conn:quote(sqDB.gtData.gt1_eircode)..","..
      conn:quote(sqDB.gtData.gt1_address_4)..","..
      conn:quote(sqDB.gtData.gt1_address_5)..","..
      conn:quote(sqDB.gtData.gt1_title)..","..
      conn:quote(sqDB.gtData.gt1_dob)..","..
      conn:quote(sqDB.gtData.gt1_gender)..","..
      "now()"..
      ")"

      sqDB.execute{sql=sql, live=false}

      local gt1_user_resultSet = sqDB.execute{sql="select last_insert_id() as id", live=true}
      sqDB.gtData.gt1_userID = gt1_user_resultSet[1].id:S()

      sqUtils.printLog("Added new related person record into users table, with id [" ..sqDB.gtData.gt1_userID .. 
         "], for patient ID [" ..sqDB.gtData.gt1_parent .. "]")

   end -- End users record added/updated in users table
   
end

--------------------------------------------------------------

function sqDB.THS.getSQFacilityToIPMMApping(facilityId)
   
   local clinic = ''
   local session = ''
   local cons = ''
   local spec = ''
   
   local res = sqDB.execute{sql=
      "select count(id) as cnt, clinic_code, session_code, consultant_code, speciality_id from facility_clinic_codes where facility_id = "..facilityId, live=true}
   
   if res[1].cnt:S() == '0' then
      error("Missing mapping in CSR table for facility id: "..facilityId)
   end
   
   clinic = res[1].clinic_code:S()
   session = res[1].session_code:S()
   cons = res[1].consultant_code:S()
   
   --lookup the speciality
   res = sqDB.execute{sql=
      "SELECT count(s.id) as cnt, s.code as specCode FROM specialities s "..
      " JOIN facility_specialities fs ON fs.speciality_id = s.id ".. 
      " JOIN facility_clinic_codes fcc ON fcc.facility_id = fs.facility_id "..
      " where fcc.facility_id = "..facilityId, live=true}
   
   if res[1].cnt:S() == '0' then
      error("Missing speciality for facility id: "..facilityId)
   end
   local spec = res[1].specCode:S()
   
   
   return clinic, session, cons, spec
  
   
end

--------------------------------------------------------------

function sqDB.THS.getParentFacilityFromCSR(clinic, hospCode)
   
   local parentFacility = ''
   local cons = ''
   
   local res = sqDB.execute{sql=
      "select count(fcc.id) as cnt, f.parent_id, fcc.consultant_code from facility_clinic_codes fcc join facilities f on f.id = fcc.facility_id "..
      "where clinic_code = "..conn:quote(clinic).." and hospital_code = "..conn:quote(hospCode).." limit 1", live=true}
   
   if res[1].cnt:S() ~= '0' then

      parentFacility = res[1].parent_id:S()
      cons = res[1].consultant_code:S()

   end
   
   return parentFacility, cons
   
end

--------------------------------------------------------------

function sqDB.THS.getFacilityFromCSR(site, clinic, session)
   
   local facilityID = ''
   
   local res = sqDB.execute{sql=
      "select count(id) as cnt, facility_id from facility_clinic_codes where hospital_code = "..conn:quote(site)..
      " and clinic_code = "..conn:quote(clinic).." and session_code = "..conn:quote(session), live=true}
   
   if res[1].cnt:S() ~= '1' then
      error("Facility ID not found in CSR table for ("..site.."|"..clinic.."|"..session..")")
   end
   
   facilityID = res[1].facility_id:S()
 
   return facilityID
   
end

--------------------------------------------------------------

function sqDB.THS.closePatientReferral(id, facilityID, controlID, disOutcome)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where "..
      " user_id = " .. id .. " and control_id = "..conn:quote(controlID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping closure...")
      return
   end
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..   " where id = ".. referralID
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   --get the discharge reason
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_discharge_reasons where "..
      " code = " .. conn:quote(disOutcome).." and facility_id = "..facilityID, live=true}

   local disReasonID = 0
   if res[1].cnt:S() ~= '0' then
      disReasonID = res[1].id:S()
   end
   
   local sqlStr = "insert into ewaiting_discharges (timestamp, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values (".. "now()"..",".. referralID..",".. disReasonID..",".. "''"..",".. sqUser.. ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}

   sqUtils.printLog("Closeure / Discharging of referral completed!")
   
end


return sqDB