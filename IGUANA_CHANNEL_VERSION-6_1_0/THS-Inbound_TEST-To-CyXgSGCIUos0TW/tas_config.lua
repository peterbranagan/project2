-- Implementation details for Tas
ClientConfig = {

   defaultAppDuration = '30',
   assigningFacility = 'PAS',
   medicareCode = 'MC',
   defaultReferralWaitingStatus = 'triage',
   defaultFacilitySourceId = '410', --GP
   
   --mapping SQ appointment reason code to IPM visit code for PV1[4]
   --will need further mapping
   appReasonMap = [[ 
   { 
     'SQ-TEST1' : 
        {
          'NW' : '5830',
          'RV' : '5835',
        }
   }
   ]]
   
}

ClientConfig['DbFile'] = '/etc/iNTERFACEWARE/test_au_db_credentials'
ClientConfig['facility'] = {}
ClientConfig['facility']['TAS'] = 6