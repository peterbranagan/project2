-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.

require 'dateparse'
require 'patient-utils'
require 'common-utils'
require 'tas_config'

local dbfile = ClientConfig['DbFile']

local raw_db_credentials = readFile(dbfile)

dbo = raw_db_credentials:split('\n')

local db_name = dbo[1]
local db_user = dbo[2]
local db_pass = dbo[3]

conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}

function main(Data)
      
   iguana.stopOnError(false)

   if not conn or not conn:check()
      then
      if conn
         then 
         conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}
      end
	   conn:close()
      conn = db.connect{api=db.MY_SQL, name=db_name, user=db_user, password=db_pass, live=true}
   end
      
   conn:begin{}
   
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}

   -- Parse the incoming message
   local MsgIn = hl7.parse{vmd='dxc_ipm_tas2.vmd', data=Data}
   
   local msgType  = MsgIn.MSH[9][1]:S()
   
   local PIDcheck = MsgIn
   
   --Check if it is an MFN Message i.e. process ADT and SIU Messages 
   if MsgIn.MSH[9][1]:S() ~= 'MFN' then
      
      local ipm_mrn = 0
      local sq_mrn = 0
      local sq_referral_id ='0'

      i = 1
      while i <= 4 do
         --
         local mrn_code = MsgIn.PID[3][i][5]:S()
         local mrn_value = MsgIn.PID[3][i][1]:S()

         if mrn_code == 'PAS' then
            ipm_mrn = mrn_value
         elseif mrn_code == 'EMR' then
            sq_mrn = mrn_value
         elseif mrn_code == 'EAN' then
            sq_referral_id = mrn_value
         end   
         i = i + 1
      end
      --
      local mrn = ipm_mrn
      
      --local mrn = MsgIn.PID[3][1][1]:S()

      --local mrn = MsgIn.PID[3][2][1]:S()

      local toProcess = 'Yes'

      local PIDID = MsgIn.PID[3][1][1]
      --   

      --local patientID = getPatientIdByMrn(mrn)

      local sendingFacility = MsgIn.MSH[4][1]:nodeValue()
      local facilityID = ClientConfig.facility.TAS
      local parentFacilityID = nil;
       
      if MsgIn.MSH[9][1]:S() ~= 'REF' and MsgIn.MSH[9][2]:S() ~= 'A34' and MsgIn.MSH[9][2]:S() ~= 'A40'then
         local apptType = MsgIn.PV2[12]:S()
         local apptPriority = MsgIn.PV2[25]:S()
         local apptCategory = MsgIn.PV1[18]:S()

         local clinCode1 = MsgIn.PV1[3][2]:S()
         --local clinCode1 = MsgIn.PV1[
         
         local reasonCode = MsgIn.PV1[4]:S()

         local reasonID = 1

         local clinCode = MsgIn.PV1[3][1]:S()
      end
      --local clinCode = MsgIn.PV1[3][2]:S()

      --facilityID = getFacilityIDClinic(clinCode, 'TAS')
      
      --find the parent facility_id
      
      local parentFID = conn:execute{sql="select parent_id from facilities where id = "..facilityID, live=true}
   
      if parentFID[1]['parent_id']:S() ~= '0' then
         parentFacilityID = parentFID[1]['parent_id']:S()
      else
         parentFacilityID = facilityID
      end
      --
      local patientID = getPatientIdByMrn(mrn, parentFacilityID)

      local parentMobile = 'No'

      if facilityID ~= nil
         then
         if toProcess == 'Yes' then     
            if patientID == '0'
               then
               if MsgIn.MSH[9][2]:S() == 'A28' then
                  patientID = createPatient(MsgIn.PID, mrn,MsgIn.NK1)  
               else
                  patientID = createPatient(MsgIn.PID, mrn,nil)
               end
               --
               if MsgIn.MSH[9][1]:S() == 'ADT' then
                  createGPDetails(patientID, MsgIn.PD1, facilityID)
               end

               if MsgIn.MSH[9][2]:S() == 'A40' or MsgIn.MSH[9][2]:S() == 'A34'
                  then
                  local minorPatient = getPatientIdByMrn(mrn,parentFacilityID)

                  mergePatient(patientID,minorPatient,facilityID)
               end
            else
               if MsgIn.MSH[9][2]:S() == 'A28' then
                  updatePatient(patientID, MsgIn.PID, parentMobile, mrn,MsgIn.NK1)
               else
                  updatePatient(patientID, MsgIn.PID, parentMobile, mrn,nil)
               end
               
               if MsgIn.MSH[9][1]:S() == 'ADT' then
                  createGPDetails(patientID, MsgIn.PD1, facilityID)
               end
               
               if MsgIn.MSH[9][2]:S() == 'A40' or MsgIn.MSH[9][2]:S() == 'A34'
                  then
                  local minorPatient = getPatientIdByMrn(mrn,parentFacilityID)

                  mergePatient(patientID,minorPatient,facilityID)
               end
            end       

            addToFacility(patientID, parentFacilityID)

            --Check if it's an A05
            if MsgIn.MSH[9][2]:S() == 'A05' then

               local waitListID = MsgIn.PV1[5][1]:S()
               local referralID = ''
               
               --   
               -- Need to find ROL segment where ROL[1][2] = 'REFRL'
               --  then referralID = ROL[1][1] 
               local rolGroup = MsgIn:child("ROL", 2)
               
               for i = 1, #rolGroup do
                  if (rolGroup[i][1][2]:S() == 'REFRL') then
                     trace("ROL segment found with namespace \'REFRL\'")
                     referralID = rolGroup[i][1][1]:S()
                     break
                  end
               end 
               if referralID == '' then
                  iguana.logDebug("Unable to find referral ID in ROL segment for message control ID [" .. 
                     MsgIn.MSH[10] .. "]")                  
               end

               --  
               --Update the referral if one exists - it should
               updateWaitList(patientID,waitListID,referralID,parentFacilityID)
            end
            
            -- check if the message is about the waiting list
            if MsgIn.MSH[9][2]:S() == 'I12'then
              
               local refSourceSegment
               for i = 1, #MsgIn.Group2 do
                  if (MsgIn.Group2[i].PRD[1][1]:S() == 'RP') then
                     trace("RP segment found")
                     refSourceSegment = MsgIn.Group2[i].PRD
                     break
                  end
               end               
               
               if refSourceSegment ~= nil then 
                  createPatientReferral(patientID, parentFacilityID, clinCode,MsgIn.RF1,refSourceSegment)
               else
                  iguana.logError("Unable to process referral as no PRD segment with a Referral Source present")
               end
               --
            end
            --
            if MsgIn.MSH[9][2]:S() == 'I13'
               then
               local referralID = getPatientReferral(patientID,parentFacilityID,MsgIn.RF1)
               --
               if referralID ~= '0' then
                  --update referral
                  updatePatientReferral(patientID, parentFacilityID, clinCode,MsgIn.RF1)
               else
                  local refSourceSegment
                  for i = 1, #MsgIn.Group2 do
                     if (MsgIn.Group2[i].PRD[1][1]:S() == 'RP') then
                        trace("RP segment found")
                        refSourceSegment = MsgIn.Group2[i].PRD
                        break
                     end
                  end               

                  if refSourceSegment ~= nil then 
                     createPatientReferral(patientID, parentFacilityID, clinCode,MsgIn.RF1,refSourceSegment)
                  else
                     iguana.logError("Unable to process referral as no PRD segment with a Referral Source present")
                  end
               end
            end
            --
         else
            if patientID ~= '0'
               then
               if MsgIn.MSH[9][2]:S() == 'A28' then
                  updatePatient(patientID, MsgIn.PID, parentMobile, mrn,MsgIn.NK1)
               else
                  updatePatient(patientID, MsgIn.PID, parentMobile, mrn,nil)
               end
               --
               if MsgIn.MSH[9][2]:S() == 'A40' or MsgIn.MSH[9][2]:S() == 'A34'
                  then
                  local minorPatient = getPatientIdByMrn(mrn,parentFacilityID)

                  mergePatient(patientID,minorPatient,facilityID)
               end
            else
               if MsgIn.MSH[9][2]:S() == 'A40' or MsgIn.MSH[9][2]:S() == 'A34'
                  then
                  local minorPatient = getPatientIdByMrn(mrn,parentFacilityID)

                  patientID = createPatient(MsgIn.PID, mrn,nil)
                  addToFacility(patientID, parentFacilityID)
                  mergePatient(patientID,minorPatient,facilityID)
               end
            end
         end

         local msgType = MsgIn.MSH[9][1]:S()

         if msgType == 'SIU' then
            AppStatus = MsgIn.SCH[25][1]:S()
            clinCode1 = MsgIn.PV1[3][1]:S()
         end
         
         
         local reasonID = 1

         -- check to see if it is an SU message so we can create, update or cancel the appointment
         if MsgIn.MSH[9][2]:S() == 'S12' then
            
            createAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, MsgIn.PV2, MsgIn.PV1, MsgIn.AIP);
         end
         --
         if MsgIn.MSH[9][2]:S() == 'S13' then
            if AppStatus == 'Attended' then
               updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID);
            elseif AppStatus == 'CheckedOut' then
               updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID);
            else
               updateAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, MsgIn.PV2, MsgIn.PV1,MsgIn.AIP);
            end
         end
         --
         if MsgIn.MSH[9][2]:S() == 'S14' then
            --
            if AppStatus == 'Attended' then
               updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID);
            elseif AppStatus == 'CheckedOut' then
               updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID);
            else
               updateAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, MsgIn.PV2, MsgIn.PV1,MsgIn.AIP);
            end
         end
         --
         if MsgIn.MSH[9][2]:S() == 'S15' then
            cancelAppointment(patientID, facilityID, MsgIn.SCH, reasonID);
         end
         --
         if MsgIn.MSH[9][2]:S() == 'S26' then
            dnaAppointment(patientID, facilityID, MsgIn.SCH, reasonID);
         end

         -- check if its an ADMIN message
         local msgType = MsgIn.MSH[9][1]:S()
         --
         
         --DO WE NEED THIS - SEEMS LIKE A DUPLICATE OF THE ABOVE CODE !!!!
         if MsgIn.MSH[9][1]:S() == 'REF' then
            if not MsgIn.Group2[2].PRD:isNull() then 
               createPatientReferral(patientID, parentFacilityID, clinCode,MsgIn.RF1,MsgIn.Group2[2].PRD)
            else
               createPatientReferral(patientID, parentFacilityID, clinCode,MsgIn.RF1,MsgIn.Group2[1].PRD)
            end
         end
         --
      else
         error('Could not get facilityID')
      end
   --Process the MFN message
   else
      -- Process the MFN
      -- First Check do we have an existing session code for this clinic. 
      -- If we do process the update, if not then create a new clinic
      
      local clinicCode = MsgIn.LOC[1][1]:S()
      local clinicDesc = MsgIn.LOC[2]:S()
      local facilityID = getFacilityIDClinic(clinicCode, 'NMH')
      
      local days = MsgIn.Z01[1][1]:S()
      local sessionStart = MsgIn.Z01[2]:S()
      local sessionEnd = MsgIn.Z01[3]:S()
      local sessionDur = MsgIn.Z01[4]:S()
      local sessionFreq = MsgIn.Z01[5][1]:S()
      --
      if facilityID ~= '0' then
      --update the session capacity settings
         updateCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
      else
      --create new clinic and session capacity
      --******NEED TO ADD********--
          --local test = '0'
         createClinicCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq, clinicCode, clinicDesc)
      end

   end
   --
   conn:commit{live=true}
end

function getPatientIdByMrn(mrn, facilityID)
   
   local PatientId = conn:execute{sql="select count(id), id from users where mrn = " .. conn:quote(mrn) ..
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = "..facilityID..")", live=true}

   if PatientId[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return PatientId[1].id:S()
   end
   
end

function mergePatient(id, minorid, facilityID)
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = conn:execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request")
   end

   local isMinorPatientValid = false
   if (minorid ~= '0') then
      trace(minorid)
      local facilityUserCheck = conn:execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request")
   end
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   --Referrals First
   local MergeRefs = conn:execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = conn:execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}
      --
      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{HMC HL7 Merge - Minor ID: " .. minorid .. " Major ID: " .. id ..
         "ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"
         iguana.logDebug("mergePatient().Insert merged user details: " .. sql)
         conn:execute{sql=sql, live=false}
      end
      --
      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID) ..
      " and user_id = " .. minorid
      iguana.logDebug(" mergePatient(). Update ewaiting_referrals: " .. sql)
      conn:execute{sql=sql, live=false}
      --
   end
   --
   --Reservations Second
   local MergeRes = conn:execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}
   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = conn:execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}
      --
      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         conn:execute{sql=sql, live=false}
      end
      --
      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) .. 
      " or parent_id = " .. conn:quote(facilityID) .. ")" .. 
      " and user_id = " .. minorid
      conn:execute{sql=sql, live=false}
      --
   end
   --
   --Patient Flow Third
   local MergePatFlow = conn:execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = conn:execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}
      --
      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{THS HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         conn:execute{sql=sql, live=false}
      end
      --
      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) .. 
      " or parent_id = " .. conn:quote(facilityID) .. ")"..
      " and user_id = " .. minorid
      conn:execute{sql=sql, live=false}
      --
   end
   --
   -- Remove the minor one from the Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) .. 
   " and user_id = " .. minorid
   conn:execute{sql=sql, live=false}
   --
end   
   
   
   
   
   
   

function createPatient(PID, mrn, NK1)
   
   --ocal mrn = PID[3][1][1]:S()
   --local mrn = PID[3][2][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local user_type = "N"
   local dob = PID[7]:parseDOB()
   local gender = PID[8]:nodeValue()   
   local home_phone = ''
   local mobile = ''
   local email = ''
   local title = PID[5][5]:S()
   local Home_Address = PID[11][1][5]:S()
   
   if PID[13][1][3]:S() == 'PH' then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][3]:S() == 'CP' then
      mobile = PID[13][1][1]:S()
   elseif PID[13][1][3]:S() == 'Internet' then
      email = PID[13][1][1]:S()
   end
   --
   if PID[13][2][3]:S() == 'PH' then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][3]:S() == 'CP' then
      mobile = PID[13][2][1]:S()
   elseif PID[13][2][3]:S() == 'Internet' then
      email = PID[13][2][1]:S()
   end
   --
   if PID[13][3][3]:S() == 'PH' then
      home_phone = PID[13][3][1]:S()
   elseif PID[13][3][3]:S() == 'CP' then
      mobile = PID[13][3][1]:S()
   elseif PID[13][3][3]:S() == 'Internet' then
      email = PID[13][3][1]:S()
   end
   --   
   local mobileint = ''
   
   if mobile:sub(1,2) == '04' then
      mobileint = '61' .. mobile:sub(2)
   else
      mobileint = mobile
   end
   --local home_phone = PID[13][1][1]:S()
   --local mobile = PID[13][2][1]:S()
   
   local address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local address5 = '' --PID[11][1][5]:nodeValue()
   
   --if mobile:len() == 0
   --   then   
      conn:execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created) " ..
         "values (trim(replace("..
         conn:quote(mobileint)..", ' ', '')), "..
         conn:quote(home_phone)..","..
         conn:quote(email)..","..
         conn:quote(mrn)..","..
         conn:quote(firstname)..","..
         conn:quote(surname)..","..
         conn:quote(user_type)..","..
         conn:quote(dob)..","..
         conn:quote(gender)..","..
         conn:quote(address1)..","..
         conn:quote(address2)..","..
         conn:quote(address3)..","..
         conn:quote(address4)..","..
         conn:quote(address5)..","..
         conn:quote(Home_Address)..","..
         conn:quote(title)..","..
         "'active', now()"..
         ")"
         , live=false}
   
   local result = conn:execute{sql="select last_insert_id() as id", live=true}
   local patientID = result[1].id:S()
   iguana.logDebug("Created patient with id: [" ..patientID.."]")
   --[[
   IR-43: 09/01/2023: Chris Ireland: Process optional NK1 segment
   If an NK1 segment is present:
   Add a user record to record details of the related person
   Add a user_additional_info record to register the NoK contact status
   Assertion: No NoK user record can already exist for that patient and relationship as patient record is being created
   ** Limitation: Only the first NK1 segment is processed. **
   ]]--      

   if NK1 ~= nil then
      if not (NK1:isNull()) then
         local nk1_parent = result[1].id:S()
         local nk1_firstname = NK1[2][2]:S()
         local nk1_surname = NK1[2][1]:S()
         local nk1_rel_code =  NK1[3][1]:S()
         if (nk1_rel_code == '') then
            iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
         end
         local nk1_rel_id = conn:execute{sql="select id from family_relationship where code = "  .. conn:quote(nk1_rel_code), live=true}        
         local nk1_mobile = ''
         local nk1_mobile_format = ''
         if not NK1[6][1]:isNull() then
            nk1_mobile = NK1[6][1]:S()
            if nk1_mobile:sub(1,2) == '04' then
               nk1_mobile_format = '61' .. nk1_mobile:sub(2)
            else
               nk1_mobile_format = nk1_mobile
            end 
         end         
         local nk1_phone = ''
         if not isEmpty(NK1[5][1]:S()) then
            nk1_phone = NK1[5][1]:S()
         end
         local nk1_address_1 = NK1[4][1]:S()
         local nk1_address_2 = NK1[4][2]:S()
         local nk1_address_4 = NK1[4][3]:S()
         local nk1_eircode = NK1[4][5]:S()

         local sql =       
            "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
            "Address_1, Address_2, Home_Address, Address_4, created) " ..
            "values ( " ..
            conn:quote(nk1_parent)..",".. 
            conn:quote(nk1_rel_id[1]:S()) .. "," ..
            "trim(replace(" .. conn:quote(nk1_mobile_format) .. ", ' ', '')), " ..
            conn:quote(nk1_phone)..","..
            -- conn:quote(email)..","..
            conn:quote(nk1_firstname)..","..
            conn:quote(nk1_surname)..","..
            conn:quote(nk1_address_1)..","..
            conn:quote(nk1_address_2)..","..
            conn:quote(nk1_eircode)..","..
            conn:quote(nk1_address_4)..","..
            "now()"..
            ")"

         conn:execute{sql=sql, live=false}
         iguana.logDebug(sql)
         
         local nk1_user_result = conn:execute{sql="select last_insert_id() as id", live=true}

         iguana.logDebug("Added new related person record into users table, with id [" ..nk1_user_result[1].id:S() .. 
            "], for patient id [" ..nk1_parent .. "]")

         -- Update related persons users_additional_info record to be next of kin if contact role is NOK
         if (NK1[7][1]:S() == 'NOK') then
            conn:execute{sql=
               "insert into users_additional_info (user_id, act_as_nok)" ..
               "values (" ..conn:quote(nk1_user_result[1].id:S()) .. ", '1')", live=false }

            local nk1_userAddInfo_id = conn:execute{sql="select last_insert_id() as id", live=true}
            
            iguana.logDebug("Created user_additional_info record with id: [" ..nk1_userAddInfo_id[1].id:S().."], for userID: [" ..nk1_user_result[1].id:S().."]")
         end
      end  
   end
   
   return result[1].id:S()
   
end

function updatePatient(id, PID, parentMobile, mrn, NK1)
   
   --local mrn = PID[3][1][1]:S()
   --local mrn = PID[3][2][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local dob = PID[7]:parseDOB()
   local gender = PID[8]:nodeValue()
   local home_phone = ''
   local mobile = ''
   local email = ''
   local Home_Address = PID[11][1][5]:S()
   local title = PID[5][5]:S()
   
   if PID[13][1][3]:S() == 'PH' then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][3]:S() == 'CP' then
      mobile = PID[13][1][1]:S()
   elseif PID[13][1][3]:S() == 'Internet' then
      email = PID[13][1][1]:S()
   end
   --
   if PID[13][2][3]:S() == 'PH' then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][3]:S() == 'CP' then
      mobile = PID[13][2][1]:S()
   elseif PID[13][2][3]:S() == 'Internet' then
      email = PID[13][2][1]:S()
   end
   --
   if PID[13][3][3]:S() == 'PH' then
      home_phone = PID[13][3][1]:S()
   elseif PID[13][3][3]:S() == 'CP' then
      mobile = PID[13][3][1]:S()
   elseif PID[13][3][3]:S() == 'Internet' then
      email = PID[13][3][1]:S()
   end
   -- 
   local mobileint = ''
   
   if mobile:sub(1,2) == '04' then
      mobileint = '61' .. mobile:sub(2)
   else
      mobileint = mobile
   end
   --
   if mobileint == '' then
      if parentMobile ~= 'No' then
         if parentMobile:sub(1,2) == '04' then
            mobileint = '61' .. parentMobile:sub(2)
         end
      end
   end
   -- local mobile = PID[13][2][1]:nodeValue()
   -- local home_phone = PID[13][1][1]:nodeValue()  
   local address1 = PID[11][1][1]:nodeValue()
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local address5 = '' --PID[11][1][5]:nodeValue()
   
  -- if mobile:len() == 0
  --    then
      local sql ="update users " ..
      "set "..
      "mrn = " .. conn:quote(mrn) .. ", "..
      "firstname = " .. conn:quote(firstname) .. ", "..
      "surname = " .. conn:quote(surname) .. ", "..
      "Date_of_Birth = " .. conn:quote(dob) .. ", "..
      "mobile = trim(replace(" .. conn:quote(mobileint) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(home_phone) .. ", "..
      "email = " .. conn:quote(email) .. ", "..
      "Gender = " .. conn:quote(gender) .. ", "..
      "Address_1 = " .. conn:quote(address1) .. ", "..
      "Address_2 = " .. conn:quote(address2) .. ", "..
      "Address_3 = " .. conn:quote(address3) .. ", "..
      "Address_4 = " .. conn:quote(address4) .. ", "..
      "Address_5 = " .. conn:quote(address5) .. ", "..
      "Home_Address = " .. conn:quote(Home_Address) .. ", "..
      "title = " .. conn:quote(title) .. ", "..
      "modified = now() " ..
      "where id = ".. id
   
      conn:execute{sql=sql, live=false}

   if NK1 ~= nil then
      if not (NK1:isNull()) then

         local nk1_userID = ''
         local nk1_parent = id
         local nk1_firstname = NK1[2][2]:S()
         local nk1_surname = NK1[2][1]:S()
         local nk1_rel_code =  NK1[3][1]:S()
         if (nk1_rel_code == '') then
            iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..id.. "] with no relationship defined")
         end
         local nk1_rel_id = conn:execute{sql="select id from family_relationship where code = "  .. conn:quote(nk1_rel_code), live=true}
         local nk1_mobile = ''
         local nk1_mobile_format = ''
         if not NK1[6][1]:isNull() then
            nk1_mobile = NK1[6][1]:S()
            if nk1_mobile:sub(1,2) == '08' then
               nk1_mobile_format = '353' .. nk1_mobile:sub(2)
            else
               nk1_mobile_format = nk1_mobile
            end 
         end
         local nk1_phone = ''
         if not isEmpty(NK1[5][1]:S()) then
            nk1_phone = NK1[5][1]:S()
         end
         local nk1_address_1 = NK1[4][1]:S()
         local nk1_address_2 = NK1[4][2]:S()
         local nk1_address_4 = NK1[4][3]:S()
         local nk1_address_5 = NK1[4][4]:S()
         local nk1_eircode = NK1[4][5]:S()
         --
         local NKUserQueryResultSet = conn:execute{sql=
            "select count(id), id from users " ..
            "where parent = " .. id .. 
            " and relationship = " .. conn:quote(nk1_rel_id[1]:S()), live=true}

         -- Update existing or create new related person record in user table
         if NKUserQueryResultSet[1]['count(id)']:S() ~= '0' then

            nk1_userID = NKUserQueryResultSet[1]['id']:S()

            iguana.logDebug("Updating related person details from NK1 segment into users table for patient: " .. id)

            local sql ="update users " ..
            "set "..
            "mobile = trim(replace(" .. conn:quote(nk1_mobile_format) .. ", ' ', '')), "..
            "home_phone = " .. conn:quote(nk1_phone) .. ", "..
            "Address_1 = " .. conn:quote(nk1_address_1) .. ", "..
            "Address_2 = " .. conn:quote(nk1_address_2) .. ", "..
            "Home_Address = " .. conn:quote(nk1_eircode) .. ", "..
            "Address_4 = " .. conn:quote(nk1_address_4) .. ", "..
            "Address_5 = " .. conn:quote(nk1_address_5) .. 
            " where id = ".. nk1_userID

            conn:execute{sql=sql, live=false}

            iguana.logDebug(sql)

         elseif NKUserQueryResultSet[1]['count(id)']:S() == '0' then

            iguana.logDebug("Adding related person details from NK1 segment into users table for patient: " .. id)

            local sql = 
            "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
            "Address_1, Address_2, Home_Address, Address_4, Address_5, created) " ..
            "values ( " ..
            conn:quote(nk1_parent)..",".. 
            conn:quote(nk1_rel_id[1]:S()) .. "," ..
            "trim(replace(" .. conn:quote(nk1_mobile_format) .. ", ' ', '')), "..
            conn:quote(nk1_phone)..","..
            -- conn:quote(email)..","..
            conn:quote(nk1_firstname)..","..
            conn:quote(nk1_surname)..","..
            conn:quote(nk1_address_1)..","..
            conn:quote(nk1_address_2)..","..
            conn:quote(nk1_eircode)..","..
            conn:quote(nk1_address_4)..","..
            conn:quote(nk1_address_5)..","..
            "now()"..
            ")"

            conn:execute{sql=sql, live=false}
            iguana.logDebug(sql)

            local nk1_user_resultSet = conn:execute{sql="select last_insert_id() as id", live=true}
            nk1_userID = nk1_user_resultSet[1].id:S()

            iguana.logDebug("Added new related person record into users table, with id [" ..nk1_userID .. 
               "], for patient ID [" ..nk1_parent .. "]")


         end -- End users record added/updated in users table

         -- If related person is NOK, 
         -- then update existing, or create new, user_additional_info record
         -- and set act_as_nok to '1' (true)

         if (NK1[7][1]:S() == 'NOK') then
            local NKUserAddInfoResultSet = conn:execute{sql=
               "select count(id), id from users_additional_info " ..
               "where user_id = " .. nk1_userID, live=true}

            -- If users_additional_info record exists, update it
            if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
               local sql = "update users_additional_info " ..
               "set act_as_nok = '1' " .. 
               "where id = ".. nk1_userID

               conn:execute{sql=sql, live=false}
               iguana.logDebug(sql)

               iguana.logDebug("Added NOK category status to users_additional_info for user ID [" .. nk1_userID .. "]")

               -- else if users_additional_info record does not exist, then create it
            elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

               local sql = "insert into users_additional_info (user_id, act_as_nok) " ..
               "values (" ..conn:quote(nk1_userID) .. ", '1')"

               conn:execute{sql=sql, live=false}
               iguana.logDebug(sql)

               local nk1_act_as_nok_result = conn:execute{sql="select last_insert_id() as id", live=true}

               iguana.logDebug("Created user_additional_info record with id: [" ..nk1_act_as_nok_result[1].id:S().."], for userID: [" ..nk1_userID.."]")

            end            
         end -- End NK1[7][1] == NOK
      end
   end -- End NK1 processing
   
end

function createAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)
 
   --extract the res date and time
      --local mrn = SCH[1][1]:S()
      local opdAppointment = dateparse.parse(SCH[11][4]:D())
      local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

      local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
      local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
      local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
      local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
      --local DXCId = SCH[1][1]:S()
      local DXCId = SCH[2][1]:S()
      --local apptType = PV2[12]:S()
      local apptType = SCH[8][1]:S()
      local apptPriority = PV2[25]:S()
      local apptCategory = PV1[18]:S()
      local appReason = 1
      local DXCReason = SCH[7][1]:S()
      local DXCAIPID = AIP[3][1]:S()
      local DXCSpeciality = AIP[4][1]:S()
      local refID = SCH[4][1]:S()
      local DXCSession = SCH[5][1]:S()
  
   -- 
   --Check if an appointment already exists for this patient
      local ResId = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " 
      .. conn:quote(opdAppointmentString) .. " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

      if ResId[1]['count(id)']:S() == '0'
      then
   
           --insert the new appointment
            conn:execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, "..
            "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by,".. 
            "is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
            "values ("..
            facilityID..","..
            id..","..
            "'f',0"..","..
            conn:quote(DXCId).."," ..
            conn:quote(opdAppointmentString)..","..
            conn:quote(opdAppointmentTimeString)..","..
            "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval 30 minute, 1, 5)"..","..
        --    conn:quote(opdAppointmentEndTimeString)..","..
            conn:quote(clinCode)..","..
            reasonID..","..
            conn:quote(refID)..","..
            "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
            ")"
            , live=false}
      
            local result = conn:execute{sql="select last_insert_id() as id", live=true}
            
            --Add A user audit record for the app creation
            conn:execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.created',"..
                 result[1].id:S()..","..id..",null)"
            , live=false}      
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'REASON'"..","..
            conn:quote(DXCReason)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'AIPID'"..","..
            conn:quote(DXCAIPID)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'DXCSPEC'"..","..
            conn:quote(DXCSpeciality)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'SESSION'"..","..
            conn:quote(DXCSession)..
             ")"
            , live=false}
      
      else
            conn:execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..", google_cal_id = " ..conn:quote(clinCode).. 
            ", hl7_id = "..conn:quote(refID)..", source = 'h' where id = "..ResId[1].id, live=false}   
      
            -- check if the reservation_Ipm records exist - if not create them
            local IPMResId = conn:execute{sql="select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
            --
            if IPMResId[1]['count(res_id)']:S() == '0' then
         
               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'TYPE'"..","..
               conn:quote(apptType)..
               ")"
               , live=false}
      
               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'PRIORITY'"..","..
               conn:quote(apptPriority)..
               ")"
               , live=false}
      
               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'CATEGORY'"..","..
               conn:quote(apptCategory)..
               ")"
               , live=false}

               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'REASON'"..","..
               conn:quote(DXCReason)..
               ")"
               , live=false}

               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'AIPID'"..","..
               conn:quote(DXCAIPID)..
               ")"
               , live=false}

               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'DXCSPEC'"..","..
               conn:quote(DXCSpeciality)..
               ")"
               , live=false}
         
               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               ResId[1].id:S()..","..
               "'SESSION'"..","..
               conn:quote(DXCSession)..
               ")"
               , live=false}
            --
            end
            --
      end
end

function updateAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   --local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local refID = SCH[4][1]:S()
   local DXCId = SCH[2][1]:S()
   local apptType = SCH[8][1]:S()
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local DXCSession = SCH[5][1]:S()
   
   --local ResId = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}
   local ResId = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " .. conn:quote(opdAppointmentString) ..
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then
      
      --check if an existing appointment exists
      local ResIdUpd = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
         conn:execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
           , live=false}
      end
      
      --insert the new appointment
      conn:execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, reservation_time,"..
           "reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by, is_attended,booking_user_id, source, print,stats, "..
           "quality,capacity_sync, is_urgent) " .. 
            "values ("..
            facilityID..","..
            id..","..
            "'f',0"..","..
            conn:quote(DXCId).."," ..
            conn:quote(opdAppointmentString)..","..
            conn:quote(opdAppointmentTimeString)..","..
            "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval 30 minute, 1, 5)"..","..
            --conn:quote(opdAppointmentEndTimeString)..","..
            conn:quote(clinCode)..","..
            reasonID..","..
            conn:quote(refID)..","..
            "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
            ")"
            , live=false}  
      
            local result = conn:execute{sql="select last_insert_id() as id", live=true}
      
            --Add A user audit record for the app creation
            conn:execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.created',"..
                 result[1].id:S()..","..id..",null)"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
             ")"
            , live=false}
      
            conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'REASON'"..","..
               conn:quote(DXCReason)..
               ")"
               , live=false}

               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'AIPID'"..","..
               conn:quote(DXCAIPID)..
               ")"
               , live=false}

               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'DXCSPEC'"..","..
               conn:quote(DXCSpeciality)..
               ")"
               , live=false}
         
               conn:execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'SESSION'"..","..
               conn:quote(DXCSession)..
               ")"
               , live=false}
      --
    else 
      conn:execute{sql="update reservation set " .. 
            "reservation_date = " .. conn:quote(opdAppointmentString)..","..
            "reservation_end_time = substring(convert("..conn:quote(opdAppointmentTimeString)..", time) + interval 30 minute, 1, 5)"..","..
            --"reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
            "google_cal_id = " .. conn:quote(clinCode)..","..
            "hl7_id = " .. conn:quote(refID)..","..
            "modified = now(), modified_source = 'h'"..","..
            "reservation_time = " .. conn:quote(opdAppointmentEndTimeString)..
            " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
            "user_id = " .. id
           , live=false}
      
            --Add A user audit record for the app creation
            conn:execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.rescheduled',"..
                 conn:quote(ResId[1]['id']:S())..","..id..",null)"
            , live=false}      
      
    end
    --
end

function updatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   local appReason = 1
   
   local ResId = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0'
      then
      --
      local ResId2 = conn:execute{sql="select count(id), id from patient_flow where stage = 'CHECK-IN' and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}
      
      if ResId2[1]['count(id)']:S() == '0' then
         --insert the new patient flow
         conn:execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..
            "9919,'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}
       end
    end
    --
end

function cancelAppointment(id, facilityID, SCH, reasonID)
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local ResIdUpd = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      conn:execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where location_pref = " .. conn:quote(DXCId).." and " ..
         "user_id = " .. id
         , live=false}
      
      --Add A user audit record for the app creation
      conn:execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.cancelled',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end
--
function dnaAppointment(id, facilityID, SCH, reasonID)
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = conn:execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      conn:execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      conn:execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

function getPatientReferral(id,facilityID,RF1)
   
   local refID = RF1[6][1]:S()
   
   local referralCount = conn:execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. id.." and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return referralCount[1].id:S()
   end
   
end

function createPatientReferral(id,facilityID, clinCode, RF1, PRD)
   
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = 410
   local refID = RF1[6][1]:S()
   local refExpiry = RF1[8]:S()
   local waitingStatus = 'triage'
   local refDateReferred = ''
   
   if not isEmpty(RF1[9]:S()) then
      refDateReferred = RF1[9]:parseDate()
      trace(refDateReferred)
   end
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   local refPriorityName = RF1[2][2]:S()
   
   local refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
   --
   local referralPriority = conn:execute{sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   --
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   --
   local referralSpeciality = conn:execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   --
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   --
   local refCondition = conn:execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end
   --
   local referralCount = conn:execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   trace(referralCount[1].id:S())

   if referralCount[1]['count(id)']:S() == '0' then
      
      sql="insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, referral_source_id,"..
         "referral_source_name, status, `condition`, control_id, comments, created, created_by) " ..
         "values ("..
         facilityID..","..
         id..","..
         conn:quote(refDateReferred)..","..
         facilityCategory..","..
         facilitySpeciality..","..
         FacilitySourceId..","..
         conn:quote(refSourceName)..","..
         conn:quote(waitingStatus)..","..
         conn:quote(facilityConditionTag)..","..
         conn:quote(refID)..","..
         "'HL7 Referral'"..","..
         "now()"..","..
         "22"..
         ")"

      conn:execute{sql=sql, live=false}
      iguana.logDebug("createPatientReferral: " .. sql)
      
      local result = conn:execute{sql="select last_insert_id() as id", live=true}
      trace(result) 
      sql = "insert into ewaiting_referral_condition_tags values (" .. result[1].id .."," .. facilityConditionTag .. ")"
      iguana.logDebug("createPatientReferral:" ..sql)
      conn:execute{sql=sql, live=false}
   --  
   end
end
--
function updatePatientReferral(id,facilityID, clinCode, RF1)
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = 410
   local refID = RF1[6][1]:S()
   local refExpiry = RF1[8]:S()
     
   local waitingStatus = 'triage'
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   local refPriorityName = RF1[2][2]:S()
   --
   local referralPriority = conn:execute{sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --date referred
   local refDateReferred = RF1[9]:parseDate()
   
   --
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   --
   local referralSpeciality = conn:execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   --
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   --
   local refCondition = conn:execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end
   --
   local referralCount = conn:execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() ~= '0' then

      conn:execute{sql="update ewaiting_referrals set "..
         "facility_id = "..facilityID..","..
         "user_Id = "..id..","..
         "category_id = "..facilityCategory..","..
         "speciality_id = "..facilitySpeciality..","..
         "referral_source_id = "..FacilitySourceId..","..
         "date_referred = "..conn:quote(refDateReferred)..","..
         "modified = now(), modified_by = 22 "..
         "where id = ".. referralCount[1]['id']:S(), live=false}
      --  
   end
end
--
function createGPDetails(patientID, PD1, faciliityID)
   --
   --extract the relevant GP details
   local gpFirstname = ''
   local gpOrgID = ''
   local gpID = ''
   local gpPractice = ''
   local gpFullName = ''
   
   gpFirstname = PD1[4][3]:S()
   gpSurname = PD1[4][2]:S()
   gpPractice = PD1[3][1]:S()
   gpID = PD1[4][1]:S()
   gpFullName = gpFirstname.." "..gpSurname
   --
   local gpCount = conn:execute{sql="select count(id), id from gp_details"..
      " where medical_council_number = " .. conn:quote(gpID) .. 
      " and firstname = " .. conn:quote(gpFirstname) ..
      " and surname = " .. conn:quote(gpSurname), live=true}
   
   if gpCount[1]['count(id)']:S() == '0' then
      --create the new GP
      conn:execute{sql="insert into gp_details (id,firstname,surname,address_1,medical_council_number)"..
         " values (null, " .. conn:quote(gpFirstname)..","..conn:quote(gpSurname)..","..
         conn:quote(gpPractice)..","..conn:quote(gpID)..")", live=false}

      local newGPID1 = conn:execute{sql="select last_insert_id() as id", live=true}
      local newGPID = newGPID1[1].id:S()
      --
      --now insert it into the facility_gp table
      conn:execute{sql="insert into facility_gp (id,facility_id,gp_details,hospital_code,gmc_code,dh_code,m_number)"..
         " values (null,"..conn:quote(faciliityID)..","..conn:quote(newGPID)..",0,0,0,0)", live=false}

      conn:commit{live=true}
      --
      --now update the patient details
      conn:execute{sql="update users set gp_details = "..conn:quote(newGPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
   end
   --
end

function getFacilityIDClinic(clinCode, hospitalCode)
   
   local clinicCount = conn:execute{sql="select count(id), facility_id from facility_clinic_codes"..
      " where clinic_code = " .. conn:quote(clinCode) .. 
      " and hospital_code = " .. conn:quote(hospitalCode), live=true}
   
   if clinicCount[1]['count(id)']:S() == '0' then
      return '0'
   else
      return clinicCount[1].facility_id:S()
   end
   
end

function createClinicCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq, clinicCode, clinicDesc)

   -- first check if we have a template clinic in the env for ipM
   local paramCount = conn:execute{sql="select count(id), param_value from application_parameter"..
      " where param_name = 'ipm.parent'", live=true}
   
   if paramCount[1]['count(id)']:S() == '0' then
      error('Application parameter for ipm.parent is not setup. Please add before proceeding.')
   else
      -- find a relevant template clinic to create teh details from
      local templateCount = conn:execute{sql="select count(id), id from facilities where parent_id = ".. conn:quote(paramCount[1]['param_value']:S())..
      " and duration = " .. conn:quote(sessionDur), live=true}
      
      if templateCount[1]['count(id)']:S() == '0' then
         error('No template found matching the clinic duration')
      else
         local templateFID = templateCount[1]['id']:S()
         --
         local newFID = createNewFacility(templateFID, clinicDesc)
         --
         createClinicCode(newFID, clinicCode)
         --
         createNewAllowableSlots(newFID, templateFID)
         --
         updateCapacity(newFID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
         --
      end
   end
end

function updateCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
   --
   -- Assumption here is that the entries are already set up in aloable_slots
   -- This is JUST an update to existing capacity
   
   local start_time1 = sessionStart:sub(1,2)
   local start_time2 = sessionStart:sub(3,4)
   local start_time = start_time1..':'..start_time2
   --
   local end_time1 = sessionEnd:sub(1,2)
   local end_time2 = sessionEnd:sub(3,4)
   local end_time = end_time1..':'..end_time2
   
   local ipm_day1 = days:split(',')[1]
   local ipm_day2 = days:split(',')[2]
   local ipm_day3 = days:split(',')[3]
   local ipm_day4 = days:split(',')[4]
   local ipm_day5 = days:split(',')[5]
   local ipm_day6 = days:split(',')[6]
   local ipm_day7 = days:split(',')[7]
   
   --********NEED to cater for change in durations********---
   local checkDur = checkClinicDur(facilityID, sessionDur)
   
   --********NEED to add in audit trail of changes********---
      
   --if the durations match then it's a straighforward update
   if checkDur == '1' then
      --reset Calendar Days
      conn:execute{sql="update calendar_days " .. 
         " set monday = 'N', tuesday = 'N', wednesday = 'N'," ..
         " thursday = 'N', friday = 'N', saturday = 'N'," ..
         " sunday = 'N'"..
         " where facility_id = " .. facilityID, live=false}
      
      --reset capacity slots back to 0
      conn:execute{sql="update allowable_slots set allowable = 0" ..
         " where facility_id = " .. facilityID
         , live=false}

      conn:commit{live=true} -- need to commit to prevent conflict with update
   else
      --we will need to create new allowable slots
   end
   --
   local day1 = setAllowableDays(facilityID, ipm_day1)
   local day2 = setAllowableDays(facilityID, ipm_day2)
   local day3 = setAllowableDays(facilityID, ipm_day3)
   local day4 = setAllowableDays(facilityID, ipm_day4)
   local day5 = setAllowableDays(facilityID, ipm_day5)
   local day6 = setAllowableDays(facilityID, ipm_day6)
   local day7 = setAllowableDays(facilityID, ipm_day7)
    
   --Update the relevant sessions
   if day1 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day1)
   end
   --
   if day2 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day2)
   end
   --
   if day3 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day3)
   end
   --
   if day4 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day4)
   end
   --
   if day5 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day5)
   end
   --
   if day6 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day6)
   end
   --
   if day7 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day7)
   end
end
--
function setAllowableDays(facilityID, day)
   --
   if day == 'MON' then
      conn:execute{sql="update calendar_days set monday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      
      return 'Mon'
      --
   elseif day == 'TUE' then
      conn:execute{sql="update calendar_days set tuesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Tue'
      --
   elseif day == 'WED' then
      conn:execute{sql="update calendar_days set wednesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Wed'
      --
   elseif day == 'THU' then
      conn:execute{sql="update calendar_days set thursday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Thu'
      --
   elseif day == 'FRI' then
      conn:execute{sql="update calendar_days set friday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Fri'
      --
   elseif day == 'SAT' then
      conn:execute{sql="update calendar_days set saturday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sat'
      --
   elseif day == 'SUN' then
      conn:execute{sql="update calendar_days set sunday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sun'
      --
   else
      return '0'
   end
end
--
function updateAlloableSlots(facilityID, start_time, end_time, day)
   --
   --readjust end_time so that it is not included in the update
   conn:execute{sql="update allowable_slots set allowable = 1" ..
      " where facility_id = " .. facilityID ..
      " and (time >= " .. conn:quote(start_time) .. 
      " and time < " .. conn:quote(end_time) .. ")" ..
      " and date = " .. conn:quote(day), live=false}
   --
end
--
function checkClinicDur(facilityID, sessionDur)
   --
   local clinicDur = conn:execute{sql="select count(id), duration from facilities"..
      " where id = " ..conn:quote(facilityID), live=true}
   
   if clinicDur[1]['duration']:S() == sessionDur then
      return '1'
   else
      return clinicDur[1].duration:S()
   end

end

function createNewFacility(templateFID, clinicDesc)
        
         --**************
         --- WHERE DO I GET THE PARENT ID from
         --**************
         
         -- now create the clinic in the facilities table
         local createFID = conn:execute{sql="insert into facilities (id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,firstname,"..
            "surname,short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,fkey,hl7_endpoint,"..
            "search_ip_restricted) "..
         "select null as id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,".. conn:quote(clinicDesc).." as firstname,"..
            "surname,".. conn:quote(clinicDesc).. " as short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,null as fkey,hl7_endpoint,"..
            "search_ip_restricted from facilities where id = " .. templateFID, live=true}
         
         local newFID1 = conn:execute{sql="select last_insert_id() as id", live=true}
         local newFID = newFID1:S()
         --
         --create the calendar days entries
         local createFIDDays = conn:execute{sql="insert into calendar_days (id,timestamp,facility_id,monday,"..
            "tuesday,wednesday,thursday,friday,saturday,sunday) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,monday,tuesday,wednesday,thursday,friday,saturday,"..
            "sunday from calendar_days where facility_id = ".. templateFID, live=true}
         
         --create the calendar actions entries
         local createFIDActions = conn:execute{sql="insert into calendar_actions " ..
            "(id,timestamp,facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule from calendar_actions where facility_id = ".. templateFID, live=true}
         
         --create facility_config
         local createFIDConfig = conn:execute{sql="insert into facility_config " ..
            "(id,timestamp,facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen) "..
        "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen from facility_config where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
   return newFID
   
end

function createNewAllowableSlots(newFID, templateFID)
   
   --create the calendar days entries
   local createAllowableSlots = conn:execute{sql="insert into allowable_slots (id,timestamp,facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id) "..
      "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id from allowable_slots where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
end

function createClinicCode(newFID, sessionCode)
   
   -------***************NEEDS MORE WORK !!!!!!!
   
   --create the facility_clinic_code record
   local createClinicCodes = conn:execute{sql="insert into facility_clinic_codes (id,hospital_group_id,facility_id,clinic_code,"..
      "hospital_code,reason_id,active,created_at,updated_at,waiting_status,condition_id,speciality_id,category_id,"..
      "source_id,opd_video) values (null, 14, ".. conn:quote(newFID)..","..conn:quote(sessionCode)..",'NMH',null,0,now(), null,"..
      "'pre_triage',0,0,0,0,0)", live=true}
   
   conn:commit{live=true}
   
end

function createSlotDateRange(facilityID, startDate, endDate)
   
   --create a new record for allowable_slots_date_ranges
   local createSlotDateRanges = conn:execute{sql="insert into allowable_slots_date_ranges (id,facility_id,"..
      "start_date,end_date,active,created_at,updated_at) values ".. 
      "(null,"..conn:quote(facilityID), ","..conn:quote(startDate)..","..conn:quote(endDate)..","..
      "1, now(), now())", live=true}
   
   conn:commit{live=true}
   
   local result = conn:execute{sql="select last_insert_id() as id", live=true}
   --
   return result
   --
end
--
function updateWaitList(patientID,waitListID,referralID, facilityID)
   
   local referralCount = conn:execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(referralID), live=true}
   
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      local createSlotDateRanges = conn:execute{sql="update ewaiting_referrals set covid_19_id = "..conn:quote(waitListID)..
      " where id = "..conn:quote(referralCount[1]['id']:S()), live=false}
   
      conn:commit{live=true}
      
   end
end


function isEmpty(s)
    return s == nil or s == '' or type(s) == 'userdata'
end
