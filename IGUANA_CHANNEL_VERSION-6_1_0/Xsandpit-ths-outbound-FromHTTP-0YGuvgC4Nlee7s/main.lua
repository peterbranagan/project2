--set the database path
--_G.DbFile = '/etc/iNTERFACEWARE/test_au_db_credentials'
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

sqDB = require('sqDatabase')
map = require('mappings')

_G.msgCtrlID = 0

---------------------------------------------------------
function main(Data)
   
   --[[ TESTING
   local ss = sqUtils.getQueuedMessageCount()
   ss = sqUtils.getErrorMessageCount()
   ]]
   
  
   --[[ TESTING
   local errorMsgs = sqUtils.getJsonMappedValue(sqCfg.extra_params,'errorMsgs')
   trace(s3)
   local s4 = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(errorMsgs),'2')
   trace(s4)
   ]]
  
   iguana.stopOnError(false)
   
   sqUtils.printLog("JSON Received: \n"..Data)

   --Check that we have a connection an accesible
   --If it fails then try to reconnect
   if not conn or not conn:check() then
      conn:close()
      conn = sqCfg.dbConnect()
   end
   
   conn:begin{}
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}

   -- Parse the HTTP request
   local Request = net.http.parseRequest{data=Data}

   -- Only accept POST requests
   if Request.method ~= 'POST' then
      net.http.respond{body='{"success": false, "message": "Method not allowed"}', code=405}
      return
   end
   
   -- Respond to the client immediately
   net.http.respond{body='{"success": true}', code=200}

   -- Parse the incoming JSON
   local Payload = json.parse{data=net.http.parseRequest{data=Data}.body}
   
   -- Convert each appointment from JSON into a HL7 message
   for Key,Value in pairs(Payload)
   do
         
      _G.msgCtrlID = util.guid(128)
      
      --Process patient demographic updates
      if Value.type ~= 'appointment' then
         
         if Value.type == 'document' then

            ---- need to loop through files and generate an MDM for each file


            --testing MDM againt patient exports until MDM export in place
            vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile") 
            Message = hl7.message{vmd=vmdFile, name='MDMT04'}
            mappings.mdm.buildT04(Message, Value)       
            sqUtils.printLog("MDM^T04 generated: \n"..Message:S())
            queue.push{data=Message:S()} 
            ------------------------------------------------------------------

         else

            local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile") 
            local Message = hl7.message{vmd=vmdFile, name='ADTA31'}
            sqUtils.printLog("Retrieving patient demographics..")
            map.buildA31(Message, Value)
            sqUtils.printLog("A31 generated: \n"..Message:S())
            --queue.push{data=Message:S()}      


         end
         

      else

         --Process appointments
         if Value.appointment ~= json.NULL then

            local proceed = 'Yes'

            trace(Value.appointment.id) --reservation id

            --if the appointment type is a planned appointment then we don't send
            local plannedAppCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'plannedAppCode')
            if tostring(Value.appointment.reason_id) == plannedAppCode then
               proceed = 'No'
            end

            local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile")
            local Message = hl7.message{vmd=vmdFile, name='SIUS12'}

            local CheckIn = Value.patient_flow.check_in_time

            sqUtils.printLog("Value.appointment.source = "..tostring(Value.appointment.source))
            sqUtils.printLog("Value.appointment.modified_source = "..tostring(Value.appointment.modified_source))
            sqUtils.printLog("CheckIn = "..tostring(CheckIn))
            sqUtils.printLog("Value.patient_flow.current_stage = "..tostring(Value.patient_flow.current_stage))
            sqUtils.printLog("Value.appointment.is_attended = "..tostring(Value.appointment.is_attended))
            sqUtils.printLog("Proceed = "..proceed)

            if Value.appointment.source == 'h' then
               proceed = 'No'
               trace(Value.appointment.modified_source)
               if Value.appointment.modified_source ~= json.NULL and Value.appointment.modified_source ~= 'h' then
                  proceed = 'Yes'
               end
            else
               proceed = 'Yes'
            end

            if proceed == 'Yes' then

               if Value.appointment.source == 'h' then
                  if CheckIn == 'false' then
                     if Value.appointment.modified_source ~= 'h' then
                        proceed = 'Yes'
                     else
                        proceed = 'No'
                     end

                     --[[
                     if Value.appointment.modified_source == 'b' then
                     proceed = 'Yes'
                  elseif Value.appointment.modified_source == 'c' then
                     proceed = 'Yes'
                  elseif Value.appointment.modified_source == 'o' then      
                     proceed = 'Yes'
                  elseif Value.appointment.modified_source == 'p' then      
                     proceed = 'Yes'
                  elseif Value.appointment.modified_source == 'PR' then      
                     proceed = 'Yes'
                  else
                     proceed = 'No'
                  end
                     ]]

                  end
               else
                  proceed = 'Yes'
               end
            end


            --PB 03.11.23: logic taken from CHO8
            if Value.appointment.modified_source ~= 'h' and Value.appointment.is_attended ==  'no' and 
               Value.patient_flow.current_stage == 'DNA' then
               proceed = 'Yes'   -- This will be different from NMH live behaviour 04/05/2023 jlui2
               iguana.logDebug("proceed: Yes, " .. 
                  "Value.patient_flow.current_stage: " ..  Value.patient_flow.current_stage)
            end

            if Value.appointment.modified_source == 'h' then
               if CheckIn == 'false' then
                  proceed = 'No'
               end
            end

            local CheckIn = Value.patient_flow.check_in_time
            local CheckInStage = Value.patient_flow.current_stage

            if proceed == 'No' then
               sqUtils.printLog("Json payload ignored!")
            end

            if proceed == 'Yes' then

               sqUtils.printLog("Processing json payload...")

               Message = map.mapMSH(Message, Value.appointment, Value.patient_flow)

               sqUtils.printLog("Process message type: "..Message.MSH[9]:S())

               if ( CheckIn == 'false' and (Message.MSH[9][2]:S() ~= 'A03') ) or Message.MSH[9][2]:S() == 'S26' then     
                  Message = map.mapSCH(Message, Value.appointment, Value.referral)
               else
                  Message = map.mapEVN(Message, Value.appointment, Value.patient_flow)
               end

               Message = map.mapPID(Message, Value.patient, Value.patient_flow)
               Message = map.mapPV1(Message, Value.appointment, Value.patient_flow, Value.referral)
               Message = map.mapPV2(Message, Value.appointment, Value.patient_flow, Value.referral)

               if CheckIn == 'false' and (Message.MSH[9][2]:S() ~= 'A03') then
                  Message = map.mapAIL(Message, Value.appointment, Value.patient_flow)
                  Message = map.mapAIP(Message, Value.appointment, Value.patient_flow)
               end 

               sqUtils.printLog("Sending to IPM: \n"..Message:S())

               -- Push each appointment to the queue
               queue.push{data=Message:S()}
               conn:commit{live=true}

            end  

         end

      end


   end

   -- Respond to the client
   --net.http.respond{body='{"success": true}', code=200}

end


-------------------------------------------------------------------------
--PB 20.10.23
function getScheduleIdentifier(appId)
   
   local scheduleIdentifierMap = sqUtils.getJsonMappedValue(sqCfg.extra_params,'scheduleIdentifierMap')
   return sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(scheduleIdentifierMap),'default')

end

--------------------------------------------------------------


--------------------------------------------------------------

