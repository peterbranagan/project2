--set the database path
--_G.DbFile = '/etc/iNTERFACEWARE/test_au_db_credentials'
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

function main(Data)
      
   iguana.stopOnError(false)
   
   local Msg = hl7.parse{vmd='ack.vmd', data=Data}
   local Ack = hl7.message{vmd='ack.vmd', name='Ack'}
   Ack.MSH[3][1] = "SWIFTQ"
   Ack.MSH[4][1] = "SWIFTQ"
   Ack.MSH[5][1] = "IMS_PAS"
   Ack.MSH[6][1] = "ERHA_NTST"
   Ack.MSH[7][1] = os.date('%Y%m%d%H%M%S')  
   Ack.MSH[9][1] = 'ACK'
   Ack.MSH[10] = Msg.MSH[10]
   Ack.MSH[11][1] = 'P'
   Ack.MSH[12][1] = '2.4'
   
   --connect to the iguana table to process the ac_msg flag
   local lconn = db.connect{api=db.MY_SQL, name=sqConfig.dbCredentials.db_name, 
         user=sqConfig.dbCredentials.db_user, 
         password=sqConfig.dbCredentials.db_pass, live=true}
   
   --Check for the allowed message types here, before vmd processing
   --so as not to throw error if message structure not defined in the VMD file
   local hl7Str = sqUtils.split(Data,'|')
   local msgType = string.sub(hl7Str[9],1,7)
   
   --Store the message control ID
   _G.msgCtrlID = hl7Str[10]
   
   local reject_msg = not sqUtils.messageTypeAllowed(hl7Str, msgType)
     
   if reject_msg then
      
      --send an eror ACK back rejecting the message
      sqUtils.printLog("LLP: Returning NACK to sender, "..msgType.." message not allowed")
      Ack.MSA[1] = 'AA'
      Ack.MSA[2] = Msg.MSH[10]
      Ack.MSA[3] = "Incoming message type not allowed"
      ack.send(Ack:S())
      resetAckMsg(lconn)

   else

      --Default to AA until told otherwiase
      Ack.MSA[1] = 'AA'
      Ack.MSA[2] = Msg.MSH[10]
      Ack.MSA[3] = "OK"

      --if customACK is set to 0 in extra_params then use fast ack
      local customACK = sqUtils.getMappedValue(sqCfg.extra_params,'customACK')
      
      --if processCA is set to 1 in extra_params then use CA warning ACKS
      local processCA = sqUtils.getMappedValue(sqCfg.extra_params,'processCA')
      
      if customACK == '0' then 
         ack.send(Ack:S())
         resetAckMsg(lconn)
         sqUtils.printLog("closing lconn ...")
         lconn:close()
         return
      end
      
      -- Parse the incoming message
      local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile")
      local MsgIn = hl7.parse{vmd=vmdFile, data=Data} 

      sqUtils.printLog("LLP: Waiting for translator outcome..")

      local errMsg = ""

      --wait for translator script to complete before processing the ACK
      local r = 1
      local errMsg = ""
      repeat 

         local res = lconn:execute{sql="SELECT ack_msg FROM iguana_config where channel = "..
            lconn:quote(sqConfig.ie_channel).." and ie_instance = "..
            lconn:quote(sqConfig.ie_instance), live=true} 

         errMsg = res[1].ack_msg:S()

         if errMsg == nil or errMsg == "" then 
            sqUtils.printLog("still waiting on translator ack response....")
            util.sleep(3000)
            r = r + 1
         end
         if r > 10 then
            errMsg = "Tmeout waiting for translator response"
         end
      until errMsg ~= ""

      sqUtils.printLog("LLP: ACK response received from translator, Value: "..errMsg)

      if errMsg == "OK" then
         sqUtils.printLog("Sending ACK :"..Ack:S())
         ack.send(Ack:S())
      elseif string.sub(errMsg, 1, 3) == "CA:" and processCA == '1' then
         Ack.MSA[1] = 'CA'
         Ack.MSA[3] = string.sub(errMsg, 4, 999)
         sqUtils.printLog("Sending ACK :"..Ack:S())
         ack.send(Ack:S())         
      else
         --send an eror ACK back
         Ack.MSA[1] = 'AE'
         Ack.MSA[3] = errMsg
         ack.send(Ack:S())
         
         --determine if email to be sent
         local sendMail = sqUtils.getMappedValue(sqCfg.ack_email,'sendEmail')
         trace(sendMail)
         if sendMail == '1' then
            local subject = sqUtils.getMappedValue(sqCfg.ack_email,'subject')
            local recipients = sqUtils.getMappedValue(sqCfg.ack_email,'sendTo')
            sqUtils.sendMail(subject, errMsg, recipients)
         end
         
      end
      
      resetAckMsg(lconn)
      
   end

   sqUtils.printLog("closing lconn ...")
   lconn:close()

end

---------------------------------------------------------------------------

---------------------------------------------------------------------------
function resetAckMsg(lconn)
   sqUtils.printLog("LLP: resetting ack_msg..")
   lconn:execute {sql="update iguana_config set ack_msg = '' where channel = "..
         lconn:quote(sqConfig.ie_channel).." and ie_instance = "..
         lconn:quote(sqConfig.ie_instance), live=false}
end