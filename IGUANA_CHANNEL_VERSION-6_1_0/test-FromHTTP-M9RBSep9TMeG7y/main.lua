
--set the database path
_G.DbFile = '/etc/iNTERFACEWARE/test_au_db_credentials'
--_G.DbFile2 = os.getenv("SQDB_FILEPATH")

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

--require 'tas_config'

sqDB = require('sqDatabase')
map = require('mappings')

---------------------------------------------------------
function main(Data)
trace(_G.DbFile)
   iguana.stopOnError(false)

   sqUtils.printLog("JSON Received: \n"..Data)

   --Check that we have a connection an accesible
   --If it fails then try to reconnect
   if not conn or not conn:check() then
      conn:close()
      conn = sqCfg.dbConnect()
   end
   
   conn:begin{}
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}

   -- Parse the HTTP request
   local Request = net.http.parseRequest{data=Data}

   -- Only accept POST requests
   if Request.method ~= 'POST' then
      net.http.respond{body='{"success": false, "message": "Method not allowed"}', code=405}
      return
   end

   -- Parse the incoming JSON
   local Payload = json.parse{data=net.http.parseRequest{data=Data}.body}
  

   -- Convert each appointment from JSON into a HL7 message
   for Key,Value in pairs(Payload)
   do
      
      local Message = hl7.message{vmd='dxc_ipm_tas.vmd', name='SIUS12'}

      local CheckIn = Value.patient_flow.check_in_time

      local proceed = 'Yes'

      sqUtils.printLog("Value.appointment.source = "..tostring(Value.appointment.source))
      sqUtils.printLog("Value.appointment.modified_source = "..tostring(Value.appointment.modified_source))
      sqUtils.printLog("CheckIn = "..tostring(CheckIn))
      sqUtils.printLog("Value.patient_flow.current_stage = "..tostring(Value.patient_flow.current_stage))
      sqUtils.printLog("Value.appointment.is_attended = "..tostring(Value.appointment.is_attended))
      
      if proceed == 'Yes' then
         if Value.appointment.source == 'h' then
            if CheckIn == 'false' then
               if Value.appointment.modified_source == 'b' then
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'c' then
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'o' then      
                  proceed = 'Yes'
               elseif Value.appointment.modified_source == 'p' then      
                  proceed = 'Yes'
               else
                  proceed = 'No'
               end
            end
         else
            proceed = 'Yes'
         end
      end
      
      --PB 03.11.23: logic taken from CHO8
      if Value.appointment.modified_source ~= 'h' and Value.appointment.is_attended ==  'no' and 
         Value.patient_flow.current_stage == 'DNA' then
         proceed = 'Yes'   -- This will be different from NMH live behaviour 04/05/2023 jlui2
         iguana.logDebug("proceed: Yes, " .. 
            "Value.patient_flow.current_stage: " ..  Value.patient_flow.current_stage)
      end

      if Value.appointment.modified_source == 'h' then
         if CheckIn == 'false' then
            proceed = 'No'
         end
      end

      local CheckIn = Value.patient_flow.check_in_time
      local CheckInStage = Value.patient_flow.current_stage
         
      if proceed == 'No' then
         sqUtils.printLog("Json payload ignored!")
      end
      
      if proceed == 'Yes' then
         
         sqUtils.printLog("Processing json payload...")

         Message = map.mapMSH(Message, Value.appointment, Value.patient_flow)
         
         sqUtils.printLog("Process message type: "..Message.MSH[9]:S())
         
         if CheckIn == 'false' and (Message.MSH[9][2]:S() ~= 'A03') then     
            Message = map.mapSCH(Message, Value.appointment, Value.referral)
         else
            Message = map.mapEVN(Message, Value.appointment, Value.patient_flow)
         end
         
         Message = map.mapPID(Message, Value.patient, Value.patient_flow)
         Message = map.mapPV1(Message, Value.appointment, Value.patient_flow, Value.referral)
         Message = map.mapPV2(Message, Value.appointment, Value.patient_flow, Value.referral)
         
         if CheckIn == 'false' and (Message.MSH[9][2]:S() ~= 'A03') then
            Message = map.mapAIL(Message, Value.appointment, Value.patient_flow)
            Message = map.mapAIP(Message, Value.appointment, Value.patient_flow)
         end 
         
         sqUtils.printLog("Sending to IPM: \n"..Message:S())

         -- Push each appointment to the queue
         queue.push{data=Message:S()}
         conn:commit{live=true}

      end  
   end

   -- Respond to the client
   net.http.respond{body='{"success": true}', code=200}

end


-------------------------------------------------------------------------
--PB 20.10.23
function getScheduleIdentifier(appId)
   
   return sqUtils.getJsonMappedValue(ClientConfig.scheduleIdentifierMap,'default')

end

--------------------------------------------------------------


--------------------------------------------------------------

