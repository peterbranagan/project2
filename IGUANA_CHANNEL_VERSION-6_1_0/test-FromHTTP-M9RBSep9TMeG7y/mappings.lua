
sqUtils = require('sqUtils')

mappings = {

   adt = {},
   orm = {},
   siu = {}

}

----------------------------------------------------------------------------
function mappings.mapMSH(Message, Appointment, PatientFlow)
   
   Message.MSH[3][1] = 'Swiftqueue'
   Message.MSH[4][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultSendingFacility')
   Message.MSH[5][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReceivingAppCode')
   Message.MSH[6][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultSendingFacility')
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')
   Message.MSH[15] = 'NE'
   Message.MSH[16] = 'AL'

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S13'
      else
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S15'
      end
   elseif Appointment.modified_source == 'c' then
      if Appointment.status == 'active' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S13'
      else
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S15'
      end   
   else
      Message.MSH[9][1] = 'SIU'
      Message.MSH[9][2] = 'S12'
   end
   
   if PatientFlow.check_in_time ~= 'false' then
      Message.MSH[9][1] = 'ADT'
      Message.MSH[9][2] = 'A04'
   end

   if PatientFlow.complete_time ~= 'false' then
      Message.MSH[9][1] = 'ADT'
      Message.MSH[9][2] = 'A03'
   end
   
   if Appointment.is_attended == 'no' then
      if PatientFlow.current_stage == 'DNA' then
         Message.MSH[9][1] = 'SIU'
         Message.MSH[9][2] = 'S26'
      end
   end

   Message.MSH[10] = util.guid(128)

   Message.MSH[12] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'hl7Version')


   --[[ ####Removed temporarily - validate during testing ####
   -- google_cal_id used to store clin_code only
   if Message.MSH[9][2] == 'S12' then
   conn:execute{sql="update reservation set " .. 
   "google_cal_id = " .. conn:quote(Message.MSH[10]:S())..
   " where id = " .. Appointment.id
   , live=false}
end
   ]]--

   return Message

end

----------------------------------------------------------
function mappings.mapEVN(Message, Appointment, PatientFlow)

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.EVN[1] = 'S13'
      else
         Message.EVN[1] = 'S15'
      end
   else
      Message.EVN[1] = 'S12'
   end
   --
   if PatientFlow.check_in_time ~= 'false' then
      Message.EVN[1] = 'A04'
   end
   --
   if PatientFlow.complete_time ~= 'false' then
      Message.EVN[1] = 'A03'
   end
   --
   Message.EVN[2] = os.ts.date('!%Y%m%d%H%M%S')

   return Message

end

----------------------------------------------------------
function mappings.mapSCH(Message, Appointment, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   local sqApptID = ""
   local extApptID = ""

   if not sqUtils.isEmpty(Appointment.id) then
      sqApptID = Appointment.id
   end
   
   if not sqUtils.isEmpty(Appointment.location_pref) then
      extApptID = Appointment.location_pref
   end

   Message.SCH[1][1] = sqApptID
   Message.SCH[1][2] = 'SWIFTQ'

   Message.SCH[2][1] = extApptID
   Message.SCH[2][2] = 'OAM'

   Message.SCH[5][1] = getScheduleIdentifier(Appointment.id)
        
   if type(Referral) ~= 'userdata' and Referral.control_id ~= NULL then
      Message.SCH[4][1] = Referral.control_id
      Message.SCH[4][2] = 'REF'          
   end

   local appId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      appId = Appointment.parent_id
   end
   
   local res = sqDB.getAppointmentReason(appId)
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.SCH[7][1] = res[1].ipm_value:S()
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      local clinCode = sqDB.getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
      Message.SCH[7][1] = sqUtils.getJsonMappedValue2(sqCfg.appreason_map,clinCode,'default')
   end
   
   --Message.SCH[7][1] = sqDBTHS.getAppointmentReason(appId)
   Message.SCH[7][2] = 'Follow Up'
   Message.SCH[9] = Appointment.duration
   Message.SCH[10][1] = 'MIN'
   Message.SCH[11][3] = Appointment.duration
   Message.SCH[11][4] = Appointment.start
   Message.SCH[11][5] = sqUtils.addMinutesToDate(Appointment.start, Appointment.duration)
   
   
   local res = sqDB.getAppPriorityByAppId(appId)
   trace(res[1].ipm_value:S())
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.SCH[11][6] = res[1].ipm_value:S()
   else
      Message.SCH[11][6] = sqUtils.getJsonMappedValue(sqCfg.priority_map,'default')
   end
   
   
   --Message.SCH[11][6] = sqDBTHS.getPriorityCode(appId, Referral)

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = 'Rescheduled'
      else
         Message.SCH[25][1] = 'Cancelled'
      end
   else
      Message.SCH[25][1] = 'New'
   end
   
   if Message.MSH[9][2]:S() == 'S26' then
      Message.SCH[25][1] = 'DNA'
   end
   
   return Message
end

----------------------------------------------------------
function mappings.mapPID(Message, Patient, PatientFlow)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   Message.PID[1] = 1 

   local pidIndex = 1
   if not sqUtils.isEmpty(Patient.mrn) then
      Message.PID[3][pidIndex][1] = Patient.mrn
      Message.PID[3][pidIndex][5] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultIdentifierTypeCode')
      pidIndex = pidIndex + 1
   end
   --[[ requested to be removed by Ian Guthrie
   if not sqUtils.isEmpty(Patient.id) then
      Message.PID[3][pidIndex][1] = Patient.id
      Message.PID[3][pidIndex][5] = 'SQID'
   end
   ]]

   Message.PID[5][1] = Patient.surname
   Message.PID[5][2] = Patient.firstname
   Message.PID[7] = Patient.birth_year_field .. Patient.birth_month_field .. Patient.birth_date_field
   Message.PID[8] = Patient.Gender

   return Message
end  

----------------------------------------------------------
function mappings.mapPV1(Message, Appointment, PatientFlow, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   Message.PV1[1] = 1
   Message.PV1[2] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultPatientClass')

   --Message.PV1[3][1] = 'SQ-TEST1'  -- ##### Needs correct value - test only ####
   --PB 20.10.23
   Message.PV1[3][1] = sqDB.getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
   
   if not sqUtils.isEmpty(Appointment.google_cal_id) then
      Message.PV1[3][1] = Appointment.google_cal_id   
   end
 
   --populate vistit type  / appointment reason
   local clinCode = Message.PV1[3][1]:S()
   Message.PV1[4] = sqUtils.getJsonMappedValue2(sqCfg.appreason_map,clinCode,Appointment.reason_id)
   
   Message.PV1[5][1] = ''
   if (type(Referral) ~= 'userdata') and (Referral.covid_19_id ~= '') then
      Message.PV1[5][1] = Referral.covid_19_id
   end
    
   
   local docCode, docName = sqDB.getAttendingDoctor(Appointment.id, Appointment.facility_id)
   if docCode ~= '' then
      Message.PV1[7][1] = docCode
   else
      local ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,docName)
      if ret == '' then
         ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,'default')
      end
      Message.PV1[7][1] = ret
   end
   
     
   res = sqDB.getSpecialtyCode(Appointment.id, Appointment.facility_id, Appointment.reason_id)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.PV1[10] = res[1]['ipm_value']:S()    
   else
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.PV1[10] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)    
   end
   
   local fID = Appointment.facility_id  
   
   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local res = sqDB.getAppCategory(AppId)
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.PV1[18] = res[1].ipm_value:S()
   else
      Message.PV1[18] = sqUtils.getJsonMappedValue(ClientConfig.appCategoryMap,'default')
   end
   
   Message.PV1[19][1] = Appointment.location_pref
   
   -- only check and send these values when the app is complete
   if Message.MSH[9][2]:S() == 'S26' then
      
      Message.PV1[36] = '0'
   
   elseif PatientFlow.complete_time ~= 'false' then
      
      Message.PV1[14] = getAttendanceStatusCode(Appointment.custom_fields.attnd_attended_status)
      Message.PV1[36] = getOutcomeStatusCode(Appointment.custom_fields.scocm_outcome)
   
   end
   
   if PatientFlow.complete_time ~= 'false' then     
      Message.PV1[45] = PatientFlow.complete_time
   elseif PatientFlow.check_in_time ~= 'false' then
      Message.PV1[44] = PatientFlow.check_in_time
   else
      Message.PV1[44] = Appointment.start
   end

   return Message
end

-------------------------------------------------

function mappings.mapPV2(Message, Appointment, PatientFlow, Referral)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")
 
	Message.PV2[8] = Appointment.start
   Message.PV2[9] = sqUtils.addMinutesToDate(Appointment.start, Appointment.duration)
   
   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local res = sqDB.getAppType(AppId)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
      Message.PV2[12] =  res[1]['ipm_value']:S()
   else
      Message.PV2[12] = sqUtils.getJsonMappedValue(ClientConfig.appTypeMap,'default')
   end
      
   Message.PV2[24] = getAttendanceStatusCode(Appointment.custom_fields.attnd_attended_status)
   
   local res = sqDB.getAppPriorityByAppId(AppId)
   if not sqUtils.isEmpty(res[1].ipm_value:S()) then
      Message.PV2[25] = res[1].ipm_value:S()
   else
      Message.PV2[25] = sqUtils.getJsonMappedValue(sqCfg.priority_map,'default')
   end
      
   Message.PV2[27] = getOutcomeStatusCode(Appointment.custom_fields.scocm_outcome)
     
   return Message
end 

--------------------------------------------------

function mappings.mapAIP(Message, Appointment, PatientFlow)
   sqUtils.printLog("Calling "..sqUtils.getCurrentFunctionName().."...")

   -- get the details from the DB
   Message.AIP[1] = 1

   local AppId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      AppId = Appointment.parent_id
   end
   
   local docCode, docName = sqDB.getAttendingDoctor(AppId, Appointment.facility_id)
   if docCode ~= '' then
      Message.AIP[3][1] = docCode
   else
      local ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,docName)
      if ret == '' then
         ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,'default')
      end
      Message.AIP[3][1] = ret
   end
     
   local res = sqDB.getSpecialtyCode(AppId)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.AIP[4][1] = res[1]['ipm_value']:S()    
   else
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.AIP[4][1] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)    
   end

   return Message
end

--------------------------------------------------

function mappings.mapAIL(Message, Appointment, PatientFlow)

   -- get the details from the DB
   Message.AIL[1] = 1
   Message.AIL[2] = 'A'
   
   local appId = Appointment.id
   if not sqUtils.isEmpty(Appointment.parent_id) then
      appId = Appointment.parent_id
   end
   
   local clinCode = Appointment.google_cal_id
   if not sqUtils.isEmpty(Appointment.google_cal_id) then
      clinCode = sqDB.getClinicConfigMapping('hospital_code', Appointment.facility_id, Appointment.reason_id)
   end
 
   Message.AIL[3][1] = clinCode
   
   Message.AIL[4][1] = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultLocationType')
   
   local res = sqDB.getSpecialtyCode(appId, Appointment.facility_id, Appointment.reason_id)
   if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         Message.AIL[5][1] = res[1]['ipm_value']:S()    
   else
      local v = sqDB.getClinicConfigMapping('speciality_id', Appointment.facility_id, Appointment.reason_id)
      Message.AIL[5][1] = sqUtils.getJsonMappedValue(sqCfg.specialty_map,v)    
   end

   Message.AIL[12][1] = 'BOOKED'

   return Message
end 

-------------------------------------------------------------------------

function getAttendanceStatusCode(status)
   
   local res = sqUtils.getJsonMappedValue(ClientConfig.attendanceStatusCodeMap,status)
   if res == '' then
      res = sqUtils.getJsonMappedValue(ClientConfig.attendanceStatusCodeMap,'default')
   end
   
   return res
   
end

-------------------------------------------------------------------------

function getOutcomeStatusCode(status)
   
   local res = sqUtils.getJsonMappedValue(ClientConfig.outcomeStatusCodeMap,status)
   if res == '' then
      res = sqUtils.getJsonMappedValue(ClientConfig.outcomeStatusCodeMap,'default')
   end
   
   return res
   
end
   


--------------
return mappings
---------------
