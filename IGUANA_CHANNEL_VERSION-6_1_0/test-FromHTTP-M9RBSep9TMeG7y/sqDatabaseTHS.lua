--[[

Author: Peter Branagan
Date: Oct 2023

All database activity needs be executed from this modules, separated from any other scripts


]]


local sqDB = {


}

-----------------------------------------------------------------
function sqDB.execute(val)
   
   --[[
         NOTE: **** The audit table being written to this function 
                     will grow, will need a script to purge monthly ****
   ]]
   
   if _G.dbAudit == '1' then 

      --log all inserts and updates to the iguana db audit table

      local channel = iguana.channelName()
      local sql = val.sql:gsub("\\'","'")
      sql = sql:gsub("\n","")
      local msgid = _G.msgCtrlID
      local insert = val.sql:upper():find('INSERT INTO')
      if insert ~= nil then insert = true end
      local update = val.sql:upper():find('UPDATE ')
      if update ~= nil then update = true end
      local delete = val.sql:upper():find('DELETE FROM ')
      if delete ~= nil then delete = true end

      if insert or update or delete then
         
         --[[
         conn:execute{sql="insert into iguana_db_audit (channel_name, msgid, sql_str) values ("..    
            conn:quote(channel)..","..
            conn:quote(msgid)..","..
            conn:quote(sql)..")"
            , live=false}
         ]]
         
         local dt = os.date("%Y-%m-%d %H:%M:%S")
         local line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
         
         if not iguana.isTest() then
            --sqUtils.printLog("Writing SQL statement to DB Audit File..")
            local filePath = "/root/iguana_db_audit.txt"
            local file, err = io.open(filePath, "a")
            if not file then
               iguana.logError("Error opening file: " .. err)
               return
            end

            file:write(line .. "\n") -- Add a newline character to separate lines
            file:close()
         end
         
         
      end
   end
   
   
   --execute the SQL caommand
   
   local res = conn:execute(val)
     
   return res
   
end

-----------------------------------------------------------------

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getClinicConfigMapping(key, ...)
      
   --needs 2 parameters, facility id and reason id
   if select('#', ...) <= 2 then
      local facilityId, reasonId = select(1,...)
      
      local res = sqDB.execute{sql=
            "select "..key.." from facility_clinic_codes where facility_id = "..facilityId.." and reason_id = "..reasonId,
            live=true}
      return res[1][key]:S()
    
   else
      return ''
   end  
   
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getPriorityCode(appId,referral)
   
   if referral == json.NULL then
      return sqUtils.getJsonMappedValue(ClientConfig.priorityMap,'default')
   end

   local res = sqDB.execute{sql=
      "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
      "from reservation_ipm " ..
      "where ipm_name = 'PRIORITY' and res_id = " .. appId
      , live=true}

   if tonumber(res[1].cnt:S()) >= 1 then
      return res[1]['ipm_value']:S()
   else

      -- look for the referral priority
      res = sqDB.execute{sql="select count(*) as cnt, IFNULL(priority,'') as priority " ..
         "from ewaiting_categories " ..
         "where id = " .. referral.category_id 
         , live=true}

      if tonumber(res[1].cnt:S()) >= 1 then
         return res[1]['priority']:S()
      else

         --the appointment was not created on IPM therefore entry wont exist
         --So use internal map
         return sqUtils.getJsonMappedValue(ClientConfig.priorityMap,'default')
      end

   end
   
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getSpecialtyCode(appId, facilityId, reasonId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      local v = sqDB.getClinicConfigMapping('speciality_id', facilityId, reasonId)
      return sqUtils.getJsonMappedValue(ClientConfig.specialtyMap,v)
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getAttendingDoctor(appId, facilityId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 11111111 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      
      res = sqDB.execute{sql=
         "select firstname from facilities where parent_id = "..
         ClientConfig.parentFacilityId.." and facility_type = 'sp'"
         , live=true}
      
      local docName = res[1].firstname:S()
      
      local ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,docName)
      if ret == '' then
         ret = sqUtils.getJsonMappedValue(ClientConfig.attendingDoctorMap,'default')
      end
      
      return ret
   
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getAppointmentType(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'TYPE' " ..
         "and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return sqUtils.getJsonMappedValue(ClientConfig.appTypeMap,'default')
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getAppointmentReason(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return sqUtils.getJsonMappedValue(ClientConfig.appReasonMap,'default')
      
   end
end

-------------------------------------------------------------------------
--PB 20.10.23
function sqDB.getAppointmentCategory(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not isEmpty(res[1]['ipm_value']:S()) then
         return res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      --So use internal map
      return sqUtils.getJsonMappedValue(ClientConfig.appCategoryMap,'default')
      
   end
end


----------------------------------------------------------------------------



return sqDB