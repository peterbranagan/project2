
-- Implementation details for Tas
ClientConfig = {

   
   
   
   appTypeMap = [[ 
      { 
        'default' : 'OTPAT'
      }
   ]],
   
   attendanceStatusCodeMap = [[ 
   {
   'Not Specified' : 'NSP',
   'Attended' : '5',
   'Did Not Attend' : 'DNA',
   'Cancelled' : 'CANCL',
   'Appointment Cancelled by Patient' : '2',
   'Postponed' : '0',
   'Patient Arrived Late (seen)' : '6',
   'Unable to Attend' : 'UTA',
   'Cancelled by Clinician' : 'CBC',
   'Patient Did Not Wait' : 'DNW',
   'Arrived' : 'ARR',
   'Did Not Attend' : '3',
   'Phone Review' : 'PHREV',
   'Patient Arrived Late and Could not be Seen' : '7',
   'Clinic Task' : 'CTCPHS',
   'default' : '5'
   }
   ]],
   
   outcomeStatusCodeMap = [[ 
   {
   'Not Specified' : 'NSP',
   'Return Appointment' : 'AR',
   'Patient Died' : 'DIED',
   'Discharged' : 'DIS',
   'Admitted from Clinic' : 'H',
   'Suspended' : 'S',
   'Cancelled' : 'CANCD',
   'Review' : 'REVIW',
   'Admitted to this Rural Facility' : 'ATTRF',
   'Client to Contact' : 'CLIENT',
   'Assessment completed' : 'ASSCOP',
   'Ongoing Management' : 'ONGMAN',
   'Treatment Completed' : 'TRTCO',
   'Close Referral' : 'CR',
   'Patient Did Not Wait' : 'DNW',
   'Planned Review' : 'WLREV',
   'Transfer to Acute Hospital' : 'TTAH',
   'Transfer to other Rural Facility' : 'TTRF',
   'Not seen by Clinican' : 'NSBC',
   'Waiting List' : 'W',
   'Pre-assessment Appt' : 'PA',
   'Did Not Attend' : '0',
   'Diabetes - Task Completed' : 'DIAB',
   'Request For Admission' : 'RFA',
   'Did Not Return to Reception' : 'PDNRR',
   'SM Specialist Review/Assessment' : 'SMSRA',
   'SM Referred On' : 'SMREO',
   'SM Ongoing Management' : 'SMONM',
   'SM Wait List' : 'SMWAL',
   'SM No Further Contact' : 'SMNFC',
   'SM (Re) Allocation' : 'SMRAL',
   'SM Complete' : 'SMCNP',
   'SP Level 1' : 'L1',
   'Level 2' : 'L2',
   'Level 3' : 'L3',
   'Level 4' : 'L4',
   'SP Actualised' : 'ACT',
   'Actualised' : 'CSSACT',
   'Diagnosis' : 'NSSD',
   'Maintenance' : 'NSSM',
   'Complex' : 'NSSA',
   'Palliative' : 'NSSP',
   'Parkinson stage 1' : 'NSSP1',
   'Parkinson stage 2' : 'NSSP2',
   'Parkinson stage 3' : 'NSSP3',
   'Parkinson stage 4' : 'NSSP4',
   'Treatment Completed - Ref to GP' : 'GP',
   'Task Completed' : 'TCCPHS',
   'Medication Changed' : 'MCCPHS',
   'Health Education Provided' : 'HEPCPHS',
   'Internal Referral' : 'INTREFCPHS',
   'External Referral' : 'EXTREFCPHS',
   'Internal Admission' : 'INTADMCPHS',
   'Interrupted' : 'INTRPTCPHS',
   'default' : 'NSP'
   }
   ]],
   
   --not stored with code in DB at the moment
   attendingDoctorMap = [[
   {
   'Dr Jan Mewett - Consultant' : '21167',
   'default' : '21167'
   }
   ]],
   
   scheduleIdentifierMap = [[ 
      { 
        'default' : 'AM-SQ-ORTHO'
      }
   ]],
   
  
   
   appCategoryMap = [[ 
      { 
        'default' : 'PUB'
      }
   ]],
   
   
     

}

--ClientConfig['DbFile'] = '/etc/iNTERFACEWARE/test_au_db_credentials'
--ClientConfig['facility'] = {}
--ClientConfig['facility']['TAS'] = 6