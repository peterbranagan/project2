-- This scheduler script can be used to run a script at a given time

-- http://help.interfaceware.com/v6/scheduler-example

local scheduler = {}
scheduler.runAt = require 'scheduler'

_G.purgeDays = 30 -- no.of days of logs to keep
_G.maxFileSize = 100  -- 100 MB
_G.runTime = 21.5 -- 21:30

---- SCROLL DOWN AND VIEW THE OUTPUT FROM THE VARIABLE content AROUND ABOUT LINE 44 TO VIEW THE LOG
 
function main()
   -- The first time the scheduled time is exceeded the function is run
   -- this means the function will run once at (just after) the scheduled time
   -- Note: runAt can handle (optional) multiple parameters
  
   scheduler.runAt(_G.runTime, purgeDBAudit)
   
   local stats = os.fs.stat(os.getenv('DB_AUDIT_FILEPATH'))
 
   --view the log
   if iguana.isTest() then
      
      --local filePath = "/etc/iNTERFACEWARE/iguana_db_audit.txt"
            --local file, err = io.open(filePath, "a")
            --if not file then
             --  iguana.logError("Error opening file: " .. err)
             --  return
            --end

            --file:write(line .. "\n") -- Add a newline character to separate lines
            --file:close()
      
      --[[
           Fields:
      
           Date/Time, Channel, HL7 MessageID, SQL Atatement
      ]]
      local content = readFile(os.getenv('DB_AUDIT_FILEPATH'))
      
      trace("File Size = "..(stats.size/1000000).." MB")
      trace(content)
   end
   
   if stats ~= nil then
      if (stats.size/1000000) > _G.maxFileSize then
         error("Database log file greater than ".._G.maxFileSize.." MB !")  
      end
   end
   
end

-- Within the editor we run the function all the time.
function purgeDBAudit()
   
   if not iguana.isTest() then

      local daysToPurge = _G.purgeDays + 1
      local filePath = os.getenv('DB_AUDIT_FILEPATH')

      local file = io.open(filePath, "r")
      if not file then
         print("Error opening file for reading")
         return
      end

      local tmpFile = io.open(os.getenv('DB_AUDIT_FILEPATH')..".TMP", "a")
      if not tmpFile then
         print("Error opening file for appending")
         return
      end

      for line in file:lines() do
         local dt = line:sub(2,11)
         local days = daysOld(dt)
         if days < daysToPurge then
            tmpFile:write(line .. "\n")          
         end     
      end

      tmpFile:close()
      file:close()

      local success, errMsg = os.remove(filePath)
      if success then
         print("original audit file deleted successfully.")
      end

      local success, errMsg = os.rename(os.getenv('DB_AUDIT_FILEPATH')..".TMP", filePath)
      if success then
         iguana.logInfo("New purged audit file created successfully.")
      end

      iguana.logInfo("Data in iguana_db_audit file purged to ".._G.purgeDays.." days!")

   end
      
end

function readFile(file)
   local File = io.open(file, 'r')
   local FileContents = File:read('*a')
   File:close()
   
   return FileContents
   
end

function daysOld(dateString)

   if dateString:sub(1,2) ~= "20" then
      --if it doesnt start with a date then the line is corrupt
      --this will force the line to be removed from the audit file
      return 999
   end
   
   local pattern = "(%d+)-(%d+)-(%d+)"
   local year, month, day = dateString:match(pattern)

   local t1 = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day)})
   local t2 = os.time(os.date("*t"))

   local differenceInSeconds = os.difftime(t2, t1)
   local differenceInDays = differenceInSeconds / (24 * 60 * 60)

   return math.ceil(differenceInDays) -- Rounded up to the nearest whole day
end



