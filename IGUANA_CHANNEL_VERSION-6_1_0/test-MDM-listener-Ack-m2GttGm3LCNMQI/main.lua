function main(Data)

   iguana.stopOnError(false)
   --[[
   MSH|^~\&|IIEIPM|NWA|Swiftqueue|NWA|20240515082905||ACK^S15|AFE543665101600E135B3D9630F2FA89|P|2.4|||NE|NE|||
   MSA|CA|AFE543665101600E135B3D9630F2FA89|Record Processed Successfully|||
   ]]

   local Msg = hl7.parse{vmd='ack.vmd', data=Data}
   local Ack = hl7.message{vmd='ack.vmd', name='Ack'}
   Ack.MSH[3][1] = "IIEIPM"
   Ack.MSH[4][1] = "NWA"
   Ack.MSH[5][1] = "Swiftqueue"
   Ack.MSH[6][1] = "NWA"
   Ack.MSH[7][1] = os.date('%Y%m%d%H%M%S')  
   Ack.MSH[9][1] = 'ACK'
   Ack.MSH[10] = Msg.MSH[10]
   Ack.MSH[11][1] = 'P'
   Ack.MSH[12][1] = '2.4'

   --Default to AA until told otherwiase
   --[[
   Ack.MSA[1] = 'AE'
   Ack.MSA[2] = Msg.MSH[10]
   Ack.MSA[3] = "Listener failed to execute process [from_SwiftQueue_HL7] with error [Process execution retries exceeded, terminating process]"
   ]]
   
   Ack.MSA[1] = 'AA'
   Ack.MSA[2] = Msg.MSH[10]
   Ack.MSA[3] = "Record Processed Successfully"
   
   ack.send(Ack:S())


end